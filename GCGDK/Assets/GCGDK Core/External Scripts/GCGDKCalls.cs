﻿using GCGDK;
using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.Reflection;

public class GCGDKCalls : MonoBehaviour
{
    [HideInInspector]
    public Manager Manager;
    [HideInInspector]
    public PlayLoop PlayLoop;

    public string MinigameString = "MINIGAME!";
    public string BonusString = "BONUS!";
    public string NoWinString = "TRY AGAIN";
    public string MinigameMissString = "MINIGAME MISSED";
    public string BonusMissString = "BONUS MISSED";
    public string NoWinMissString = "";

    public float SmallCashWinBangTime = 1;
    public float MediumCashWinBangTime = 2;
    public float BigCashWinBangTime = 3;
    public float HugeCashWinBangTime = 4;
    public float MassiveCashWinBangTime = 5;
    public float TokenBangTime = 1;
    public float MinigameBangTime = 8;
    public float BonusBangTime = 10;
    public float NoWinBangTime = 0;

    [HideInInspector]
    public int codespaceInt;

    private void Awake()
    {
        Manager = GameObject.Find("Manager").GetComponent<Manager>();
        PlayLoop = Manager.GetComponent<PlayLoop>();
    }

    public void RevealUnselectedResults()
    {
        PlayLoop.RevealUnselectedResults(MinigameMissString, BonusMissString, NoWinMissString);
    }

    public void RevealSelectedResult()
    {
        PlayLoop.RevealSelectedResult(MinigameString, BonusString, NoWinString, SmallCashWinBangTime, MediumCashWinBangTime, BigCashWinBangTime, HugeCashWinBangTime, MassiveCashWinBangTime, TokenBangTime, NoWinBangTime);
    }

    public void ProceedToState7()
    {
        PlayLoop.ProceedToState7();
    }

    public void PostSendRewardInState7()
    {
        PlayLoop.PostSendRewardInState7();
    }

    public void RewardMinigame()
    {
        PlayLoop.RewardMinigame(MinigameBangTime);
    }

    public void RewardBonus()
    {
        PlayLoop.RewardBonus(BonusBangTime);
    }

    public void FinishHand()
    {
        PlayLoop.FinishHand();
    }

    private void Update()
    {
        if (Manager)
        {
            codespaceInt = Manager.CodespaceStateInt;
        } else
        {
            codespaceInt = 0;
        }
    }
}
