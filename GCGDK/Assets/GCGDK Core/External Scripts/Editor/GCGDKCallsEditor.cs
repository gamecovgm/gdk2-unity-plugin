﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GCGDKCalls))]
public class GCGDKCallsEditor : Editor
{
    GCGDKCalls GCGDKCalls;
    private bool showVariables;

    private void OnEnable()
    {
        GCGDKCalls = (GCGDKCalls)target;
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.HelpBox("This script isn't required to use. It's here to simply help visualize the Codespace flow with appropriate buttons.", MessageType.Info, true);

        GUILayout.Space(10);
        EditorGUI.BeginDisabledGroup(GCGDKCalls.PlayLoop == null);

        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(GCGDKCalls.codespaceInt != 6);
        if (GUILayout.Button("RevealUnselectedResults"))
        {
            GCGDKCalls.RevealUnselectedResults();
        }
        GUILayout.FlexibleSpace();
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(5);
        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(GCGDKCalls.codespaceInt != 6);
        if (GUILayout.Button("RevealSelectedResult"))
        {
            GCGDKCalls.RevealSelectedResult();
        }
        GUILayout.FlexibleSpace();
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(5);
        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(GCGDKCalls.codespaceInt != 6);
        if (GUILayout.Button("ProceedToState7"))
        {
            GCGDKCalls.ProceedToState7();
        }
        GUILayout.FlexibleSpace();
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(5);
        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(GCGDKCalls.codespaceInt != 7);
        if (GUILayout.Button("PostSendRewardInState7"))
        {
            GCGDKCalls.PostSendRewardInState7();
        }
        GUILayout.FlexibleSpace();
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(5);
        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(GCGDKCalls.codespaceInt != 7);
        if (GUILayout.Button("RewardMinigame"))
        {
            GCGDKCalls.RewardMinigame();
        }
        GUILayout.FlexibleSpace();
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(5);
        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(GCGDKCalls.codespaceInt != 7);
        if (GUILayout.Button("RewardBonus"))
        {
            GCGDKCalls.RewardBonus();
        }
        GUILayout.FlexibleSpace();
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(5);
        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginDisabledGroup(GCGDKCalls.codespaceInt != 7 && GCGDKCalls.codespaceInt != 8);
        if (GUILayout.Button("FinishHand"))
        {
            GCGDKCalls.FinishHand();
        }
        GUILayout.FlexibleSpace();
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.EndHorizontal();

        EditorGUI.EndDisabledGroup();

        GUILayout.Space(10);
        string variablesString = "";
        if (showVariables)
        {
            variablesString = "Hide Variables";
        } else
        {
            variablesString = "Show Variables";
        }
        showVariables = EditorGUILayout.Foldout(showVariables, variablesString, new GUIStyle(EditorStyles.foldout) { fontStyle = FontStyle.Bold });
        if (showVariables)
        {
            base.OnInspectorGUI();
        }

        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Updated");
            EditorUtility.SetDirty(target);
        }
        serializedObject.ApplyModifiedProperties();
    }
}
