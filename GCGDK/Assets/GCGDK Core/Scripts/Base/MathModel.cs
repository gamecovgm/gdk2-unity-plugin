﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;
using TMPro;
using GCGDK;
using UnityEngine.UI;

namespace GCGDK
{
    [DisallowMultipleComponent]
    public class MathModel : MonoBehaviour
    {
        public float[][] miniGameMultipliers;
        public float[][] miniGameFinalMultipliers;
        public float[][] bonusMultipliersTier1;
        public float[][] bonusMultipliersTier2;
        public float[][] bonusMultipliersTier3;
        public float[][] tokenMultipliers;

        IEnumerable<float> miniGameMultipliersFromXML;
        IEnumerable<float> miniGameFinalMultipliersFromXML;
        IEnumerable<float> bonusMultipliersFromXMLTier1;
        IEnumerable<float> bonusMultipliersFromXMLTier2;
        IEnumerable<float> bonusMultipliersFromXMLTier3;
        IEnumerable<float> tokenMultipliersFromXML;

        /// <summary>
        /// Useful class if xml defines wins as both cash and tokens
        /// </summary>
        public class Prize
        {
            public float multiplierAward = 0;
            public int tokenAward = 0;
            public int index;
            public int weight;
            public float oddsPercentage;
        }

        [SerializeField]
        public Prize[][] prize;

        [HideInInspector]
        public Manager Manager;

        [SerializeField]
        public int[] denoms = new int[5] { 50, 100, 200, 300, 500 };
        [SerializeField]
        public float[][] multipliers;
        [SerializeField]
        public float[][] odds;
        [SerializeField]
        public float[][] tokens;

        [HideInInspector]
        public XDocument mathModel;
        [HideInInspector]
        public IEnumerable<int> betDenomsFromXML;
        [HideInInspector]
        public IEnumerable<float> multipliersFromXML;

        internal List<Transform> PopMainGameValues = new List<Transform>();
        internal List<Transform> PopMiniGameValues = new List<Transform>();
        internal List<Transform> PopMiniGameFinalValues = new List<Transform>();
        internal List<Transform> PopBonusRoundTier1Values = new List<Transform>();
        internal List<Transform> PopBonusRoundTier2Values = new List<Transform>();
        internal List<Transform> PopBonusRoundTier3Values = new List<Transform>();
        internal List<Transform> PopTokenValues = new List<Transform>();
        internal List<Transform> PTDenomButtonText = new List<Transform>();
        internal List<Transform> PTDenomHighlightedButtonText = new List<Transform>();
        //internal GameObject[] PTDenoms;

        private void Awake()
        {
            Manager = GetComponent<Manager>();
        }

        /// <summary>
        /// Get the denoms, multipliers and odds from the Math Model XML.
        /// </summary>
        public virtual void Parse()
        {

            denoms = new int[5] { 50, 100, 200, 300, 500 };

            mathModel = XDocument.Load("C:/XML/" + Manager.Codespace.Link.selectedModule + ".xml");

            multipliers = new float[denoms.Length][];
            odds = new float[denoms.Length][];
            miniGameMultipliers = new float[denoms.Length][];
            miniGameFinalMultipliers = new float[denoms.Length][];
            bonusMultipliersTier1 = new float[denoms.Length][];
            bonusMultipliersTier2 = new float[denoms.Length][];
            bonusMultipliersTier3 = new float[denoms.Length][];
            tokenMultipliers = new float[denoms.Length][];


            multipliersFromXML = from item in mathModel.Descendants("Prize")
                                 where (string)item.Parent.Parent.Parent.Element("Name") == Manager.Codespace.Link.selectedMathModel
                                 && item.Parent.Name == "Initial_Cash_Prizes"
                                 select (float)item.Element("Value");

            IEnumerable<int> oddsFromXML = from item in mathModel.Descendants("Prize")
                                           where (string)item.Parent.Parent.Parent.Element("Name") == Manager.Codespace.Link.selectedMathModel
                                           && item.Parent.Name == "Initial_Cash_Prizes"
                                           select (int)item.Element("Weight");

            miniGameMultipliersFromXML = from item in mathModel.Descendants("Prize")
                                         where (string)item.Parent.Parent.Parent.Element("Name") == Manager.Codespace.Link.selectedMathModel
                                         && item.Parent.Name == "Small_Bonus_Prizes"
                                         select (float)item.Element("Value");

            miniGameFinalMultipliersFromXML = from item in mathModel.Descendants("Prize")
                                              where (string)item.Parent.Parent.Parent.Element("Name") == Manager.Codespace.Link.selectedMathModel
                                              && item.Parent.Name == "Small_Bonus_Final-Prizes"
                                              select (float)item.Element("Value");

            tokenMultipliersFromXML = from item in mathModel.Descendants("Prize")
                                      where (string)item.Parent.Parent.Parent.Element("Name") == Manager.Codespace.Link.selectedMathModel
                                      && item.Parent.Name == "Initial_Token_Prizes"
                                      select (float)item.Element("Value");

            miniGameMultipliersFromXML = miniGameMultipliersFromXML.GroupBy(x => x).Select(z => z.First());

            miniGameMultipliersFromXML = miniGameMultipliersFromXML.OrderBy(item => item);

            miniGameFinalMultipliersFromXML = miniGameFinalMultipliersFromXML.GroupBy(x => x).Select(z => z.First());

            miniGameFinalMultipliersFromXML = miniGameFinalMultipliersFromXML.OrderBy(item => item);

            tokenMultipliersFromXML = tokenMultipliersFromXML.GroupBy(x => x).Select(z => z.First());

            tokenMultipliersFromXML = tokenMultipliersFromXML.OrderBy(item => item);

            bonusMultipliersFromXMLTier1 = from item in mathModel.Descendants("Prize")
                                           where (string)item.Parent.Parent.Parent.Element("Name") == Manager.Codespace.Link.selectedMathModel
                                           && item.Parent.Name == "Big_Bonus_Level_1_Prizes"
                                           select (float)item.Element("Value");

            bonusMultipliersFromXMLTier1 = bonusMultipliersFromXMLTier1.GroupBy(x => x).Select(z => z.First());

            bonusMultipliersFromXMLTier1 = bonusMultipliersFromXMLTier1.OrderBy(item => item);

            bonusMultipliersFromXMLTier2 = from item in mathModel.Descendants("Prize")
                                           where (string)item.Parent.Parent.Parent.Element("Name") == Manager.Codespace.Link.selectedMathModel
                                           && item.Parent.Name == "Big_Bonus_Level_2_Prizes"
                                           select (float)item.Element("Value");

            bonusMultipliersFromXMLTier2 = bonusMultipliersFromXMLTier2.GroupBy(x => x).Select(z => z.First());

            bonusMultipliersFromXMLTier2 = bonusMultipliersFromXMLTier2.OrderBy(item => item);


            bonusMultipliersFromXMLTier3 = from item in mathModel.Descendants("Prize")
                                           where (string)item.Parent.Parent.Parent.Element("Name") == Manager.Codespace.Link.selectedMathModel
                                           && item.Parent.Name == "Big_Bonus_Level_3_Prizes"
                                           select (float)item.Element("Value");

            bonusMultipliersFromXMLTier3 = bonusMultipliersFromXMLTier3.GroupBy(x => x).Select(z => z.First());

            bonusMultipliersFromXMLTier3 = bonusMultipliersFromXMLTier3.OrderBy(item => item);


            int denomDenominator = oddsFromXML.Sum();

            for (int i = 0; i < denoms.Length; i++)
            {
                multipliers[i] = new float[multipliersFromXML.Count()];
                odds[i] = new float[oddsFromXML.Count()];
                miniGameMultipliers[i] = new float[miniGameMultipliersFromXML.Count()];
                miniGameFinalMultipliers[i] = new float[miniGameFinalMultipliersFromXML.Count()];
                bonusMultipliersTier1[i] = new float[bonusMultipliersFromXMLTier1.Count()];
                bonusMultipliersTier2[i] = new float[bonusMultipliersFromXMLTier2.Count()];
                bonusMultipliersTier3[i] = new float[bonusMultipliersFromXMLTier3.Count()];
                tokenMultipliers[i] = new float[tokenMultipliersFromXML.Count()];

                for (int a = 0; a < multipliersFromXML.Count(); a++)
                {
                    multipliers[i][a] = multipliersFromXML.ElementAt(a) / 50;
                    odds[i][a] = oddsFromXML.ElementAt(a) / (float)denomDenominator;
                }

                //Dbg.Trace("MiniGame Multipliers for " + denoms[i]);

                for (int x = 0; x < miniGameMultipliersFromXML.Count(); x++)
                {
                    miniGameMultipliers[i][x] = miniGameMultipliersFromXML.ElementAt(x) / 50;
                    //Dbg.Trace(miniGameMultipliers[i][x] + "x - " + miniGameMultipliersFromXML.ElementAt(x));
                }

                //Dbg.Trace("MiniGame Final Round Multipliers for " + denoms[i]);

                for (int x = 0; x < miniGameFinalMultipliersFromXML.Count(); x++)
                {
                    miniGameFinalMultipliers[i][x] = miniGameFinalMultipliersFromXML.ElementAt(x) / 50;
                    //Dbg.Trace(miniGameFinalMultipliers[i][x] + "x - " + miniGameFinalMultipliersFromXML.ElementAt(x));
                }

                //Dbg.Trace("Bonus Multipliers for " + denoms[i]);

                for (int y = 0; y < bonusMultipliersFromXMLTier1.Count(); y++)
                {
                    bonusMultipliersTier1[i][y] = bonusMultipliersFromXMLTier1.ElementAt(y) / 50;
                    //Dbg.Trace(bonusMultipliers[i][y] + "x - " + bonusMultipliersFromXML.ElementAt(y));
                }

                for (int y = 0; y < bonusMultipliersFromXMLTier2.Count(); y++)
                {
                    bonusMultipliersTier2[i][y] = bonusMultipliersFromXMLTier2.ElementAt(y) / 50;
                    //Dbg.Trace(bonusMultipliers[i][y] + "x - " + bonusMultipliersFromXML.ElementAt(y));
                }

                for (int y = 0; y < bonusMultipliersFromXMLTier3.Count(); y++)
                {
                    bonusMultipliersTier3[i][y] = bonusMultipliersFromXMLTier3.ElementAt(y) / 50;
                    //Dbg.Trace(bonusMultipliers[i][y] + "x - " + bonusMultipliersFromXML.ElementAt(y));
                }

                //Dbg.Trace("Token Multipliers for " + denoms[i]);

                for (int x = 0; x < tokenMultipliersFromXML.Count(); x++)
                {
                    tokenMultipliers[i][x] = tokenMultipliersFromXML.ElementAt(x);
                    //Dbg.Trace(tokenMultipliers[i][x] + "x - " + tokenMultipliersFromXML.ElementAt(x));
                }
            }

            //StartCoroutine(CreatePaytable());
        }

        // Yeah I'm doing this here and not in the Paytable class. So sue me!
        internal void CreatePaytable()
        {
            for (int i = 0; i < denoms.Count(); i++)
            {
                for (int a = 0; a < PopMainGameValues[i].childCount; a++)
                {
                    if (PopMainGameValues[i].GetChild(a).GetComponent<TextMeshProUGUI>())
                    {
                        PopMainGameValues[i].GetChild(a).GetComponent<TextMeshProUGUI>().text = Manager.Currencify(denoms[i] * multipliers[i][a] * .01f);
                    } 
                    else if (PopMainGameValues[i].GetChild(a).GetComponent<TextMeshPro>())
                    {
                        PopMainGameValues[i].GetChild(a).GetComponent<TextMeshPro>().text = Manager.Currencify(denoms[i] * multipliers[i][a] * .01f);
                    }
                    else if (PopMainGameValues[i].GetChild(a).GetComponent<Text>())
                    {
                        PopMainGameValues[i].GetChild(a).GetComponent<Text>().text = Manager.Currencify(denoms[i] * multipliers[i][a] * .01f);
                    }
                }

                for (int a = 0; a < PopMiniGameValues[i].childCount; a++)
                {
                    if (PopMiniGameValues[i].GetChild(a).GetComponent<TextMeshProUGUI>())
                    {
                        PopMiniGameValues[i].GetChild(a).GetComponent<TextMeshProUGUI>().text = Manager.Currencify(denoms[i] * miniGameMultipliers[i][a] * .01f);
                    } 
                    else if (PopMiniGameValues[i].GetChild(a).GetComponent<TextMeshPro>())
                    {
                        PopMiniGameValues[i].GetChild(a).GetComponent<TextMeshPro>().text = Manager.Currencify(denoms[i] * miniGameMultipliers[i][a] * .01f);
                    } 
                    else if (PopMiniGameValues[i].GetChild(a).GetComponent<Text>())
                    {
                        PopMiniGameValues[i].GetChild(a).GetComponent<Text>().text = Manager.Currencify(denoms[i] * miniGameMultipliers[i][a] * .01f);
                    }
                }

                for (int a = 0; a < PopMiniGameFinalValues[i].childCount; a++)
                {
                    if (PopMiniGameFinalValues[i].GetChild(a).GetComponent<TextMeshProUGUI>())
                    {
                        PopMiniGameFinalValues[i].GetChild(a).GetComponent<TextMeshProUGUI>().text = Manager.Currencify(denoms[i] * miniGameFinalMultipliers[i][a] * .01f);
                    } 
                    else if (PopMiniGameFinalValues[i].GetChild(a).GetComponent<TextMeshPro>())
                    {
                        PopMiniGameFinalValues[i].GetChild(a).GetComponent<TextMeshPro>().text = Manager.Currencify(denoms[i] * miniGameFinalMultipliers[i][a] * .01f);
                    }
                    else if (PopMiniGameFinalValues[i].GetChild(a).GetComponent<Text>())
                    {
                        PopMiniGameFinalValues[i].GetChild(a).GetComponent<Text>().text = Manager.Currencify(denoms[i] * miniGameFinalMultipliers[i][a] * .01f);
                    }
                }

                for (int a = 0; a < PopBonusRoundTier1Values[i].childCount; a++)
                {
                    if (PopBonusRoundTier1Values[i].GetChild(a).GetComponent<TextMeshProUGUI>())
                    {
                        PopBonusRoundTier1Values[i].GetChild(a).GetComponent<TextMeshProUGUI>().text = Manager.Currencify(denoms[i] * bonusMultipliersTier1[i][a] * .01f);
                    } 
                    else if (PopBonusRoundTier1Values[i].GetChild(a).GetComponent<TextMeshPro>())
                    {
                        PopBonusRoundTier1Values[i].GetChild(a).GetComponent<TextMeshPro>().text = Manager.Currencify(denoms[i] * bonusMultipliersTier1[i][a] * .01f);
                    }
                    else if (PopBonusRoundTier1Values[i].GetChild(a).GetComponent<Text>())
                    {
                        PopBonusRoundTier1Values[i].GetChild(a).GetComponent<Text>().text = Manager.Currencify(denoms[i] * bonusMultipliersTier1[i][a] * .01f);
                    }
                }

                for (int a = 0; a < PopBonusRoundTier2Values[i].childCount; a++)
                {
                    if (PopBonusRoundTier2Values[i].GetChild(a).GetComponent<TextMeshProUGUI>())
                    {
                        PopBonusRoundTier2Values[i].GetChild(a).GetComponent<TextMeshProUGUI>().text = Manager.Currencify(denoms[i] * bonusMultipliersTier2[i][a] * .01f);
                    }
                    else if (PopBonusRoundTier2Values[i].GetChild(a).GetComponent<TextMeshPro>())
                    {
                        PopBonusRoundTier2Values[i].GetChild(a).GetComponent<TextMeshPro>().text = Manager.Currencify(denoms[i] * bonusMultipliersTier2[i][a] * .01f);
                    }
                    else if (PopBonusRoundTier2Values[i].GetChild(a).GetComponent<Text>())
                    {
                        PopBonusRoundTier2Values[i].GetChild(a).GetComponent<Text>().text = Manager.Currencify(denoms[i] * bonusMultipliersTier2[i][a] * .01f);
                    }
                }

                for (int a = 0; a < PopBonusRoundTier3Values[i].childCount; a++)
                {
                    if (PopBonusRoundTier3Values[i].GetChild(a).GetComponent<TextMeshProUGUI>())
                    {
                        PopBonusRoundTier3Values[i].GetChild(a).GetComponent<TextMeshProUGUI>().text = Manager.Currencify(denoms[i] * bonusMultipliersTier3[i][a] * .01f);
                    }
                    else if (PopBonusRoundTier3Values[i].GetChild(a).GetComponent<TextMeshPro>())
                    {
                        PopBonusRoundTier3Values[i].GetChild(a).GetComponent<TextMeshPro>().text = Manager.Currencify(denoms[i] * bonusMultipliersTier3[i][a] * .01f);
                    }
                    else if (PopBonusRoundTier3Values[i].GetChild(a).GetComponent<Text>())
                    {
                        PopBonusRoundTier3Values[i].GetChild(a).GetComponent<Text>().text = Manager.Currencify(denoms[i] * bonusMultipliersTier3[i][a] * .01f);
                    }
                }

                for (int a = 0; a < PopTokenValues[i].childCount; a++)
                {
                    if (PopTokenValues[i].GetChild(a).GetComponent<TextMeshProUGUI>())
                    {
                        PopTokenValues[i].GetChild(a).GetComponent<TextMeshProUGUI>().text = ((denoms[i] / 50) * tokenMultipliers[i][a]).ToString();
                    } 
                    else if (PopTokenValues[i].GetChild(a).GetComponent<TextMeshPro>())
                    {
                        PopTokenValues[i].GetChild(a).GetComponent<TextMeshPro>().text = ((denoms[i] / 50) * tokenMultipliers[i][a]).ToString();
                    }
                    else if (PopTokenValues[i].GetChild(a).GetComponent<Text>())
                    {
                        PopTokenValues[i].GetChild(a).GetComponent<Text>().text = ((denoms[i] / 50) * tokenMultipliers[i][a]).ToString();
                    }
                }

                string denomText = Manager.Currencify(denoms[i]);
                if (PTDenomButtonText[i].GetComponent<TextMeshProUGUI>())
                {
                    PTDenomButtonText[i].GetComponent<TextMeshProUGUI>().text = denomText;
                }
                else if (PTDenomButtonText[i].GetComponent<TextMeshPro>())
                {
                    PTDenomButtonText[i].GetComponent<TextMeshPro>().text = denomText;
                }
                else if (PTDenomButtonText[i].GetComponent<Text>())
                {
                    PTDenomButtonText[i].GetComponent<Text>().text = denomText;
                }

                if (PTDenomHighlightedButtonText[i].GetComponent<TextMeshProUGUI>())
                {
                    PTDenomHighlightedButtonText[i].GetComponent<TextMeshProUGUI>().text = denomText;
                }
                else if (PTDenomHighlightedButtonText[i].GetComponent<TextMeshPro>())
                {
                    PTDenomHighlightedButtonText[i].GetComponent<TextMeshPro>().text = denomText;
                }
                else if (PTDenomHighlightedButtonText[i].GetComponent<Text>())
                {
                    PTDenomHighlightedButtonText[i].GetComponent<Text>().text = denomText;
                }
            }
        }
    }
}
