﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCCL;
using Microsoft.Win32;
using GCGDK;

namespace GCGDK
{
    [DisallowMultipleComponent]
    public class Cashout : MonoBehaviour
    {

        [HideInInspector]
        public Manager Manager;

        public bool handpayInProgress;
        public bool jackpotHandpayInProgress;

        string CSiniFile = "C:/CSGCore/bin/Core.ini";

        private void Awake()
        {
            Manager = GetComponent<Manager>();
        }

        private void Start()
        {
            Manager.ButtonManager.ButtonPressEvent += new ButtonPressEventHandler(ScreenButtonPressed);
        }

        /// <summary>
        /// Ask the user if they are sure, possibly provide opportunity to win the miss pool;
        /// </summary>
        /// <returns></returns>
        internal void CashoutRequested()
        {
            if (Manager.Codespace.Link.codespaceState != 4 && Manager.Codespace.Link.codespaceState != 25 && Manager.Codespace.Link.codespaceState != 26)
            {
                return;
            }

            if ((Manager.Codespace.Link.cashableValue + Manager.Codespace.Link.nonRestrictedValue) == 0 && !CashoutRestricted())
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.NonCashableBalance, Manager.Codespace.Link.restrictedValue);
                Manager.StateManager.ChangeState(State.System, "NonCashableCredit");
                return;
            }

            Manager.PlayLoop.InterruptPlay();

            if (Manager.Codespace.Link.codespaceState == 25 || Manager.Codespace.Link.codespaceState == 26)
            {
                StartCoroutine(DoCashout());
                return;
            }

            DiscourageAbandonment();

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.CashoutAmount, Manager.Currencify(Manager.Codespace.Link.cashableValue + Manager.Codespace.Link.nonRestrictedValue));

            Manager.StateManager.ChangeState(State.System, "CashOutConfirm");
        }

        /// <summary>
        /// The player does not wish to cash out at this time.
        /// </summary>
        internal void CashoutCancel()
        {
            Manager.PlayLoop.StartSession();
        }

        /// <summary>
        /// The cashout sequence.
        /// </summary>
        /// <returns></returns>
        internal IEnumerator DoCashout()
        {
            Manager.LEDs.ChangeSequence(LEDSequence.CASHOUT);

            if ((Manager.Codespace.Link.cashableValue + Manager.Codespace.Link.nonRestrictedValue) > 0 || (Manager.Codespace.Link.totalValue > 0 && CashoutRestricted()))
            {
                if (Manager.Codespace.Link.codespaceState != 4 && Manager.Codespace.Link.codespaceState != 12 && Manager.Codespace.Link.codespaceState != 25 && Manager.Codespace.Link.codespaceState != 26)
                {
                    yield break;
                }

                Manager.Codespace.Link.PostCashoutCommand();

                while (Manager.Codespace.Link.codespaceState != 12 && Manager.Codespace.Link.codespaceState != 11)
                {
                    yield return null;
                }

                if (Manager.Codespace.Link.codespaceState == 12)
                {
                    //Manager.AudioManager.PlaySFX(Manager.AudioManager.cashOut);

                    if (Manager.Codespace.Link.restrictedValue > 0 && !CashoutRestricted())
                    {
                        Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.NonCashableBalance, Manager.Codespace.Link.restrictedValue);
                        Manager.StateManager.ChangeState(State.System, "NonCashableCredit");
                    }
                    else
                    {
                        //Manager.AudioManager.PlaySFX(Manager.AudioManager.payout);

                        Manager.StateManager.ChangeState(State.System, "CashingOut");

                        Manager.Codespace.Link.PostResetMathCommand();

                        while (Manager.Codespace.Link.totalValue != 0 || !Manager.PlayLoop.MachineClean())
                        {
                            yield return null;
                        }

                        StartCoroutine(Manager.PlayLoop.EndSession());
                    }
                }
            }
        }

        /// <summary>
        /// Handpay sequence.
        /// Occurs when either the amount to be paid out is greater than the ticket limit for the machine or the printer is is not available.
        /// Triggered by Codespace state change.
        /// </summary>
        /// <returns></returns>
        internal IEnumerator Handpay()
        {
            if (!handpayInProgress)
            {
                handpayInProgress = true;

                yield return new WaitForSeconds(0.1f);

                bool creditLimitHandpay = Manager.Codespace.Link.creditLimit <= Manager.Codespace.Link.totalValue;

                Dbg.Trace("Credit Limit: " + Manager.Codespace.Link.creditLimit);

                Manager.StateManager.ChangeState(State.System, "Handpay");
                Manager.LEDs.ChangeSequence(LEDSequence.TILT);

                while (Manager.Codespace.Link.codespaceState == 11)
                {
                    yield return null;
                }

                while (Manager.Codespace.Link.codespaceState == 15)
                {
                    yield return null;
                }

                if (Manager.Codespace.Link.codespaceState == 4 || Manager.Codespace.Link.codespaceState == 1 || Manager.Codespace.Link.codespaceState == 25 || Manager.Codespace.Link.codespaceState == 27)
                {
                    Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.HandpayAmount, "");

                    if (Manager.Codespace.Link.restrictedValue > 0 && !CashoutRestricted())
                    {
                        Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.NonCashableBalance, Manager.Codespace.Link.restrictedValue);
                        Manager.StateManager.ChangeState(State.System, "NonCashableCredit");
                    }
                    else if (creditLimitHandpay && !Manager.PlayLoop.MachineClean())
                    {
                        Manager.StateManager.ChangeState(State.InGame);
                    }
                    else
                    {
                        StartCoroutine(Manager.PlayLoop.EndSession());
                    }
                }

                handpayInProgress = false;
            }
            else
            {
                Dbg.Trace("Handpay already in progress");
            }
        }

        /// <summary>
        /// Jackpot Handpay sequence.
        /// Occurs when the amount won in a single game is greater than the jackpot limit. Only the amount won in that game is paid out.
        /// Triggered by Codespace state change.
        /// </summary>
        /// <returns></returns>
        internal IEnumerator JackpotHandpay()
        {
            if (!jackpotHandpayInProgress)
            {
                jackpotHandpayInProgress = true;

                Manager.StateManager.ChangeState(State.System, "JackpotHandpay");
                Manager.LEDs.ChangeSequence(LEDSequence.TILT);

                while (Manager.Codespace.Link.codespaceState == 10)
                {
                    yield return null;
                }

                while (Manager.Codespace.Link.codespaceState == 15)
                {
                    yield return null;
                }

                if (Manager.Codespace.Link.codespaceState == 4)
                {
                    Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.JackpotHandpayAmount, "");

                    Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, "");

                    if (Manager.Codespace.Link.totalValue == 0)
                    {
                        Manager.PlayLoop.underfundedCo = StartCoroutine(Manager.PlayLoop.Underfunded());
                    }
                    else
                    {
                        Manager.StateManager.ChangeState(State.InGame);
                    }
                }

                jackpotHandpayInProgress = false;
            }
            else
            {
                Dbg.Trace("Jackpot Handpay already in progress");
            }
        }

        /// <summary>
        /// Returns a string explaining the consequences of cashing out or remaining underfunded
        /// </summary>
        /// <returns>String</returns>
        internal void DiscourageAbandonment()
        {
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.DiscourageAbandonment, "");

            if (Manager.isMultiGame)
            {
                return;
            }

            if (Manager.Codespace.Link.progressivePool > 0)
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.DiscourageAbandonment, "You will forfeit your\nmiss pool of " + Manager.Currencify(Manager.Codespace.Link.progressivePool));
            }
            else if (Manager.Codespace.Link.progressiveTokens > 0)
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.DiscourageAbandonment, "You will forfeit your " + Manager.Codespace.Link.progressiveTokens.ToString("N0") + " token" + (Manager.Codespace.Link.progressiveTokens == 1 ? "" : "s"));
            }
            else
            {
                
            }
        }

        /// <summary>
        /// Is cashing out restricted credit allowed on this machine?
        /// </summary>
        /// <returns></returns>
        private bool CashoutRestricted()
        {
            if (System.IO.File.Exists(CSiniFile))
            {
                bool localRestrictedTickets = false;
                bool restrictedCashout = false;

                string[] lines = System.IO.File.ReadAllLines(CSiniFile);

                foreach (var line in lines)
                {
                    string[] parsed = line.Split('=');

                    if (parsed[0] == "LocalRestrictedTickets")
                    {
                        localRestrictedTickets = parsed[1] == "1" ? true : false;
                    }
                    else if (parsed[0] == "RestrictedCashout")
                    {
                        restrictedCashout = parsed[1] == "1" ? true : false;
                    }
                }

                if (!localRestrictedTickets || !restrictedCashout)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if ((int)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\CodeSpace Gaming\\Core", "LocalRestrictedTickets", 0) == 0 || (int)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\CodeSpace Gaming\\Core", "RestrictedCashout", 0) == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        internal void ScreenButtonPressed(object source, ScreenButtonArgs e)
        {
            if (e.pressedButton == ButtonType.Confirm && Manager.StateManager.State == State.System)
            {
                Manager.StateManager.ChangeState(State.InGame);
                //StartCoroutine(Manager.PlayLoop.RecoverFromInterruption());
            }
        }
    }
}
