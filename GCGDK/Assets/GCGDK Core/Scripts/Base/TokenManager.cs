﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GCGDK;

namespace GCGDK
{
    public delegate void TokenSpendEventHandler(object source, TokenSpendArgs e);

    public class TokenSpendArgs : System.EventArgs
    {
        public int tokenLevel;
        public int tokenCost;
        public TokenSpendArgs(int tokenLevelSpent, int tokenSpent)
        {
            tokenLevel = tokenLevelSpent;
            tokenCost = tokenSpent;
        }
    }

    [DisallowMultipleComponent]
    public class TokenManager : MonoBehaviour
    {

        internal bool holdForTokenUpdate;

        public event TokenSpendEventHandler TokenSpendEvent;

        [HideInInspector]
        public int tokenTotal;

        internal int selectedToken;

        internal bool tokenInUse;

        // Tokens to be removed by sending Manager.Codespace.Link.PostSpendTokensCommand((uint)buttonCost) because the purchased process does not impact the math model
        internal int tokensSpent;

        [HideInInspector]
        public Manager Manager;

        private int[] queuedChanges;

        private void Awake()
        {
            Manager = GetComponent<Manager>();

            queuedChanges = new int[5];
        }

        // Use this for initialization
        void Start()
        {

            CodespaceTokenTotalChanged();

            List<GCGDKButton> tokenButtons = Manager.ButtonManager.GetButtonsOfType(ButtonType.UseToken);

            foreach (GCGDKButton tokenButton in tokenButtons)
            {
                StartCoroutine(tokenButton.TokenCostText());
            }
        }

        /// <summary>
        /// Spend tokens. Token level is sent to game to be acted on and cost is deducted from total.
        /// Afterwards tokens are refreshed. If you want tokens to be disabled after use, override.
        /// </summary>
        /// <param name="tokenLevel"></param>
        public virtual void UseTokens(int tokenLevel, int tokenCost)
        {
            if (tokenCost > tokenTotal)
            {
                return;
            }

            Dbg.Trace("Using token level " + tokenLevel);
            Manager.ButtonManager.SetButtonTypeInteractable(ButtonType.UseToken, false);
            selectedToken = tokenLevel;
            tokenInUse = true;

            tokenTotal -= tokenCost;

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.TokenBalance, tokenTotal, currencify: false);

            Manager.Codespace.Link.PostSpendTokensCommand((uint)tokenCost);

            TokenSpendEvent?.Invoke(this, new TokenSpendArgs(tokenLevel, tokenCost));

            //CheckTokenButtonsAvailable();
        }

        /// <summary>
        /// Checks if token buttons are available to be used based on whether their token cost is less than or equal to the available tokens.
        /// <para>In practice there are often more complicated criteria to decide if a given token button is available - this will get overriden a lot.</para>
        /// </summary>
        public virtual void CheckTokenButtonsAvailable()
        {
            List<GCGDKButton> tokenButtons = Manager.ButtonManager.GetButtonsOfType(ButtonType.UseToken);

            for (int i = 0; i < tokenButtons.Count; i++)
            {
                tokenButtons[i].GetComponent<Button>().interactable = tokenButtons[i].tokenCost <= tokenTotal;
            }
        }

        /// <summary>
        /// Changes the token total displayed in the GCUI.
        /// This is called by the Codespace class when the token amount is updated. Should not be called outside of that.
        /// </summary>
        public virtual void CodespaceTokenTotalChanged()
        {
            tokenTotal = Manager.Codespace.Link.progressiveTokens;

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.TokenBalance, tokenTotal, currencify: false);

            //CheckTokenButtonsAvailable();
        }

        /// <summary>
        /// Changes the cost of all the token levels - used when token cost is tied to bet value.
        /// </summary>
        /// <param name="newCost"></param>
        public virtual void ChangeTokenCost(int[] newCost)
        {
            if (newCost.Length != Manager.tokenLevelCost.Length)
            {
                Dbg.Trace("ERROR: New token cost array must be same length as existing array");
                return;
            }

            Manager.tokenLevelCost = newCost;

            List<GCGDKButton> tokenButtons = Manager.ButtonManager.GetButtonsOfType(ButtonType.UseToken);

            foreach (GCGDKButton tokenButton in tokenButtons)
            {
                StartCoroutine(tokenButton.TokenCostText());
            }
        }

        /// <summary>
        /// Pays out the miss pool. This depends dramatically on how the math model for a given game functions.
        /// This code works for SC2, but will probably need to be overridden for anything else.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerator PayMissPool()
        {
            yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState == 4);

            Manager.Codespace.Link.PostRedeemPoolCommand();
            Manager.Codespace.Link.PostPlayZeroGameCommand();

            yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState == 6);

            Manager.Codespace.Link.PostSnapShotCommand();

            CheckTokenButtonsAvailable();
        }
    }
}
