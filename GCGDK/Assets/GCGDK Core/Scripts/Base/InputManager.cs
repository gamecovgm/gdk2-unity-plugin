﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using GCGDK;
using System.IO;

#if GC_USE_XINPUT
using XInputDotNetPure;
#endif


namespace GCGDK
{
    public delegate void ControllerButtonEventHandler(object source, ControllerButtonArgs e);
    public delegate void ControllerMoveEventHandler(object source, MoveArgs e);

    public class ControllerButtonArgs : System.EventArgs
    {
        public ControllerButtons button;
        public bool buttonPressed;
        public ControllerButtonArgs(ControllerButtons thisButton, bool pressed)
        {
            button = thisButton;
            buttonPressed = pressed;
        }
    }

    public class MoveArgs : System.EventArgs
    {
        public Directions direction;
        public MoveArgs(Directions thisDirection)
        {
            direction = thisDirection;
        }
    }

    [DisallowMultipleComponent]
    public class InputManager : MonoBehaviour
    {
        /*public enum BTButtonLight { Bottom1 = 10, Bottom2 = 11, Bottom3 = 12, Bottom4 = 13, Bottom5 = 99, BigButton = 16 }

        public BartopButtonLight bottom1Light;
        public BartopButtonLight bottom2Light;
        public BartopButtonLight bottom3Light;
        public BartopButtonLight bottom4Light;
        public BartopButtonLight bottom5Light;
        public BartopButtonLight bigButtonLight;

        /// <summary>
        /// A class to manage the button lights on the Bartop unit
        /// </summary>
        public class BartopButtonLight
        {
            public bool on;
            public BTButtonLight thisButton;
            Manager Manager;
            bool blinking;

            public BartopButtonLight(bool turnOn, BTButtonLight button, Manager manager)
            {
                Manager = manager;

                thisButton = button;

                if (thisButton == BTButtonLight.Bottom5)
                {
                    if (turnOn)
                    {
                        Manager.Codespace.Link.PostEnableGFEButtonCommand(1);
                        on = true;
                    }
                    else
                    {
                        Manager.Codespace.Link.PostDisableGFEButtonCommand(1);
                        on = false;
                    }

                    return;
                }

                if (turnOn)
                {
                    Manager.Codespace.Link.PostButtonLightOnCommand((uint)button);
                    on = true;
                }
                else
                {
                    Manager.Codespace.Link.PostButtonLightOffCommand((uint)button);
                    on = false;
                }
            }

            public void Light(bool turnOn)
            {
                if (turnOn == on && !blinking) { return; }

                Manager.StartCoroutine(DoLight(turnOn));
            }

            public void Blink(bool blink, float interval = 0.5f)
            {
                if (blinking == blink) { return; }

                if (blink)
                {
                    blinking = true;
                    Manager.StartCoroutine(DoBlink(interval));
                }
                else
                {
                    blinking = false;
                }
            }

            IEnumerator DoLight(bool turnOn)
            {
                blinking = false;

                yield return null;

                if (thisButton == BTButtonLight.Bottom5)
                {
                    if (turnOn)
                    {
                        Manager.Codespace.Link.PostEnableGFEButtonCommand(1);
                        on = true;
                    }
                    else
                    {
                        Manager.Codespace.Link.PostDisableGFEButtonCommand(1);
                        on = false;
                    }
                    yield break;
                }

                if (turnOn)
                {
                    Manager.Codespace.Link.PostButtonLightOnCommand((uint)thisButton);
                    on = true;
                }
                else
                {
                    Manager.Codespace.Link.PostButtonLightOffCommand((uint)thisButton);
                    on = false;
                }
            }

            IEnumerator DoBlink(float interval)
            {
                float timer = 0;

                while (blinking)
                {
                    Light(!on);

                    while (blinking && timer < interval)
                    {
                        timer += Time.deltaTime;
                        yield return null;
                    }

                    if (blinking) { timer = 0; }
                }
            }
        } */

        [HideInInspector]
        public Manager Manager;

        /*private bool moveCleared = true;

        private bool buttonPushed = false;
        private bool prevButtonPushed;

        private bool controllerAttached = true;

        private float horizValue;
        private float vertValue; */

        private bool doBlinking;

        public event ControllerButtonEventHandler ButtonEvent;
        public event ControllerMoveEventHandler MoveEvent;

        private bool playerIndexSet = false;
#if GC_USE_XINPUT
        public PlayerIndex playerIndex;
        [SerializeField]
        public GamePadState state;
        public GamePadState prevState;
#endif
        void Awake()
        {
            Manager = GetComponent<Manager>();

            /*bottom1Light = new BartopButtonLight(false, BTButtonLight.Bottom1, Manager);
            bottom2Light = new BartopButtonLight(false, BTButtonLight.Bottom2, Manager);
            bottom3Light = new BartopButtonLight(false, BTButtonLight.Bottom3, Manager);
            bottom4Light = new BartopButtonLight(false, BTButtonLight.Bottom4, Manager);
            bottom5Light = new BartopButtonLight(false, BTButtonLight.Bottom5, Manager);
            bigButtonLight = new BartopButtonLight(false, BTButtonLight.BigButton, Manager); */
        }

        // Use this for initialization
        void Start()
        {
            Manager.Codespace.Link.PostJoyClearCommand();
        }

        /// <summary>
        /// Turn on or off the controller vibration (if supported).
        /// </summary>
        /// <param name="on"></param>
        internal void Vibrate(bool on = true)
        {
            int value = true ? 255 : 0;
#if GC_USE_XINPUT
            GamePad.SetVibration(playerIndex, value, value);
#endif
        } 

        /// <summary>
        /// We use the vibration API to control the light blinking in some cases. This sets up a blink.
        /// </summary>
        /// <param name="interval"></param>
        /// <param name="go"></param>
        public virtual void ControllerBlink(float interval = 0.35f, bool go = true)
        {
            StartCoroutine(BlinkLight(interval, go));
        } 

        // We are using the vibration command to control the LED in the controller: 0 = off, 255 = full on
        IEnumerator BlinkLight(float interval, bool go)
        {
            doBlinking = false;

            yield return null;

            if (!go) yield break;

            yield return null;

            doBlinking = true;

            float timer = 0;

            while (doBlinking)
            {
#if GC_USE_XINPUT
                GamePad.SetVibration(playerIndex, 255, 255);
#endif
                timer = 0;

                while (timer <= interval && doBlinking)
                {
                    timer += Time.deltaTime;
                    yield return null;
                }
#if GC_USE_XINPUT
                GamePad.SetVibration(playerIndex, 0, 0);
#endif
                timer = 0;

                while (timer <= interval && doBlinking)
                {
                    timer += Time.deltaTime;
                    yield return null;
                }
            }
        }

        /// <summary>
        /// Often the Cole Kepro cabinets need to have their touch calibration reset after being opened and closed.
        /// We can do this automatically using Tilt messages in Codespace.
        /// This can be called in case we are using a demo mode with no Codespace and it needs to be done manually.
        /// </summary>
        public virtual void ResetMonitorTouch()
        {
            if (!File.Exists(@"C:/devcon.exe")) { return; }

            Process touchCFG = new Process();

            touchCFG.StartInfo.FileName = "C:/devcon.exe";
            touchCFG.StartInfo.Arguments = @"restart USB\Class_FF";
            touchCFG.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            touchCFG.StartInfo.Verb = "runas";
            touchCFG.Start();
        }

        public void OnQuit()
        {
#if GC_USE_XINPUT
            GamePad.SetVibration(playerIndex, 0, 0);
#endif
        }

        public virtual void AnyButtonPressed()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.Any, true));
            }
        }

        public virtual void PressA()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.A, true));
            }
        }
        public virtual void PressB()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.B, true));
            }
        }
        public virtual void PressX()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.X, true));
            }
        }
        public virtual void PressY()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.Y, true));
            }
        }

        public virtual void PressLeftShoulder()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.LeftShoulder, true));
            }
        }

        public virtual void PressRightShoulder()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.RightShoulder, true));
            }
        }

        public virtual void PressLeftStick()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.LeftStick, true));
            }
        }

        public virtual void PressRightStick()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.RightStick, true));
            }
        }
        public virtual void PressStart()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.Start, true));
            }
        }

        public virtual void PressGuide()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.Guide, true));
            }
        }

        public virtual void PressBack()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.Back, true));
            }
        }

        public virtual void PressLeftTrigger()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.LeftTrigger, true));
            }
        }

        public virtual void PressRightTrigger()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.RightTrigger, true));
            }
        }

        public virtual void ReleaseA()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.A, false));
            }
        }
        public virtual void ReleaseB()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.B, false));
            }
        }
        public virtual void ReleaseX()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.X, false));
            }
        }
        public virtual void ReleaseY()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.Y, false));
            }
        }

        public virtual void ReleaseLeftShoulder()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.LeftShoulder, false));
            }
        }

        public virtual void ReleaseRightShoulder()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.RightShoulder, false));
            }
        }

        public virtual void ReleaseLeftStick()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.LeftStick, false));
            }
        }

        public virtual void ReleaseRightStick()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.RightStick, false));
            }
        }
        public virtual void ReleaseStart()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.Start, false));
            }
        }
        public virtual void ReleaseGuide()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.Guide, false));
            }
        }

        public virtual void ReleaseBack()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.Back, false));
            }
        }

        public virtual void ReleaseLeftTrigger()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.LeftTrigger, false));
            }
        }

        public virtual void ReleaseRightTrigger()
        {
            if (ButtonEvent != null)
            {
                ButtonEvent(this, new ControllerButtonArgs(ControllerButtons.RightTrigger, false));
            }
        }

        public virtual void MoveLeft()
        {
            if (MoveEvent != null)
            {
                MoveEvent(this, new MoveArgs(Directions.Left));
            }
        }

        public virtual void MoveRight()
        {
            if (MoveEvent != null)
            {
                MoveEvent(this, new MoveArgs(Directions.Right));
            }
        }

        public virtual void MoveUp()
        {
            if (MoveEvent != null)
            {
                MoveEvent(this, new MoveArgs(Directions.Up));
            }
        }

        public virtual void MoveDown()
        {
            if (MoveEvent != null)
            {
                MoveEvent(this, new MoveArgs(Directions.Down));
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.L))
            {
                ResetMonitorTouch();
            }

            if (Input.GetKey(KeyCode.Escape))
            {
                //Manager.Quit();
                Application.Quit();
            }

            if ((Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl)) && Input.GetKeyDown(KeyCode.M))
            {
                if (Manager.StateManager)
                {
                    if (Manager.StateManager.State == State.InGame && Manager.currentMathModel == "8002")
                    {
                        Manager.Emulation.Emulate();
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                Manager.WindowManager.ShowMouse();
            }

            if (Input.GetKeyDown(KeyCode.H))
            {
                Manager.WindowManager.HideMouse();
            }

#if GC_USE_XINPUT
            if (Manager.Settings.useController)
            {
                if (!playerIndexSet || !prevState.IsConnected)
                {
                    for (int i = 0; i < 4; ++i)
                    {
                        PlayerIndex testPlayerIndex = (PlayerIndex)i;
                        GamePadState testState = GamePad.GetState(testPlayerIndex);
                        if (testState.IsConnected)
                        {
                            UnityEngine.Debug.Log(string.Format("Controller found {0}", testPlayerIndex));
                            playerIndex = testPlayerIndex;
                            playerIndexSet = true;
                        }
                    }
                }

                prevState = state;
                state = GamePad.GetState(playerIndex);

//#if !UNITY_EDITOR
            if (state.IsConnected && !controllerAttached)
            {
                controllerAttached = true;
                Manager.Codespace.Link.PostJoyClearCommand();
            }


            if (!state.IsConnected && controllerAttached)
            {
                controllerAttached = false;
                Manager.Codespace.Link.PostJoyErrorCommand();
                Dbg.Trace("NO CONTROLLER");
            }
//#endif

                if (state.IsConnected)
                {
                    prevButtonPushed = buttonPushed;

                    if (state.Buttons.A == ButtonState.Pressed ||
                        state.Buttons.B == ButtonState.Pressed ||
                        state.Buttons.X == ButtonState.Pressed ||
                        state.Buttons.Y == ButtonState.Pressed ||
                        state.Buttons.LeftShoulder == ButtonState.Pressed ||
                        state.Buttons.RightShoulder == ButtonState.Pressed ||
                        state.Buttons.LeftStick == ButtonState.Pressed ||
                        state.Buttons.RightStick == ButtonState.Pressed ||
                        state.Buttons.Back == ButtonState.Pressed ||
                        state.Buttons.Guide == ButtonState.Pressed ||
                        state.Buttons.Start == ButtonState.Pressed ||
                        state.Triggers.Left > 0 ||
                        state.Triggers.Right > 0
                        )
                    {
                        buttonPushed = true;
                    }
                    else
                    {
                        buttonPushed = false;
                    }

                    if (!prevButtonPushed && buttonPushed)
                    {
                        AnyButtonPressed();
                    }

                    if (prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Pressed)
                    {
                        PressA();
                    }

                    if (prevState.Buttons.A == ButtonState.Pressed && state.Buttons.A == ButtonState.Released)
                    {
                        ReleaseA();
                    }

                    if (prevState.Buttons.B == ButtonState.Released && state.Buttons.B == ButtonState.Pressed)
                    {
                        PressB();
                    }

                    if (prevState.Buttons.B == ButtonState.Pressed && state.Buttons.B == ButtonState.Released)
                    {
                        ReleaseB();
                    }

                    if (prevState.Buttons.X == ButtonState.Released && state.Buttons.X == ButtonState.Pressed)
                    {
                        PressX();
                    }

                    if (prevState.Buttons.X == ButtonState.Pressed && state.Buttons.X == ButtonState.Released)
                    {
                        ReleaseX();
                    }
                    if (prevState.Buttons.Y == ButtonState.Released && state.Buttons.Y == ButtonState.Pressed)
                    {
                        PressY();
                    }

                    if (prevState.Buttons.Y == ButtonState.Pressed && state.Buttons.Y == ButtonState.Released)
                    {
                        ReleaseY();
                    }

                    if (prevState.Buttons.LeftShoulder == ButtonState.Released && state.Buttons.LeftShoulder == ButtonState.Pressed)
                    {
                        PressLeftShoulder();
                    }

                    if (prevState.Buttons.LeftShoulder == ButtonState.Pressed && state.Buttons.LeftShoulder == ButtonState.Released)
                    {
                        ReleaseLeftShoulder();
                    }

                    if (prevState.Buttons.RightShoulder == ButtonState.Released && state.Buttons.RightShoulder == ButtonState.Pressed)
                    {
                        PressRightShoulder();
                    }

                    if (prevState.Buttons.RightShoulder == ButtonState.Pressed && state.Buttons.RightShoulder == ButtonState.Released)
                    {
                        ReleaseRightShoulder();
                    }
                    if (prevState.Buttons.LeftStick == ButtonState.Released && state.Buttons.LeftStick == ButtonState.Pressed)
                    {
                        PressLeftStick();
                    }

                    if (prevState.Buttons.LeftStick == ButtonState.Pressed && state.Buttons.LeftStick == ButtonState.Released)
                    {
                        ReleaseLeftStick();
                    }

                    if (prevState.Buttons.RightStick == ButtonState.Released && state.Buttons.RightStick == ButtonState.Pressed)
                    {
                        PressRightStick();
                    }

                    if (prevState.Buttons.RightStick == ButtonState.Pressed && state.Buttons.RightStick == ButtonState.Released)
                    {
                        ReleaseRightStick();
                    }

                    if (prevState.Buttons.Start == ButtonState.Released && state.Buttons.Start == ButtonState.Pressed)
                    {
                        PressStart();
                    }

                    if (prevState.Buttons.Start == ButtonState.Pressed && state.Buttons.Start == ButtonState.Released)
                    {
                        ReleaseStart();
                    }

                    if (prevState.Buttons.Guide == ButtonState.Released && state.Buttons.Guide == ButtonState.Pressed)
                    {
                        PressGuide();
                    }

                    if (prevState.Buttons.Guide == ButtonState.Pressed && state.Buttons.Guide == ButtonState.Released)
                    {
                        ReleaseGuide();
                    }

                    if (prevState.Buttons.Back == ButtonState.Released && state.Buttons.Back == ButtonState.Pressed)
                    {
                        PressBack();
                    }

                    if (prevState.Buttons.Back == ButtonState.Pressed && state.Buttons.Back == ButtonState.Released)
                    {
                        ReleaseBack();
                    }

                    if (prevState.Triggers.Left == 0 && state.Triggers.Left > 0)
                    {
                        PressLeftTrigger();
                    }

                    if (prevState.Triggers.Left > 0 && state.Triggers.Left == 0)
                    {
                        ReleaseLeftTrigger();
                    }

                    if (prevState.Triggers.Right == 0 && state.Triggers.Right > 0)
                    {
                        PressRightTrigger();
                    }

                    if (prevState.Triggers.Right > 0 && state.Triggers.Right == 0)
                    {
                        ReleaseRightTrigger();
                    }

                    horizValue = state.ThumbSticks.Left.X;
                    vertValue = state.ThumbSticks.Right.Y;

                    if (state.ThumbSticks.Left.X > 0 || state.ThumbSticks.Right.X > 0 || (prevState.DPad.Right == ButtonState.Released && state.DPad.Right == ButtonState.Pressed))
                    {
                        if (moveCleared)
                        {
                            MoveRight();
                        }

                        moveCleared = false;
                    }
                    else if (state.ThumbSticks.Left.X < 0 || state.ThumbSticks.Right.X < 0 || (prevState.DPad.Left == ButtonState.Released && state.DPad.Left == ButtonState.Pressed))
                    {
                        if (moveCleared)
                        {
                            MoveLeft();
                        }
                        moveCleared = false;
                    }
                    else if (state.ThumbSticks.Left.Y > 0 || state.ThumbSticks.Right.Y > 0 || (prevState.DPad.Up == ButtonState.Released && state.DPad.Up == ButtonState.Pressed))
                    {
                        if (moveCleared)
                        {
                            MoveUp();
                        }
                        moveCleared = false;
                    }
                    else if (state.ThumbSticks.Left.Y < 0 || state.ThumbSticks.Right.Y < 0 || (prevState.DPad.Down == ButtonState.Released && state.DPad.Down == ButtonState.Pressed))
                    {
                        if (moveCleared)
                        {
                            MoveDown();
                        }
                        moveCleared = false;
                    }
                    else
                    {
                        moveCleared = true;
                    }
                }      
            }

#endif   
        }
    }
}
