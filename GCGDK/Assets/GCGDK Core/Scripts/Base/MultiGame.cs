﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCGDK;
using IniParser;
using IniParser.Model;
using System.Diagnostics;
using System;
using System.IO;
using System.Text;

    public class INIFileIinfo
    {
        public GCUI GCUIData = new GCUI();
        public Cart CartData = new Cart();

        public struct GCUI
        {
            public string artifactName;
            public string artifactType;
        }

        public struct Cart
        {
            public string name;
            public string path;
            public string artifactName;
            public string artifactType;
            public string commandLineParameters;
        }
    }

/// <summary>
/// A class for handling necessary modifications and functions when the GCUI is part of a MultiGame setup (EG a bartop)
/// </summary>
[DisallowMultipleComponent]
public class MultiGame : MonoBehaviour
    {
        [HideInInspector]
        public Manager Manager;

        INIFileIinfo gameInfo;

        List<GCGDKButton> multiGameButtons;

        float attractTimeOut;

        bool inited;
    bool menuLaunched;

    bool isCartridge;

    Process cartridge;

    //public GameObject[] multiGameActivate;

    void Awake()
        {
            Manager = GetComponent<Manager>();
        }

        private void Start()
        {
            Manager.InputManager.ButtonEvent += new ControllerButtonEventHandler(ControllerButtonPressed);
        }

        internal void Init()
        {
            Manager.ButtonManager.SetButtonTypeVisible(ButtonType.MultiGameExit, Manager.isMultiGame, gameObject: true);

            if (Manager.isMultiGameMenu)
            {
                return;
            }

            multiGameButtons = Manager.ButtonManager.GetButtonsOfType(ButtonType.MultiGameExit);

            ParseIni();

            LaunchCartridge();

            inited = true;
        }

        private IEnumerator WaitForArtifact()
        {
            //#if !UNITY_EDITOR

            FileStream stream = null;

            bool available = false;

            Dbg.Trace("Checking if File Exists");

            yield return new WaitUntil(() => File.Exists(gameInfo.GCUIData.artifactName));

            FileInfo file = new FileInfo(gameInfo.GCUIData.artifactName);

            Dbg.Trace("File Exists");

            while (!available)
            {
                yield return null;

                try
                {
                    stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);

                    available = true;
                }
                catch (Exception e)
                {
                    Dbg.Trace(e.ToString());
                    Dbg.Trace("File Unavailable");
                    available = false;
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }

            Dbg.Trace("File Available");
            //#endif

            CreateArtifact(gameInfo.GCUIData.artifactName);
            yield break;
        }

        private void ParseIni()
        {
            var parser = new FileIniDataParser();
            IniData data;

            Dbg.Trace(Path.GetFullPath(@"../game.ini"));

#if !UNITY_EDITOR
            data = parser.ReadFile(Path.GetFullPath(@"../game.ini"));
#else
            data = parser.ReadFile("Assets/dummy.ini");
#endif

        string test;

        if (data.TryGetKey("Cartridge", out test))
        {
            Dbg.Trace("Cartridge!");
            isCartridge = true;
        }

        gameInfo = new INIFileIinfo
            {
                GCUIData = new INIFileIinfo.GCUI()
                {
                    artifactName = data["GCUI"]["ArtifactName"],
                    artifactType = data["GCUI"]["ArtifactType"],
                },
                CartData = new INIFileIinfo.Cart()
                {
                    name = data["Cartridge"]["Name"],
                    path = data["Cartridge"]["Path"],
                    artifactName = data["Cartridge"]["ArtifactName"],
                    artifactType = data["Cartridge"]["ArtifactType"],

                    commandLineParameters = data["Cartridge"]["CommandLineParameters"]
                }
            };

            StartCoroutine(WaitForArtifact());
        }

        private void LaunchMenu()
        {
        if (menuLaunched) { return; }

        menuLaunched = true;
            try
            {
                using (Process myProcess = new Process())
                {
                    myProcess.StartInfo.UseShellExecute = false;
                    myProcess.StartInfo.FileName = @"C:/MultiGameMenu/MultiGameMenu.exe";
                    myProcess.StartInfo.CreateNoWindow = true;
                    myProcess.StartInfo.Arguments = "-popupwindow";
                    myProcess.Start();
                }

                CreateArtifact(@"C:/MultiGameMenu/menu.txt", "loading");
            }
            catch (Exception e)
            {
                Dbg.Trace(e.Message);
            }
        }

        private void LaunchCartridge()
        {

        if (!isCartridge)
        {
            return;
        }

        /*if (gameInfo.CartData.name == "")
            {
                return;
            } */

            try
            {
                using (Process myProcess = new Process())
                {
                    myProcess.StartInfo.UseShellExecute = false;
                    myProcess.StartInfo.FileName = Path.Combine(gameInfo.CartData.path, gameInfo.CartData.path);
                    myProcess.StartInfo.CreateNoWindow = true;
                    myProcess.StartInfo.Arguments = gameInfo.CartData.commandLineParameters;
                    myProcess.Start();
                }

                CreateArtifact(gameInfo.CartData.artifactName, "loading");
            }
            catch (Exception e)
            {
                Dbg.Trace(e.Message);
            }
        }

        void CreateArtifact(string artifactName, string content = "running")
        {
            DeleteArtifact(artifactName);

            using (FileStream fs = File.Create(artifactName))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(content);
                fs.Write(info, 0, info.Length);
            }
        }

        void DeleteArtifact(string artifactName)
        {
            if (File.Exists(artifactName))
            {
                try
                {
                    File.Delete(artifactName);
                }
                catch (Exception e)
                {
                    Dbg.Trace(e.ToString());
                }
            }
        }

    public virtual void KillCartridge()
    {
        cartridge.Kill();
        DeleteArtifact(gameInfo.CartData.artifactName);
    }

    /// <summary>
    /// Go back to main menu. Triggered by button.
    /// </summary>
    public virtual void BackToMain()
        {
        if (Manager.Codespace.Link.codespaceState != 4) { return; }

        if (!Manager.isMultiGameMenu)
            {
                LaunchMenu();
            }

        if (isCartridge)
        {
            KillCartridge();
        }

        Manager.Quit();
        }

        internal void ControllerButtonPressed(object source, ControllerButtonArgs e)
        {
            if (Manager.WindowManager.dualMonitor) { return; }
        //if (!e.buttonPressed || Manager.WindowManager.cabinet != CabinetType.Bartop || Manager.isMultiGameMenu) { return; }

        switch (e.button)
            {
                case ControllerButtons.Any:
                    break;
                case ControllerButtons.A:
                    break;
                case ControllerButtons.B:
                    break;
                case ControllerButtons.X:
                    break;
                case ControllerButtons.Y:
                    if (Manager.ButtonManager.GetButtonsOfType(ButtonType.BetDown)[0].interactable)
                        Manager.ButtonManager.DoButtonAction(buttonType: ButtonType.BetDown);
                    break;
                case ControllerButtons.LeftShoulder:
                    break;
                case ControllerButtons.RightShoulder:
                    break;
                case ControllerButtons.LeftStick:
                    break;
                case ControllerButtons.RightStick:
                    break;
                case ControllerButtons.Start:
                    BackToMain();
                    break;
                case ControllerButtons.Guide:
                    BackToMain();
                    break;
                case ControllerButtons.Back:
                    BackToMain();
                    break;
                case ControllerButtons.LeftTrigger:
                    break;
                case ControllerButtons.RightTrigger:
                    break;
                default:
                    break;
            }
        }

    /// <summary>
    /// Called in Update. Check to see if the game should timeout back to the main menu. Default condition is if the game has been in Attract mode for 30 seconds. Override to change.
    /// </summary>
    internal void CheckForTimeout()
    {
#if !UNITY_EDITOR
            if (Manager.StateManager.State == State.Attract)
            {
                attractTimeOut += Time.deltaTime;

                if (attractTimeOut > 30)
                {
                    BackToMain();
                }
            }
            else
            {
                attractTimeOut = 0;
            }
#endif
    }

    // Update is called once per frame
    void Update()
        {
            if (!Manager.isMultiGame || !inited || Manager.isMultiGameMenu) { return; }

            Manager.ButtonManager.SetButtonTypeInteractable(ButtonType.MultiGameExit, Manager.Codespace.Link.codespaceState == 4);

        CheckForTimeout();
    }

        public void OnQuit()
        {
            if (Manager.isMultiGame && !Manager.isMultiGameMenu)
            {
                DeleteArtifact(gameInfo.GCUIData.artifactName);
            }
        }
    }