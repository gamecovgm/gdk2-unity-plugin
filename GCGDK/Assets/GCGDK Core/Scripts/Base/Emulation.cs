﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;
using GCGDK;

namespace GCGDK
{
    /// <summary>
    /// This class launches the interface that allows the user to emulate a specific result. The way emulation works can vary widely with each math model - this version is based on the NBN2 math.
    /// </summary>
    [DisallowMultipleComponent]
    public class Emulation : MonoBehaviour
    {
        [HideInInspector]
        public Manager Manager;
        PlayLoop playLoop;

        [SerializeField]
        internal string iniFile = "C:/CSGCore/bin/Math/Emulation.ini";
        private StreamWriter OutputStream;

        internal Toggle bonusRoundToggle, miniGameToggle, progressiveToggle, cashToggle, tokensToggle;
        internal Dropdown cashDropdown, tokenDropdown, miniGameRoundsDropdown, bonusRoundsDropdown;

        private bool doMiniGame, doBonus, doCash, doTokens;

        internal List<Transform> bonusDropdownsTier1 = new List<Transform>();
        internal List<Transform> bonusDropdownsTier2 = new List<Transform>();
        internal List<Transform> bonusDropdownsTier3 = new List<Transform>();
        internal List<Transform> miniGameDropdowns = new List<Transform>();

        internal int chosenDistribution = 0;
        internal int chosenTokens = 0;
        internal int chosenMap = 0;
        internal int chosenBonus = 0; 

        List<string> multipliers;
        internal string[] lines;
        int pickedTile;
        int numRounds;
        int[] bonusRoundPicks = new int[10];

        internal List<string> bonusRoundsList = new List<string> { "3", "4", "5", "6", "7", "8", "9", "10" };
        internal List<string> bonusDropdownOptionsList = new List<string> { "1.5x", "2.3x", "2.5x", "4x", "5.5x" };
        internal List<string> minigameRoundsList = new List<string> { "2", "3", "4", "5" };
        internal List<string> minigameFirstDropdownOptionsList = new List<string> { "1x", "1.2x", "1.5x", "1.8x", "2x", "2.5x", "3x", "4x", "5x", "6x", "8x", "10x" };
        internal List<string> minigameDropdownOptionsList = new List<string> { "1x (Final 3x)", "1.2x (Final 4x)", "1.5x (Final 5x)", "1.8x (Final 6x)", "2x (Final 7x)", "2.5x (Final 8x)", "3x (Final 10x)", "4x (Final 15x)", "5x (Final 20x)", "6x (Final 30x)", "8x (Final 40x)", "10x (Final 50x)" };
        internal List<string> cashMultiplierList = new List<string> { "-Multiplier-", "0.1x", "0.2x", "0.3x", "0.4x", "0.5x", "0.6x", "0.7x", "0.8x", "0.9x", "1x", "1.2x", "1.5x", "1.6x", "1.8x", "1.9x", "2x", "2.1x", "2.2x", "2.3x", "2.5x", "2.6x", "3x", "3.3x", "3.6x", "4x", "4.5x", "5x", "5.5x", "5.7x", "6x", "6.3x", "7x", "7.5x", "8x", "9x", "10x", "11x", "12x", "15x", "20x", "30x", "35x", "40x" };
        internal List<string> tokenMultiplierList = new List<string> { "-Token Win-", "1", "2", "3", "4", "5" };

        private void Awake()
        {
            Manager = GetComponent<Manager>();
            playLoop = GetComponent<PlayLoop>();
        }

        internal void Emulate()
        {
            chosenDistribution = 0;
            chosenMap = 0;
            chosenTokens = 0;
            chosenBonus = 0;
            Manager.StateManager.ChangeState(State.System, "Emulation");

            lines = new string[] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };

            bonusRoundToggle.isOn = false;
            miniGameToggle.isOn = false;
            progressiveToggle.isOn = false;
            cashToggle.isOn = false;
            tokensToggle.isOn = false;

            doMiniGame = false;
            doBonus = false;
            doCash = false;
            doTokens = false;

            if (tokenDropdown != null) { tokenDropdown.value = 0; } else { Debug.Log("TOKEN ROUNDS DROPDOWN IS MISSING"); }
            if (miniGameRoundsDropdown != null) { miniGameRoundsDropdown.value = 0; } else { Debug.Log("MINIGAME ROUNDS DROPDOWN IS MISSING"); }
            if (bonusRoundsDropdown != null) { bonusRoundsDropdown.value = 0; } else { Debug.Log("BONUS ROUNDS DROPDOWN IS MISSING"); }

            pickedTile = 99;

            MiniGameRounds(0);
            BonusRounds(0);

            CashMultiplier(0);
            TokensMultiplier(0);

            multipliers = new List<string>();

            multipliers.Add("-Multiplier-");

            for (int i = 0; i < Manager.MathModel.multipliers[0].Length; i++)
            {
                if (Manager.MathModel.multipliers[Manager.ButtonManager.activeDenom][i] > 0)
                    multipliers.Add(Manager.MathModel.multipliers[Manager.ButtonManager.activeDenom][i].ToString() + "x");
            }

            cashDropdown.ClearOptions();
            cashDropdown.AddOptions(multipliers);
            cashDropdown.value = 0;

            try
            {
                StopCoroutine(playLoop.readyCo);
                playLoop.gameInProgress = false;
            }
            catch (System.Exception)
            {

                // throw;
            }
        }

        internal void DoBonus(bool doIt)
        {
            doBonus = doIt;

            if (doIt)
            {
                lines[0] = 0.ToString();
                pickedTile = 2;
                lines[4] = 1.ToString();
                // look into getting the lowest bonus round value rather than hard coding that 3
                if (bonusRoundsDropdown != null) { BonusRounds(bonusRoundsDropdown.value); } else { BonusRounds(3); Debug.Log("BONUS ROUNDS DROPDOWN IS MISSING"); }
            }
            else
            {
                lines[4] = 0.ToString();
                pickedTile = 99;
            }
        }

        internal void DoMiniGame(bool doIt)
        {
            doMiniGame = doIt;

            if (doIt)
            {
                lines[0] = 1.ToString();
                pickedTile = 2;
                // look into getting the lowest minigame round value rather than hard coding that 2
                if (miniGameRoundsDropdown != null) { MiniGameRounds(miniGameRoundsDropdown.value); } else { MiniGameRounds(2); Debug.Log("MINIGAME ROUNDS DROPDOWN IS MISSING"); }
            }
            else
            {
                pickedTile = 99;
            }
        }

        internal void DoTokens(bool doIt)
        {
            doTokens = doIt;

            if (doIt)
            {
                lines[0] = 0.ToString();
                pickedTile = 1;
            }
            else
            {
                pickedTile = 99;
            }
        }

        internal void DoProgressive(bool doIt)
        {
            lines[5] = doIt ? 1.ToString() : 0.ToString();
        }

        internal void DoCash(bool doIt)
        {
            doCash = doIt;

            if (doIt)
            {
                lines[0] = 0.ToString();
                pickedTile = 0;
            }
            else
            {
                pickedTile = 99;
            }
        }

        internal void CashMultiplier(int pick)
        {
            pick = pick == 0 ? pick : pick - 1;
            lines[1] = (pick).ToString();
        }

        internal void TokensMultiplier(int pick)
        {
            pick = pick == 0 ? pick : pick - 1;
            lines[2] = (pick).ToString();
        }

        internal void MiniGameRounds(int pick)
        {
            numRounds = pick + 2;

            for (int i = 0; i < miniGameDropdowns.Count; i++)
            {
                miniGameDropdowns[i].GetComponent<Dropdown>().interactable = (i < numRounds);
            }

            Dbg.Trace("Rounds: " + numRounds);

            lines[3] = numRounds.ToString();
        }

        internal void BonusRounds(int pick)
        {
            numRounds = pick + 3;

            Dbg.Trace("Rounds: " + numRounds);

            for (int i = 0; i < bonusDropdownsTier1.Count; i++)
            {
                bonusDropdownsTier1[i].GetComponent<Dropdown>().interactable = (i < numRounds);
                bonusDropdownsTier2[i].GetComponent<Dropdown>().interactable = (i < numRounds);
                bonusDropdownsTier3[i].GetComponent<Dropdown>().interactable = (i < numRounds);
            }

            lines[3] = numRounds.ToString();
        }

        void MiniGamePrizes(int pick)
        {

        }

        void BonusRoundPrizes(int pick)
        {

        }

        internal virtual void Cancel()
        {
            DeleteFile();
            Manager.StateManager.ChangeState(State.InGame);
            playLoop.StartSession();
        }

        internal virtual void Set()
        {
            //base.Set();
            if (doMiniGame)
            {
                for (int i = 0; i < 5; i++)
                {
                    lines[i + 6] = (i < numRounds ? miniGameDropdowns[i].GetComponent<Dropdown>().value.ToString() : "0");
                }
            }

            if (doBonus)
            {
                for (int i = 0; i < 10; i++)
                {
                    //lines[i + 6] = bonusDropdownsTier1[i].GetComponent<TMP_Dropdown>().value.ToString() + bonusDropdownsTier2[i].GetComponent<TMP_Dropdown>().value.ToString() + bonusDropdownsTier3[i].GetComponent<TMP_Dropdown>().value.ToString("D2");
                    lines[i + 6] = bonusDropdownsTier1[i].GetComponent<Dropdown>().value.ToString() + bonusDropdownsTier2[i].GetComponent<Dropdown>().value.ToString() + bonusDropdownsTier3[i].GetComponent<Dropdown>().value.ToString("D2");
                    Dbg.Trace(lines[i + 6]);
                    //lines[i + 6] = bonusWins[(i < numRounds ? bonusDropdowns[i].GetComponent<TMP_Dropdown>().value : 0)];
                    //bonusRoundPicks[i] = bonusRounds[(i < numRounds ? bonusDropdowns[i].GetComponent<TMP_Dropdown>().value : 0)];
                }
            }

            playLoop.Emulation(pickedTile, bonusRoundPicks);

            OutputStream = new StreamWriter(iniFile, false);

            for (int i = 0; i < lines.Length; i++)
            {
                OutputStream.WriteLine(lines[i]);
            }

            OutputStream.Close();

            playLoop.StartSession();
        }

        internal virtual void DeleteFile()
        {
            if (File.Exists(iniFile))
                File.Delete(iniFile);
        }
    }
}
