﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCCL;
using System.Threading;
using TMPro;
using System.IO;
using GCGDK;

namespace GCGDK
{
    internal delegate void CodespaceBalanceEventHandler(object source, BalanceArgs e);

    internal class BalanceArgs : System.EventArgs
    {
        internal int newBalance;
        internal bool higher;
        internal BalanceArgs(int balance, bool up)
        {
            newBalance = balance;
            higher = up;
        }
    }

    internal delegate void CodespaceProgressiveEventHandler(object source, ProgressiveArgs e);

    public class ProgressiveArgs : System.EventArgs
    {
        public int mini;
        public int maxi;
        public int mega;

        public ProgressiveArgs(int newMini, int newMaxi, int newMega)
        {
            mini = newMini;
            maxi = newMaxi;
            mega = newMega;
        }
    }

    /// <summary>
    /// For tracking changes in Codespace params and initiating response where appropriate.
    /// </summary>
    [DisallowMultipleComponent]
    public class Codespace : MonoBehaviour
    {
        [HideInInspector]
        public Manager Manager;

        internal event CodespaceBalanceEventHandler BalanceChangeEvent;
        internal event CodespaceProgressiveEventHandler ProgressiveChangeEvent;

        [HideInInspector]
        internal CodespaceParams Link;
        internal int previousCodespaceState;
        internal bool tilted;
        internal bool updateBalancesAutomatically = true;
        internal bool playCountUpdated;

        // These need to be public so they can be seen by the math model custom editor script.
        [HideInInspector]
        public string selectedMathModel;
        [HideInInspector]
        public string selectedModule;

        private Coroutine cornerMessagesCoroutine;

        private int totalWin;

        internal int codespaceState = 4;
        private string codespaceStateString;
        private string configedGames;
        private string selectedGame;
        private int selectedGameIndex;

        // SS Pick Values
        private int bonus = 0;
        private int distribution = 1;
        private int map;

        private int progressiveTier;
        private int progressiveSingle;
        private int progressiveHigh;
        private int progressiveMid;
        private int progressiveLow;
        private int progressivePool;
        private int progressiveTokens;
        private System.DateTime progressiveHighDate;
        private System.DateTime progressiveMidDate;
        private System.DateTime progressiveLowDate;

        private string thisPlay;
        private int totalValue;
        private int cashableValue;
        private int restrictedValue;
        private int nonRestrictedValue;
        private string handpayValue;
        private bool handpayValueGiven = false;

        private string playID;

        private bool betSet = false;
        private bool gameTilt = false;
        private bool cashoutPressed = false;
        private bool maxBetPressed = false;
        private bool mainDoorClosed = true;

        private string tiltMessages;
        private string cornerMessages;

        private int betTier = 0;
        private int minimumBetValue = 50;
        private int maximumBetValue = 2000;
        private int betIncrement = 50;
        private int currentBet = 0;
        private string selectedDenoms;
        private string configedDenoms;
        private int maxPaylines = 100;
        private int minPaylines = 1;
        private int payLines = 50;
        private int maxCPL = 0;

        private int gameDifficulty = 0;

        private bool ssPickValuesLoaded = false;

        private int playCount = 0;

        private void Awake()
        {
            Manager = GetComponent<Manager>();
        }

        // Use this for initialization
        private void Start()
        {
            Link = GCCLController.codespaceParams;

            string GCUIversion = "_EDITOR";

#if !UNITY_EDITOR
        GCUIversion = File.ReadAllText("version.txt");
#endif

            GCCLController.gameVersion = "";
            GCCLController.GCPVersion = "GCUIv" + GCUIversion;
            Link.PostSetNameCommand();

            StartCoroutine(UpdateBonusWinTime());
            //StartCoroutine(Manager.PlayLoop.Recover8002FromInterruption());
            Manager.PlayLoop.Recovery();
        }

        public virtual IEnumerator SetAttendantMenuDisplay(string version)
        {
            yield return new WaitWhile(() => Link == null);

            GCCLController.GCPVersion = "GCUIv" + version;
            Link.PostSetNameCommand();

            Dbg.Trace("Version: " + GCCLController.GCPVersion);
        }

        /// <summary>
        /// Respond to Codespace state changes actively (ie not part of play loop).
        /// </summary>
        internal virtual void StateChanged()
        {
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.TiltHeader, Link.codespaceStateString);

            if (Link.codespaceState == 4)
            {
                // The balance updates after the state, so let's wait a frame
                StartCoroutine(Manager.WaitOneFrame(Manager.ButtonManager.UpdateBetDenomButtons));
            }

            if (tilted)
            {
                tilted = false;
                //Manager.AudioManager.SetVolume(3);
                Manager.StateManager.ChangeState(State.System, "Tilt");
                //StartCoroutine(Manager.PlayLoop.Recover8002FromInterruption());
                Manager.PlayLoop.Recovery();
                if (previousCodespaceState == 15)
                {
                    Manager.WindowManager.HideMouse();
                    Manager.WindowManager.ShowGCUIRegions();
                }
            }

            if (Link.codespaceState <= 3 || Link.codespaceState == 25 || Link.codespaceState == 26)
            {
                Manager.PlayLoop.InterruptPlay();

                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.TiltCashoutPrompt, ((Link.codespaceState == 25 || Link.codespaceState == 26) && (Link.cashableValue + Link.nonRestrictedValue) > 0) ? "Please Cash Out\nCashable Balance: " + Manager.Currencify(Link.cashableValue + Link.nonRestrictedValue) : "");
                tilted = true;
                Manager.StateManager.ChangeState(State.System, "Tilt");
                //Manager.AudioManager.SetVolume(0);
                Manager.LEDs.ChangeSequence(LEDSequence.TILT);
            }
            else if (Link.codespaceState == 15)
            {
                Manager.PlayLoop.InterruptPlay();
                Manager.WindowManager.HideGCUIRegions();
                Manager.WindowManager.HideCartridge(true);
                Manager.WindowManager.ShowMouse();
                //Manager.AudioManager.SetVolume(0);
                tilted = true;
                StartCoroutine(Manager.WindowManager.SetAttendantMenu());
                Manager.LEDs.ChangeSequence(LEDSequence.TILT);
            }

            if (Link.codespaceState == 10)
            {
                StartCoroutine(Manager.Cashout.JackpotHandpay());
            }
            else if (Link.codespaceState == 11)
            {
                StartCoroutine(Manager.Cashout.Handpay());
            }
        }

        /// <summary>
        /// Display the system messages in the top left corner - regulatory requirement.
        /// </summary>
        internal void DisplayCornerMessages()
        {
            try
            {
                StopCoroutine(cornerMessagesCoroutine);
            }
            catch (System.Exception)
            {
            }

            if (Link.cornerMessages == "")
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.CornerMessages, "");
                return;
            }

            string[] messages = Link.cornerMessages.Split(',');

            cornerMessagesCoroutine = StartCoroutine(RotateCornerMessages(messages));
        }

        /// <summary>
        /// Rotate the corner messages - show each one for three seconds, then the next
        /// </summary>
        /// <param name="messages">Array of messages</param>
        /// <returns></returns>
        internal IEnumerator RotateCornerMessages(string[] messages)
        {
            if (messages.Length > 1)
            {
                int i = 0;
                while (true)
                {
                    i = i == messages.Length ? 0 : i;
                    if (!messages[i].Contains("restricted-remaining") && !messages[i].Contains("Handpay"))
                    {
                        Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.CornerMessages, messages[i].Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator).Replace("$", Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol));
                        yield return new WaitForSeconds(5);
                    }
                    i++;
                }
            }
            else
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.CornerMessages, messages[0].Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator).Replace("$", Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol));
            }
        }

        /// <summary>
        /// Money has gone in or out, or been won or lost.
        /// </summary>
        internal void BalanceChanged()
        {
            // It makes intuitive sense to update denom buttons here, but as the balance changes after a bet is made it can lead
            // to showing the wrong bet as the game is played out. Instead, this will be checked when Codespace returns to Setup Bet (state 4)
            // Manager.ButtonManager.UpdateBetDenomButtons();

            if (Manager.StateManager.currentSubstate == "Attract")
            {
                Manager.PlayLoop.StartSession();
            }

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.CashBalance, Link.totalValue);
        }

        /// <summary>
        /// The miss pool has either been added to or won. Or reset.
        /// </summary>
        internal void PoolBalanceChanged()
        {
            if (Link.progressivePool == 0)
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MissPool, Manager.missPoolZeroText);
            }
            else
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MissPool, Link.progressivePool);
            }
        }

        /// <summary>
        /// The Mini Progressive Bonus value has been changed.
        /// </summary>
        internal void MiniValueChanged()
        {
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MiniProgressiveAmount, Link.progressiveLow);
            ProgressiveChangeEvent?.Invoke(this, new ProgressiveArgs(Link.progressiveLow, Link.progressiveMid, Link.progressiveHigh));
        }

        /// <summary>
        /// The Maxi Progressive Bonus value has been changed.
        /// </summary>
        internal void MaxiValueChanged()
        {
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MaxiProgressiveAmount, Link.progressiveMid);
            ProgressiveChangeEvent?.Invoke(this, new ProgressiveArgs(Link.progressiveLow, Link.progressiveMid, Link.progressiveHigh));
        }

        /// <summary>
        /// The Mega Progressive Bonus value has been changed.
        /// </summary>
        internal void MegaValueChanged()
        {
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MegaProgressiveAmount, Link.progressiveHigh);
            ProgressiveChangeEvent?.Invoke(this, new ProgressiveArgs(Link.progressiveLow, Link.progressiveMid, Link.progressiveHigh));
        }

        internal void SingleProgressiveValueChanged()
        {
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MegaProgressiveAmount, Link.progressiveSingle);
            ProgressiveChangeEvent?.Invoke(this, new ProgressiveArgs(Link.progressiveLow, Link.progressiveMid, Link.progressiveHigh));
        }


        /// <summary>
        /// The time since the mini bonus was last won has changed, either because it was won or time has passed.
        /// </summary>
        internal void MiniDateValueChanged()
        {
            System.TimeSpan difference = System.DateTime.Now - Link.progressiveLowDate;

            if (difference.Days > 500)
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MiniProgressiveDateWon, "");
            }
            else if (difference.Hours > 0)
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MiniProgressiveDateWon, "LAST WON " + difference.Days + " DAY" + (difference.Days == 1 ? " " : "S ") + difference.Hours + " HOUR" + (difference.Hours == 1 ? "" : "S") + " AGO");
            }
            else
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MiniProgressiveDateWon, "LAST WON WITHIN THE HOUR!");
            }
        }

        /// <summary>
        /// The time since the maxi bonus was last won has changed, either because it was won or time has passed.
        /// </summary>
        internal void MaxiDateValueChanged()
        {
            System.TimeSpan difference = System.DateTime.Now - Link.progressiveMidDate;

            if (difference.Days > 500)
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MaxiProgressiveDateWon, "");
            }
            else if (difference.Hours > 0)
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MaxiProgressiveDateWon, "LAST WON " + difference.Days + " DAY" + (difference.Days == 1 ? " " : "S ") + difference.Hours + " HOUR" + (difference.Hours == 1 ? "" : "S") + " AGO");
            }
            else
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MaxiProgressiveDateWon, "LAST WON WITHIN THE HOUR!");
            }
        }

        /// <summary>
        /// The time since the mega bonus was last won has changed, either because it was won or time has passed.
        /// </summary>
        internal void MegaDateValueChanged()
        {
            System.TimeSpan difference = System.DateTime.Now - Link.progressiveHighDate;

            if (difference.Days > 500)
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MegaProgressiveDateWon, "");
            }
            else if (difference.Hours > 0)
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MegaProgressiveDateWon, "LAST WON " + difference.Days + " DAY" + (difference.Days == 1 ? " " : "S ") + difference.Hours + " HOUR" + (difference.Hours == 1 ? "" : "S") + " AGO");
            }
            else
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MegaProgressiveDateWon, "LAST WON WITHIN THE HOUR!");
            }
        }

        /// <summary>
        /// Handpay value has changed. This value works for both normal handpay and jackpot handpay. This is always presented as a string in USD, so we change it in case we are operating in a different currency.
        /// </summary>
        internal void HandpayValueChanged()
        {
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.HandpayAmount, Link.handpayValue.Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator).Replace("$", Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol));
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.JackpotHandpayAmount, Link.handpayValue.Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator).Replace("$", Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol));
        }

        /// <summary>
        /// Tilt messages have changed, update the tilt screen. If the main door has closed, reset the touch input.
        /// </summary>
        internal void TiltMessagesChanged()
        {
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.TiltBody, Link.tiltMessages);

            if (Link.tiltMessages.Contains("MAIN DOOR CLOSED"))
            {
                Manager.InputManager.ResetMonitorTouch();
            }
        }

        /// <summary>
        /// Hourly update of the times since the bonuses were won.
        /// </summary>
        /// <returns></returns>
        internal IEnumerator UpdateBonusWinTime()
        {
            while (true)
            {
                yield return new WaitForSeconds(3600);

                MiniDateValueChanged();
                MaxiDateValueChanged();
                MegaDateValueChanged();
            }
        }

        /// <summary>
        /// Parsing the MathParams string
        /// </summary>
        internal void ParseMathParams()
        {
            if (!Link.mathParamsUpdated) { return; }

            foreach (string item in Link.mathParams)
            {
                string[] param = item.Split(':');

                if (param[0].Replace('"', char.MinValue).Trim() == "Tokens")
                {
                    Link.progressiveTokens = int.Parse(param[1].Replace('"', char.MinValue).Trim());
                    Manager.TokenManager.CodespaceTokenTotalChanged();
                }
                else if (param[0].Trim() == "Prog Wins")
                {
                    Link.progressiveHighDate = Utilities.UnixTimeStampToDateTime(int.Parse(param[1].Replace('"', char.MinValue).Trim()));
                }
            }

            // MathParams is mostly game specific
            Manager.PlayLoop.ParseMathParams();

            Link.mathParamsUpdated = false;
        }

        /// <summary>
        /// Take a snapshot after a specified period of time
        /// </summary>
        /// <param name="delay"></param>
        /// <param name="top"></param>
        /// <param name="gameList"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        /*internal IEnumerator TakeDelayedSnapshot(float delay, bool top = false, int[] gameList = null, string description = null)
        {
            yield return new WaitForSeconds(delay);

            Manager.Codespace.Link.PostLateSnapShotCommand(top, gameList, description);
        } */

        void Update()
        {
            if (totalWin != Link.totalWin)
            {
                totalWin = Link.totalWin;
            }

            if (codespaceState != Link.codespaceState)
            {
                previousCodespaceState = codespaceState;
                codespaceState = Link.codespaceState;
                Manager.CodespaceStateString = codespaceState + " (" + (CSStates)codespaceState + ")";
                Manager.CodespaceStateInt = codespaceState;
                StateChanged();
            }

            if (codespaceStateString != Link.codespaceStateString)
            {
                codespaceStateString = Link.codespaceStateString;
            }

            if (configedGames != Link.configedGames)
            {
                configedGames = Link.configedGames;
            }

            if (selectedGame != Link.selectedGame)
            {
                selectedGame = Link.selectedGame;
            }

            if (selectedGameIndex != Link.selectedGameIndex)
            {
                selectedGameIndex = Link.selectedGameIndex;
            }

            if (bonus != Link.bonus)
            {
                bonus = Link.bonus;
            }

            if (distribution != Link.distribution)
            {
                distribution = Link.distribution;
            }

            if (map != Link.map)
            {
                map = Link.map;
            }

            if (progressiveTier != Link.progressiveTier)
            {
                progressiveTier = Link.progressiveTier;
            }

            if (progressiveSingle != Link.progressiveSingle)
            {
                progressiveSingle = Link.progressiveSingle;
                SingleProgressiveValueChanged();
            }

            if (progressiveHigh != Link.progressiveHigh)
            {
                progressiveHigh = Link.progressiveHigh;
                MegaValueChanged();
            }

            if (progressiveMid != Link.progressiveMid)
            {
                progressiveMid = Link.progressiveMid;
                MaxiValueChanged();
            }

            if (progressiveLow != Link.progressiveLow)
            {
                progressiveLow = Link.progressiveLow;
                MiniValueChanged();
            }

            if (progressivePool != Link.progressivePool)
            {
                progressivePool = Link.progressivePool;
                PoolBalanceChanged();
            }

            if (progressiveTokens != Link.progressiveTokens)
            {
                if (updateBalancesAutomatically)
                {
                    Manager.TokenManager.CodespaceTokenTotalChanged();
                    progressiveTokens = Link.progressiveTokens;
                }
            }

            if (progressiveHighDate != Link.progressiveHighDate)
            {
                progressiveHighDate = Link.progressiveHighDate;
                MegaDateValueChanged();
            }

            if (progressiveMidDate != Link.progressiveMidDate)
            {
                progressiveMidDate = Link.progressiveMidDate;
                MaxiDateValueChanged();
            }

            if (progressiveLowDate != Link.progressiveLowDate)
            {
                progressiveLowDate = Link.progressiveLowDate;
                MiniDateValueChanged();
            }

            if (selectedMathModel != Link.selectedMathModel)
            {
                selectedMathModel = Link.selectedMathModel;
                Manager.currentMathModel = selectedMathModel;
            }

            if (selectedModule != Link.selectedModule)
            {
                selectedModule = Link.selectedModule;
                Manager.MathModel.Parse();
            }

            if (thisPlay != Link.thisPlay)
            {
                thisPlay = Link.thisPlay;
            }

            if (totalValue != Link.totalValue)
            {
                if (updateBalancesAutomatically)
                {
                    BalanceChangeEvent?.Invoke(this, new BalanceArgs(Link.totalValue, Link.totalValue > totalValue));

                    if (Link.totalValue > totalValue)
                    {
                        StartCoroutine(Manager.WaitOneFrame(Manager.ButtonManager.UpdateBetDenomButtons));
                    }

                    totalValue = Link.totalValue;
                    BalanceChanged();
                }
            }

            if (cashableValue != Link.cashableValue)
            {
                cashableValue = Link.cashableValue;
            }

            if (restrictedValue != Link.restrictedValue)
            {
                restrictedValue = Link.restrictedValue;
            }

            if (nonRestrictedValue != Link.nonRestrictedValue)
            {
                nonRestrictedValue = Link.nonRestrictedValue;
            }

            if (handpayValue != Link.handpayValue)
            {
                handpayValue = Link.handpayValue;
            }

            if (handpayValueGiven != Link.handpayValueGiven)
            {
                HandpayValueChanged();
                Link.handpayValueGiven = false;
            }

            if (playID != Link.playID)
            {
                playID = Link.playID;
            }

            if (betSet != Link.betSet)
            {
                betSet = Link.betSet;
            }

            if (gameTilt != Link.gameTilt)
            {
                gameTilt = Link.gameTilt;
            }

            if (cashoutPressed != Link.cashoutPressed)
            {
                Link.cashoutPressed = false;
                Link.PostClearCashoutCommand();
                Manager.ButtonManager.DoButtonAction(buttonType: ButtonType.Cashout);
            }

            if (maxBetPressed != Link.maxBetPressed)
            {
                Link.maxBetPressed = false;
                Link.PostClearMaxbetCommand();
                Manager.ButtonManager.DoButtonAction(buttonType: ButtonType.MaxBet);
            }

            if (mainDoorClosed != Link.mainDoorClosed)
            {
                mainDoorClosed = Link.mainDoorClosed;
            }

            if (tiltMessages != Link.tiltMessages)
            {
                tiltMessages = Link.tiltMessages;
                TiltMessagesChanged();
            }

            if (cornerMessages != Link.cornerMessages)
            {
                cornerMessages = Link.cornerMessages;
                DisplayCornerMessages();
            }

            if (betTier != Link.betTier)
            {
                betTier = Link.betTier;
            }

            if (minimumBetValue != Link.minimumBetValue)
            {
                minimumBetValue = Link.minimumBetValue;
            }

            if (maximumBetValue != Link.maximumBetValue)
            {
                maximumBetValue = Link.maximumBetValue;
            }

            if (betIncrement != Link.betIncrement)
            {
                betIncrement = Link.betIncrement;
            }

            if (currentBet != Link.currentBet)
            {
                currentBet = Link.currentBet;
            }

            if (selectedDenoms != Link.selectedDenoms)
            {
                selectedDenoms = Link.selectedDenoms;
            }

            if (configedDenoms != Link.configedDenoms)
            {
                configedDenoms = Link.configedDenoms;
            }

            if (maxPaylines != Link.maxPaylines)
            {
                maxPaylines = Link.maxPaylines;
                maximumBetValue = Link.maxCPL * Link.maxPaylines;
            }

            if (minPaylines != Link.minPaylines)
            {
                minPaylines = Link.minPaylines;
            }

            if (payLines != Link.payLines)
            {
                payLines = Link.payLines;
            }

            if (maxCPL != Link.maxCPL)
            {
                maxCPL = Link.maxCPL;
                maximumBetValue = Link.maxCPL * Link.maxPaylines;
            }

            if (ssPickValuesLoaded != Link.ssPickValuesLoaded)
            {
                ssPickValuesLoaded = Link.ssPickValuesLoaded;
            }

            if (Link.mathParamsUpdated)
            {
                ParseMathParams();
            }

            if (playCount != Link.playCount)
            {
                playCount = Link.playCount;
                playCountUpdated = true;
            }

            //Manager.buttonsAvailable = codespaceState == 4 || codespaceState == 25 || codespaceState == 26;
        }
    }
}
