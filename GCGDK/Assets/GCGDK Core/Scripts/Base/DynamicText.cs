﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCGDK;

namespace GCGDK
{
    [DisallowMultipleComponent]
    public class DynamicText : MonoBehaviour
    {

        [HideInInspector]
        public Manager Manager;

        GCGDKText[] allDynamicTextArray;
        internal List<GCGDKText> populatedTextList = new List<GCGDKText>();

        private void Awake()
        {
            Manager = GetComponent<Manager>();

            if (GameObject.Find("GCGDK"))
            {
                allDynamicTextArray = GameObject.Find("GCGDK").GetComponentsInChildren<GCGDKText>(true);
            }
            else
            {
                allDynamicTextArray = Resources.FindObjectsOfTypeAll<GCGDKText>();
                Debug.Log("DynamicText couldn't locate the GCGDK object, and performed 'FindObjectsOfTypeAll'. Length of array is " + allDynamicTextArray.Length);
            }
        }


        /// <summary>
        /// Updates all text objects in the target DynamicTextCategory to newText instantly.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="newText"></param>
        public void UpdateDynamicText(DynamicTextCategory target, string newText)
        {
            foreach (GCGDKText text in allDynamicTextArray)
            {
                if (text.content == target)
                {
                    text.UpdateText(newText);
                }
            }

            if (populatedTextList.Count == 0) { return; }

            foreach(GCGDKText text in populatedTextList)
            {
                if (text != null)
                {
                    if (text.content == target)
                    {
                        text.UpdateText(newText);
                    }
                }
            }
        }

        /// <summary>
        /// Bangs currency value up (or down) over duration seconds in all text objects in the target DynamicTextCategory.
        /// </summary>
        /// <param name="newValue"></param>
        /// <param name="duration">Bang duration (default 1 second)</param>
        public void UpdateDynamicText(DynamicTextCategory target, int newValue, float duration = 99, bool currencify = true, bool bangDown = false, string preText = "", string postText = "", AudioClip bangSFX = null)
        {
            foreach (GCGDKText text in allDynamicTextArray)
            {
                if (text.content == target)
                {
                    text.UpdateText(newValue, duration, currencify, bangDown, preText, postText, bangSFX);
                }
            }

            if (populatedTextList.Count == 0) { return; }

            foreach (GCGDKText text in populatedTextList)
            {
                if (text != null)
                {
                    if (text.content == target)
                    {
                        text.UpdateText(newValue, duration, currencify, bangDown, preText, postText, bangSFX);
                    }
                }
            }
        }

        /// <summary>
        /// Interrupts text bang in all text objects of type DynamicTextCategory.target, jumps to end.
        /// </summary>
        /// <param name="target"></param>
        public void InterruptTextBang(DynamicTextCategory target)
        {
            foreach (GCGDKText text in allDynamicTextArray)
            {
                if (text.content == target)
                {
                    text.interruptBang = true;
                }
            }

            if (populatedTextList.Count == 0) { return; }

            foreach (GCGDKText text in populatedTextList)
            {
                if (text != null)
                {
                    if (text.content == target)
                    {
                        text.interruptBang = true;
                    }
                }
            }
        }

        /// <summary>
        /// Returns true if any text object of type DynamicTextCategory.target are currently banging.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        /*public bool IsBanging(DynamicTextCategory target)
        {
            bool isIt = false;

            for (int i = 0; i < allDynamicTextArray.Length; i++)
            {
                if (allDynamicTextArray[i].banging)
                {
                    isIt = true;
                    break;
                }
            } 

            return isIt;
        } */
    }
}
