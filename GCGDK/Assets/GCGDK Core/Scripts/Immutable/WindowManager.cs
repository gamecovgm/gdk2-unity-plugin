﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using GCGDK;

namespace GCGDK
{
    [DisallowMultipleComponent]
    public class WindowManager : MonoBehaviour
    {

        [HideInInspector]
        public Manager Manager;

        [Flags()]
        private enum SetWindowPosFlags : uint
        {
            /// <summary>If the calling thread and the thread that owns the window are attached to different input queues, 
            /// the system posts the request to the thread that owns the window. This prevents the calling thread from 
            /// blocking its execution while other threads process the request.</summary>
            /// <remarks>SWP_ASYNCWINDOWPOS</remarks>
            AsynchronousWindowPosition = 0x4000,
            /// <summary>Prevents generation of the WM_SYNCPAINT message.</summary>
            /// <remarks>SWP_DEFERERASE</remarks>
            DeferErase = 0x2000,
            /// <summary>Draws a frame (defined in the window's class description) around the window.</summary>
            /// <remarks>SWP_DRAWFRAME</remarks>
            DrawFrame = 0x0020,
            /// <summary>Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to 
            /// the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE 
            /// is sent only when the window's size is being changed.</summary>
            /// <remarks>SWP_FRAMECHANGED</remarks>
            FrameChanged = 0x0020,
            /// <summary>Hides the window.</summary>
            /// <remarks>SWP_HIDEWINDOW</remarks>
            HideWindow = 0x0080,
            /// <summary>Does not activate the window. If this flag is not set, the window is activated and moved to the 
            /// top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter 
            /// parameter).</summary>
            /// <remarks>SWP_NOACTIVATE</remarks>
            DoNotActivate = 0x0010,
            /// <summary>Discards the entire contents of the client area. If this flag is not specified, the valid 
            /// contents of the client area are saved and copied back into the client area after the window is sized or 
            /// repositioned.</summary>
            /// <remarks>SWP_NOCOPYBITS</remarks>
            DoNotCopyBits = 0x0100,
            /// <summary>Retains the current position (ignores X and Y parameters).</summary>
            /// <remarks>SWP_NOMOVE</remarks>
            IgnoreMove = 0x0002,
            /// <summary>Does not change the owner window's position in the Z order.</summary>
            /// <remarks>SWP_NOOWNERZORDER</remarks>
            DoNotChangeOwnerZOrder = 0x0200,
            /// <summary>Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to 
            /// the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent 
            /// window uncovered as a result of the window being moved. When this flag is set, the application must 
            /// explicitly invalidate or redraw any parts of the window and parent window that need redrawing.</summary>
            /// <remarks>SWP_NOREDRAW</remarks>
            DoNotRedraw = 0x0008,
            /// <summary>Same as the SWP_NOOWNERZORDER flag.</summary>
            /// <remarks>SWP_NOREPOSITION</remarks>
            DoNotReposition = 0x0200,
            /// <summary>Prevents the window from receiving the WM_WINDOWPOSCHANGING message.</summary>
            /// <remarks>SWP_NOSENDCHANGING</remarks>
            DoNotSendChangingEvent = 0x0400,
            /// <summary>Retains the current size (ignores the cx and cy parameters).</summary>
            /// <remarks>SWP_NOSIZE</remarks>
            IgnoreResize = 0x0001,
            /// <summary>Retains the current Z order (ignores the hWndInsertAfter parameter).</summary>
            /// <remarks>SWP_NOZORDER</remarks>
            IgnoreZOrder = 0x0004,
            /// <summary>Displays the window.</summary>
            /// <remarks>SWP_SHOWWINDOW</remarks>
            ShowWindow = 0x0040,
        }

        /// <summary>Enumeration of the different ways of showing a window using 
        /// ShowWindow</summary>
        private enum WindowShowStyle : uint
        {
            /// <summary>Hides the window and activates another window.</summary>
            /// <remarks>See SW_HIDE</remarks>
            Hide = 0,
            /// <summary>Activates and displays a window. If the window is minimized 
            /// or maximized, the system restores it to its original size and 
            /// position. An application should specify this flag when displaying 
            /// the window for the first time.</summary>
            /// <remarks>See SW_SHOWNORMAL</remarks>
            ShowNormal = 1,
            /// <summary>Activates the window and displays it as a minimized window.</summary>
            /// <remarks>See SW_SHOWMINIMIZED</remarks>
            ShowMinimized = 2,
            /// <summary>Activates the window and displays it as a maximized window.</summary>
            /// <remarks>See SW_SHOWMAXIMIZED</remarks>
            ShowMaximized = 3,
            /// <summary>Maximizes the specified window.</summary>
            /// <remarks>See SW_MAXIMIZE</remarks>
            Maximize = 3,
            /// <summary>Displays a window in its most recent size and position. 
            /// This value is similar to "ShowNormal", except the window is not 
            /// actived.</summary>
            /// <remarks>See SW_SHOWNOACTIVATE</remarks>
            ShowNormalNoActivate = 4,
            /// <summary>Activates the window and displays it in its current size 
            /// and position.</summary>
            /// <remarks>See SW_SHOW</remarks>
            Show = 5,
            /// <summary>Minimizes the specified window and activates the next 
            /// top-level window in the Z order.</summary>
            /// <remarks>See SW_MINIMIZE</remarks>
            Minimize = 6,
            /// <summary>Displays the window as a minimized window. This value is 
            /// similar to "ShowMinimized", except the window is not activated.</summary>
            /// <remarks>See SW_SHOWMINNOACTIVE</remarks>
            ShowMinNoActivate = 7,
            /// <summary>Displays the window in its current size and position. This 
            /// value is similar to "Show", except the window is not activated.</summary>
            /// <remarks>See SW_SHOWNA</remarks>
            ShowNoActivate = 8,
            /// <summary>Activates and displays the window. If the window is 
            /// minimized or maximized, the system restores it to its original size 
            /// and position. An application should specify this flag when restoring 
            /// a minimized window.</summary>
            /// <remarks>See SW_RESTORE</remarks>
            Restore = 9,
            /// <summary>Sets the show state based on the SW_ value specified in the 
            /// STARTUPINFO structure passed to the CreateProcess function by the 
            /// program that started the application.</summary>
            /// <remarks>See SW_SHOWDEFAULT</remarks>
            ShowDefault = 10,
            /// <summary>Windows 2000/XP: Minimizes a window, even if the thread 
            /// that owns the window is hung. This flag should only be used when 
            /// minimizing windows from a different thread.</summary>
            /// <remarks>See SW_FORCEMINIMIZE</remarks>
            ForceMinimized = 11
        }

        /// <summary>
        ///     Special window handles
        /// </summary>
        public enum SpecialWindowHandles
        {
            // ReSharper disable InconsistentNaming
            /// <summary>
            ///     Places the window at the top of the Z order.
            /// </summary>
            HWND_TOP = 0,
            /// <summary>
            ///     Places the window at the bottom of the Z order. If the hWnd parameter identifies a topmost window, the window loses its topmost status and is placed at the bottom of all other windows.
            /// </summary>
            HWND_BOTTOM = 1,
            /// <summary>
            ///     Places the window above all non-topmost windows. The window maintains its topmost position even when it is deactivated.
            /// </summary>
            HWND_TOPMOST = -1,
            /// <summary>
            ///     Places the window above all non-topmost windows (that is, behind all topmost windows). This flag has no effect if the window is already a non-topmost window.
            /// </summary>
            HWND_NOTOPMOST = -2
            // ReSharper restore InconsistentNaming
        }

        private struct MARGINS
        {
            public int LeftWidth;
            public int RightWidth;
            public int TopHeight;
            public int BottomHeight;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;        // x position of upper-left corner
            public int Top;         // y position of upper-left corner
            public int Right;       // x position of lower-right corner
            public int Bottom;      // y position of lower-right corner
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool EnableWindow(IntPtr hWnd, bool bEnable);

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, WindowShowStyle nCmdShow);

        [DllImport("user32.dll")]
        private static extern IntPtr GetActiveWindow();

        public static IntPtr SetWindowLongPtr(HandleRef hWnd, int nIndex, IntPtr dwNewLong)
        {
            if (IntPtr.Size == 8)
                return SetWindowLongPtr64(hWnd, nIndex, dwNewLong);
            else
                return new IntPtr(SetWindowLong32(hWnd, nIndex, dwNewLong.ToInt32()));
        }

        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        private static extern int SetWindowLong32(HandleRef hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr")]
        private static extern IntPtr SetWindowLongPtr64(HandleRef hWnd, int nIndex, IntPtr dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr SetActiveWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        static extern int SetWindowRgn(IntPtr hWnd, IntPtr hRgn, bool bRedraw);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRect);

        [DllImport("gdi32.dll")]
        static extern int CombineRgn(IntPtr hrgnDest, IntPtr hrgnSrc1, IntPtr hrgnSrc2, int fnCombineMode);

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);

        [DllImport("dwmapi.dll")]
        static extern int DwmExtendFrameIntoClientArea(IntPtr hWnd, ref MARGINS pMarInset);

        const int GWL_STYLE = -16;
        const int GWL_EXSTYLE = -20;
        const long WS_POPUP = 0x80000000;
        const long WS_CHILD = 0x40000000;
        const long WS_VISIBLE = 0x10000000;
        const long WS_MAXIMIZE = 0x01000000L;
        const long WS_EX_TRANSPARENT = 0x00000020L;
        const long WS_EX_NOACTIVATE = 0x08000000L;
        const long WS_EX_LAYERED = 0x00080000L;

        //public GameObject lowerScreenContainer;

        [Serializable]
        public class RegionBox
        {
            private int _x = 0;
            private int _y = 0;
            public int width = 0;
            public int height = 0;

            public int x2
            {
                get
                {
                    return _x + width;
                }
            }
            public int y2
            {
                get { return _y + height; }
            }

            public int x
            {
                get
                {
                    return _x < 0 ? 0 : _x;
                }

                set
                {
                    _x = value;
                }
            }

            public int y
            {
                get
                {
                    return _y;// < 0 ? 0 : _y;
                }

                set
                {
                    _y = value;
                }
            }

            public void Adjust(float scale)
            {
                _x = (int)(_x * scale);
                _y = (int)(_y * scale);
                width = (int)(width * scale);
                height = (int)(height * scale);
            }
        }

        [Serializable]
        public class IngameRegionRects
        {
            public RectTransform[] rects;
        }

        IntPtr GCUIHandle;
        public IntPtr CartridgeHandle;

        HandleRef GCUIRef;
        HandleRef CartridgeRef;

        IntPtr[] extraRegionsGenerated;

        Process hideMousy;
        Process showMousy;

        Coroutine hideGCUIRegionsContainer;

        internal bool cartridgeHidden = false;

        MARGINS zero;
        MARGINS solid;

        bool DWMExtended = true;

        //Camera lowerCamera;

        internal bool dualMonitor;
        private GameObject[] singleMonitorShow;
        private GameObject[] singleMonitorHide;

        //[Header("Offsets - for cartridges that don't fill the screen")]
        private int cartridgeWindowLeftOffset = 0;
        private int cartridgeWindowRightOffset = 0;
        private int cartridgeWindowTopOffset = 0;
        private int cartridgeWindowBottomOffset = 0;

        //[Header("Ingame Regions - groups of regions to be displayed ingame for touchscreen games")]
        private IngameRegionRects[] ingameRegionRects;
        IntPtr[][] showCartridgeRegions;
        IntPtr combinedRegions;

        private bool mouseHidden = false;

        private bool gcuiRegionsFull = true;

        RECT cartRect;

        void Awake()
        {
            Manager = GetComponent<Manager>();

            cartridgeHidden = Manager.CartridgeHidden;
            dualMonitor = Manager.DualMonitor;
            singleMonitorShow = Manager.SingleMonitorShow;
            singleMonitorHide = Manager.SingleMonitorHide;
            cartridgeWindowLeftOffset = Manager.CartridgeWindowLeftOffset;
            cartridgeWindowRightOffset = Manager.CartridgeWindowRightOffset;
            cartridgeWindowTopOffset = Manager.CartridgeWindowTopOffset;
            cartridgeWindowBottomOffset = Manager.CartridgeWindowBottomOffset;
            ingameRegionRects = Manager.IngameRegionRects;

            hideMousy = new Process();

            hideMousy.StartInfo.FileName = "C:/nomousy.exe";
            hideMousy.StartInfo.Arguments = "-hide";
            hideMousy.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            hideMousy.StartInfo.Verb = "runas";

            showMousy = new Process();

            showMousy.StartInfo.FileName = "C:/nomousy.exe";
            showMousy.StartInfo.Arguments = "";
            showMousy.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            showMousy.StartInfo.Verb = "runas";
        }

        private void Start()
        {
            if (Display.displays.Length > 1)
            {
                dualMonitor = true;
                Display.displays[1].Activate();
            }

            if (ingameRegionRects.Length > 0)
            {
                showCartridgeRegions = new IntPtr[ingameRegionRects.Length][];
            }

            HideMouse();

            AllowLookThrough(true);

            Dbg.Trace("Screen Width: " + Screen.width + ", Height: " + Screen.height);

            Dbg.Trace("Screen Resolution: " + Screen.currentResolution);
        }

        public void Init()
        {
            StartCoroutine(WindowInitialize());

            /*if (fadeIn)
            {
                fadeIn.SetActive(true);
                fadeIn.SetActive(false, 2f);
            } */

            if (singleMonitorHide != null && singleMonitorHide.Length > 0)
            {
                for (int i = 0; i < singleMonitorHide.Length; i++)
                {
                    singleMonitorHide[i].SetActive(dualMonitor);
                }
            }

            if (singleMonitorShow != null && singleMonitorShow.Length > 0)
            {
                for (int i = 0; i < singleMonitorShow.Length; i++)
                {
                    singleMonitorShow[i].SetActive(!dualMonitor);
                }
            }
        }

        IEnumerator WindowInitialize()
        {
            zero = new MARGINS
            {
                BottomHeight = 0,
                LeftWidth = 0,
                RightWidth = 0,
                TopHeight = 0
            };

            solid = new MARGINS
            {
                BottomHeight = -1,
                LeftWidth = -1,
                RightWidth = -1,
                TopHeight = -1
            };

            GCUIHandle = IntPtr.Zero;

#if !UNITY_EDITOR   // You really don't want to enable this in the editor..
        while (GCUIHandle == IntPtr.Zero)
        {
            yield return null;
            GCUIHandle = FindWindow(null, "GCUI.3");
        }

        Dbg.Trace("GCUI handle found");

        GCUIRef = new HandleRef(null, GCUIHandle);

        SetWindowPos(GCUIHandle, (IntPtr)SpecialWindowHandles.HWND_TOPMOST, 0, 0, Screen.width, Screen.height, SetWindowPosFlags.FrameChanged | SetWindowPosFlags.ShowWindow);

        SetActiveWindow(GCUIHandle);

        DwmExtendFrameIntoClientArea(GCUIHandle, ref solid);
#endif
            if (Manager.gameHandle != "")
            {
                StartCoroutine(SetCartridgeWindow());
            }

            yield break;
        }

        public IEnumerator SetCartridgeWindow()
        {
            CartridgeHandle = IntPtr.Zero;

            while (CartridgeHandle == IntPtr.Zero)
            {
                yield return null;
                CartridgeHandle = FindWindow(null, Manager.gameHandle);
            }

            Dbg.Trace("Found Game Window");

            cartridgeHidden = false;

            CartridgeRef = new HandleRef(null, CartridgeHandle);

            GetWindowRect(CartridgeHandle, out cartRect);

            Dbg.Trace("Cartridge Rect: " + cartRect.Top + ", " + cartRect.Right + ", " + cartRect.Bottom + ", " + cartRect.Left);

            SetWindowLongPtr(CartridgeRef, GWL_STYLE, new IntPtr(WS_POPUP | WS_VISIBLE));
            SetWindowPos(CartridgeHandle, (IntPtr)SpecialWindowHandles.HWND_BOTTOM, cartridgeWindowLeftOffset, cartridgeWindowTopOffset, cartRect.Right - (cartridgeWindowRightOffset + cartridgeWindowLeftOffset + cartRect.Left), cartRect.Bottom - (cartridgeWindowBottomOffset + cartridgeWindowTopOffset + cartRect.Top), SetWindowPosFlags.FrameChanged | SetWindowPosFlags.ShowWindow);
            EnableWindow(CartridgeHandle, !Manager.useController);

            SetGCUIActive();
        }

        public IEnumerator SetAttendantMenu()
        {
            IntPtr CSHandle = IntPtr.Zero;

            while (CSHandle == IntPtr.Zero)
            {
                yield return null;
                CSHandle = FindWindow(null, "CodeSpace Gaming Operator Menu");
            }

            Dbg.Trace("Found Attendant Menu Handle");

            HandleRef CSRef = new HandleRef(null, CSHandle);

            SetWindowLongPtr(CSRef, GWL_STYLE, new IntPtr(WS_POPUP | WS_VISIBLE));
            SetWindowPos(CSHandle, (IntPtr)SpecialWindowHandles.HWND_TOP, 0, 0, Screen.width, Screen.height, SetWindowPosFlags.FrameChanged | SetWindowPosFlags.ShowWindow);
            EnableWindow(CSHandle, true);
        }

        public void HideGCUIRegions()
        {
            hideGCUIRegionsContainer = StartCoroutine(HideGCUIRegionsCoroutine());
        }

        /// <summary>
        /// Restore the GCUI to the full screen from a game view using regions.
        /// </summary>
        public void ShowGCUIRegions()
        {
            if (gcuiRegionsFull)
            {
                return;
            }

            try
            {
                StopCoroutine(hideGCUIRegionsContainer);
            }
            catch (Exception e)
            {
                Dbg.Trace(e.Message);
            }

            //preventLookThrough();

            Dbg.Trace("Restoring GCUI Window");

            SetWindowRgn(GCUIHandle, CreateRectRgn(0, 0, Screen.width, Screen.height), true);

            SetGCUIActive();

            gcuiRegionsFull = true;
        }

        IEnumerator HideGCUIRegionsCoroutine()
        {
            SetWindowRgn(GCUIHandle, CreateRectRgn(0, 0, Screen.width, Screen.height), true);

            yield return null;

            SetWindowRgn(GCUIHandle, CreateRectRgn(0, 0, 1, 1), true);

            gcuiRegionsFull = false;

            Dbg.Trace("Hiding GCUI Window");
        }

        /*public void HideGCUITransparency()
        {
            // Turn off everything
            lowerScreenContainer.SetActive(false);
            AllowLookThrough(true);
        }
        public void ShowGCUITransparency()
        {
            lowerScreenContainer.SetActive(true);
            SetGCUIActive();
        } */

        /// <summary>
        /// Show Game using regions taken from the Rects in Ingame Region Rects [index], set up in the editor.
        /// </summary>
        /// <param name="index">Which region layout to use.</param>
        public void ShowCartridgeWithRegions(int index = 0)
        {
#if !UNITY_EDITOR
        SetWindowRgn(GCUIHandle, CreateRectRgn(0, 0, Screen.width, Screen.height), true);
#endif
            combinedRegions = CreateRectRgn(0, 0, 0, 0);

            if (ingameRegionRects[index].rects.Length > 0)
            {
                showCartridgeRegions[index] = new IntPtr[ingameRegionRects[index].rects.Length];

                for (int i = 0; i < ingameRegionRects[index].rects.Length; i++)
                {
                    RegionBox box = RectTransformToRegionBox(ingameRegionRects[index].rects[i]);

                    showCartridgeRegions[index][i] = CreateRectRgn(box.x, box.y, box.x2, box.y2);
                    CombineRgn(combinedRegions, combinedRegions, showCartridgeRegions[index][i], 2);
                }
            }

#if !UNITY_EDITOR
        SetWindowRgn(GCUIHandle, combinedRegions, true);
#endif

            gcuiRegionsFull = false;
        }

        public void SetGCUIActive()
        {
            SetActiveWindow(GCUIHandle);
        }

        public void HideCartridge(bool hide)
        {
            if (CartridgeHandle != IntPtr.Zero && Manager.gameHandle != "")
            {
                if (hide && !cartridgeHidden)
                {
                    //GCUIAudio.instance.muteGame();
                    SetWindowPos(CartridgeHandle, (IntPtr)SpecialWindowHandles.HWND_BOTTOM, 0, 0, Screen.width, Screen.height, SetWindowPosFlags.FrameChanged | SetWindowPosFlags.HideWindow);
                    cartridgeHidden = hide;
                    Dbg.Trace("Cartridge Hide");
                }
                else if (!hide && cartridgeHidden)
                {
                    SetWindowLongPtr(CartridgeRef, GWL_STYLE, new IntPtr(WS_POPUP | WS_VISIBLE));
                    SetWindowPos(CartridgeHandle, (IntPtr)SpecialWindowHandles.HWND_BOTTOM, cartridgeWindowLeftOffset, cartridgeWindowTopOffset, cartRect.Right - (cartridgeWindowRightOffset + cartridgeWindowLeftOffset + cartRect.Left), cartRect.Bottom - (cartridgeWindowBottomOffset + cartridgeWindowTopOffset + cartRect.Top), SetWindowPosFlags.FrameChanged | SetWindowPosFlags.ShowWindow);
                    EnableWindow(CartridgeHandle, !Manager.useController);
                    cartridgeHidden = hide;
                    Dbg.Trace("Cartridge Show");
                    //GCUIAudio.instance.setVolume();
                }

                SetGCUIActive();
            }
        }

        /// <summary>
        /// Allow transparency inthe GCUI to show the game beneath.
        /// </summary>
        /// <param name="allow">True to allow transparency.</param>
        public void AllowLookThrough(bool allow)
        {
            Camera.main.backgroundColor = (allow ? Color.clear : Color.black);
        }

        public void HideMouse()
        {
            Dbg.Trace("Mouse hidden: " + mouseHidden + ", debug build: " + UnityEngine.Debug.isDebugBuild);
            if (!mouseHidden && !UnityEngine.Debug.isDebugBuild)
            {
                if (Manager.GameDisplayMode == GameDisplayMode.Integrated)
                {
                    Cursor.visible = false;
                }
                else
                {
#if !UNITY_EDITOR
        hideMousy.Start();
#endif
                }
                Dbg.Trace("Hiding Mouse");
                mouseHidden = true;
            }
        }

        public void ShowMouse()
        {
            Dbg.Trace("SHOW MOUSE");
            if (mouseHidden && !UnityEngine.Debug.isDebugBuild)
            {
                if (Manager.GameDisplayMode == GameDisplayMode.Integrated)
                {
                    Cursor.visible = true;
                }
                else
                {
#if !UNITY_EDITOR
        showMousy.Start();
#endif
                }
                Dbg.Trace("Showing Mouse");
                mouseHidden = false;
            }
        }

        private RegionBox RectTransformToRegionBox(RectTransform transform)
        {
            Vector2 size = transform.rect.size;

            float screenConversionConstantX;
            float screenConversionConstantY;

            screenConversionConstantX = Screen.width / 1920f;
            screenConversionConstantY = Screen.height / 1080f;

            int screenX = Mathf.RoundToInt((transform.position.x + 960) - (size.x * (1 - transform.pivot.x)));
            int screenY = Mathf.RoundToInt(Mathf.Abs(transform.position.y - 540) - (size.y * (1 - transform.pivot.y)));

            RegionBox box = new RegionBox
            {
                x = (int)(screenX * screenConversionConstantX),
                y = (int)(screenY * screenConversionConstantY),
                width = (int)(size.x * screenConversionConstantX),
                height = (int)(size.y * screenConversionConstantY)
            };

            return box;
        }

        private void OnApplicationQuit()
        {
            ShowMouse();
        }
    }
}
