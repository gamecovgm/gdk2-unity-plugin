﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCCL;
using GCGDK;
using System;
using UnityEditor;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System.Reflection;
//using TMPro;

namespace GCGDK
{
    /// <summary>
    /// The spine of the GCUI. Pretty much everything is referenced via the Manager.
    /// </summary>
    [DisallowMultipleComponent]
    public class Manager : MonoBehaviour
    {
        // All the Cartridge Script stuff
        [HideInInspector]
        internal GameParams GameParamsLink;
        [HideInInspector]
        internal bool loaded = false;
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam); 
        const UInt32 WM_CLOSE = 0x0010;
        const UInt32 WM_DESTROY = 0x0002;
        private DateTime bootTime;

        internal ButtonManager ButtonManager;
        internal Cashout Cashout;
        internal Codespace Codespace;
        internal DynamicText DynamicText;
        internal Emulation Emulation;
        internal MultiGame MultiGame;
        internal PlayLoop PlayLoop;
        internal InputManager InputManager;
        internal MathModel MathModel;
        internal StateManager StateManager;
        internal TokenManager TokenManager;
        internal WindowManager WindowManager;
        internal LEDController LEDs;
        internal Dbg Dbg;
        internal bool isMultiGameMenu = false;

        internal MathType MathType = MathType.Gdk2; // For testing purposes
        public bool InstantBool = false; // Set by a ManagerEditor toggle. Pulled by the actual bool in the PlayLoop script at Awake.
        public string UserPrompt = ""; // Only for the ManagerEditor
        public string UserPromptDescription = ""; // Only for the ManagerEditor
        [HideInInspector]
        public string currentMathModel; // Only for the ManagerEditor

        // Additional Settings
        public bool isMultiGame;
        public string currencyStringFormat = "C2";
        public float defaultPanelFadeTime = 0.25f;
        public float defaultBangTextTime = 1f;
        public int[] tokenLevelCost;
        public GameDisplayMode GameDisplayMode;
        public string gameHandle;
        public bool useController;
        public double cartridgeRestartTimeOutMinutes;
        public int timeLimit = 0;
        public bool useFMOD = false;
        public string betButtonTextPre = "BET";
        public string paytableWinTextPre = "WIN";
        public string missPoolZeroText;
        public bool logControllerInputs = true;
        public Culture culture;
        public string[] CultureInfoStrings = { "en-US", "en-GB", "fr-FR", "nl-NL", "es-CL", "es-ES" };

        // State Manager
        public string CurrentState;
        public string CodespaceStateString; // Only for the ManagerEditor
        public int CodespaceStateInt; // Only for the ManagerEditor
        public bool IgnoreMenuPopulation, IgnoreHelpPopulation, IgnorePaytablePopulation;
        public Sprite DefaultBackgroundArt;
        public Font DefaultPopTextFont;
        //public TMP_FontAsset DefaultPopTMPFont;
        public Sprite[] HelpSprites;
        internal Transform HelpSystemScreen;
        internal int currentHelpPage = 0;

        // Window Manager
        public bool CartridgeHidden, DualMonitor;
        public GameObject[] SingleMonitorShow;
        public GameObject[] SingleMonitorHide;
        public int CartridgeWindowLeftOffset = 0;
        public int CartridgeWindowRightOffset = 0;
        public int CartridgeWindowTopOffset = 0;
        public int CartridgeWindowBottomOffset = 0;
        public WindowManager.IngameRegionRects[] IngameRegionRects;

        // Audio Manager
        public AudioClip DefaultCashBangSFX, DefaultTokenBangSFX, DefaultMinigameBangSFX, DefaultBonusBangSFX; // Currently not implemented
        internal GameObject PopulatedAudioSourceObject;

        // Use this for initialization
        void Awake()
        {
            ScriptFactory();

            Dbg.EchoToConsole = true;
            Dbg.AddTimeStamp = true;
            Dbg.DoLogging = true;

#if !UNITY_EDITOR
            Dbg.DoLogging = false;
            isMultiGame = false;
        ParseArguments();
#endif

            GCCLController.Initialize();
            GCCLController.InitializeGame();
            GameParamsLink = GCCLController.gameParams;
        }

        private void ParseArguments()
        {
            string[] args = Environment.GetCommandLineArgs();

            GetComponent<Dbg>().DoLogging = false;
            isMultiGame = false;

            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i].Trim())
                {
                    case "log":
                        GetComponent<Dbg>().DoLogging = true;
                        break;
                    case "culture":
                        ChangeCulture(args[i + 1].Trim());
                        break;
                    case "multigame":
                        isMultiGame = true;
                        break; 
                    default:
                        break;
                }
            }
        }

        private void Start()
        {
            // probably call initialize on all the scripts - control the order
            StartCoroutine(Begin());


            bootTime = DateTime.Now;
            if (gameHandle != "")
            {
                StartCoroutine(CycleGame());
            }
            GameParamsLink.state = ConnectedGameState.Info;
        }

        private void Update()
        {
            if (!loaded && GameParamsLink.isGameConnected && GameParamsLink.state == ConnectedGameState.Waiting)
            {
                loaded = true;
                WindowManager.Init();
                WindowManager.HideCartridge(true);
                StateManager.ChangeState(State.InGame, "Attract");

                //StartCoroutine(PlayLoop.Recover8002FromInterruption());
                PlayLoop.Recovery();
            } 
        }

        /// <summary>
        /// Wait for all connections to be set up...
        /// </summary>
        /// <returns></returns>
        private IEnumerator Begin()
        {
            yield return new WaitForSeconds(0.1f);

            if (Codespace.tilted)
            {
                StateManager.ChangeState(State.System, "Tilt", fadeTime: 0);
            }
            else if (!GameParamsLink.isGameConnected && gameHandle != "")
            {
                StateManager.ChangeState(State.System, "Loading", fadeTime: 0);
            } 
            else if(Codespace.Link.totalValue == 0)
            {
                StateManager.ChangeState(State.InGame, "Attract",fadeTime: 0);
            }

#if UNITY_EDITOR
            if (culture != Culture.USA)
            {
                ChangeCulture(CultureInfoStrings[(int)culture]);
            }
#endif
            MultiGame.Init();

            WindowManager.Init();
            WindowManager.HideCartridge(true);
            StateManager.ChangeState(State.InGame, "Attract");

            //StartCoroutine(PlayLoop.Recover8002FromInterruption());
            PlayLoop.Recovery();

            CodespaceStateString = Codespace.codespaceState + " (" + (CSStates)Codespace.codespaceState + ")";
        }

        private void ChangeCulture(string newCulture)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(newCulture);
        }


        /// <summary>
        /// Sets up the scene by adding or getting the correct scripts as components for the game.
        /// </summary>
        private void ScriptFactory()
        {

            if (GetComponent<PlayLoop>())
            {
                PlayLoop = GetComponent<PlayLoop>();
            }
            else
            {
                PlayLoop = gameObject.AddComponent(typeof(PlayLoop)) as PlayLoop;
            }

            if (GetComponent<StateManager>())
            {
                StateManager = GetComponent<StateManager>();
            }
            else
            {
                StateManager = gameObject.AddComponent(typeof(StateManager)) as StateManager;
            }

            if (GetComponent<LEDController>())
            {
                LEDs = GetComponent<LEDController>();
            }
            else
            {
                LEDs = gameObject.AddComponent<LEDController>();
            }
            if (!GetComponent<GCCLController>())
            {
                gameObject.AddComponent<GCCLController>();
            }

            if (GetComponent<Dbg>())
            {
                Dbg = GetComponent<Dbg>();
            }
            else
            {
                Dbg = gameObject.AddComponent<Dbg>();
            }

            if (GetComponent<Emulation>())
            {
                Emulation = GetComponent<Emulation>();
            }
            else
            {
                Emulation = gameObject.AddComponent(typeof(Emulation)) as Emulation;
            }

            // WindowManager - you don't get to override this
            if (GetComponent<WindowManager>())
            {
                WindowManager = GetComponent<WindowManager>();
            }
            else
            {
                //WindowManager = gameObject.AddComponent<WindowManager>();
                Dbg.Trace("NO WINDOW MANAGER COMPONENT");
            }

            if (GetComponent<MultiGame>())
            {
                MultiGame = GetComponent<MultiGame>();
            } else
            {
                MultiGame = gameObject.AddComponent(typeof(MultiGame)) as MultiGame;
            }

            if (GetComponent<ButtonManager>())
            {
                ButtonManager = GetComponent<ButtonManager>();
            }
            else
            {
                ButtonManager = gameObject.AddComponent(typeof(ButtonManager)) as ButtonManager;
            }

            if (GetComponent<Cashout>())
            {
                Cashout = GetComponent<Cashout>();
            }
            else
            {
                Cashout = gameObject.AddComponent(typeof(Cashout)) as Cashout;
            }

            if (GetComponent<Codespace>())
            {
                Codespace = GetComponent<Codespace>();
            }
            else
            {
                Codespace = gameObject.AddComponent(typeof(Codespace)) as Codespace;
            }

            if (GetComponent<DynamicText>())
            {
                DynamicText = GetComponent<DynamicText>();
            }
            else
            {
                DynamicText = gameObject.AddComponent(typeof(DynamicText)) as DynamicText;
            }

            if (GetComponent<InputManager>())
            {
                InputManager = GetComponent<InputManager>();
            }
            else
            {
                InputManager = gameObject.AddComponent(typeof(InputManager)) as InputManager;
            }

            if (GetComponent<MathModel>())
            {
                MathModel = GetComponent<MathModel>();
            }
            else
            {
                MathModel = gameObject.AddComponent(typeof(MathModel)) as MathModel;
            }

            if (GetComponent<TokenManager>())
            {
                TokenManager = GetComponent<TokenManager>();
            }
            else
            {
                TokenManager = gameObject.AddComponent(typeof(TokenManager)) as TokenManager;
            }
        }

        /// <summary>
        /// Convert an integer money value from Codespace into a correctly formatted currency string.
        /// </summary>
        /// <param name="i">Integer to currencify</param>
        /// <param name="noDecimals">If the decimal value is ".00" remove it from the string.</param>
        /// <returns>Correctly formatted currency string</returns>
        public string Currencify(int i, bool noDecimals = false)
        {
            if (noDecimals)
            {
                string currency = ((decimal)((float)i / 100)).ToString(currencyStringFormat).Replace(" ", string.Empty);

                if (currency.Substring(currency.Length - 2, 2) == "00")
                {
                    currency = currency.Substring(0, currency.Length - 3);
                }

                return currency;
            }
            else
            {
                return ((decimal)((float)i / 100)).ToString(currencyStringFormat).Replace(" ", string.Empty);
            }
        }

        /// <summary>
        /// Convert a float into a correctly formatted currency string.
        /// </summary>
        /// <param name="i">Float to currencify</param>
        /// <param name="noDecimals">If the decimal value is ".00" remove it from the string.</param>
        /// <returns></returns>
        public string Currencify(float i, bool noDecimals = false)
        {
            i = (Mathf.Approximately(i, Mathf.Round(i))) ? Mathf.Round(i) : i;

            if (noDecimals)
            {
                string currency = (System.Math.Floor((decimal)i * 100) / 100).ToString(currencyStringFormat).Replace(" ", string.Empty);

                if (currency.Substring(currency.Length - 2, 2) == "00")
                {
                    currency = currency.Substring(0, currency.Length - 3);
                }

                return currency;

            }
            else
            {
                return (System.Math.Floor((decimal)i * 100) / 100).ToString(currencyStringFormat).Replace(" ", string.Empty);
            }
        }

        private IEnumerator CycleGame()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();

                if (DateTime.Now > bootTime.AddMinutes(cartridgeRestartTimeOutMinutes) && StateManager.currentSubstate == "Idle" && Codespace.Link.codespaceState == 4 && GameParamsLink.isGameConnected && Codespace.Link.totalValue == 0)
                {
                    yield return new WaitForSeconds(15);

                    if (DateTime.Now > bootTime.AddMinutes(cartridgeRestartTimeOutMinutes) && StateManager.currentSubstate == "Idle" && Codespace.Link.codespaceState == 4 && GameParamsLink.isGameConnected && Codespace.Link.totalValue == 0)
                    {
                        loaded = false;
                        WindowManager.cartridgeHidden = false;
                        GameParamsLink.isGameConnected = false;

                        int timer = 0;

                        while (FindWindow(null, gameHandle) != IntPtr.Zero)
                        {
                            if (timer > 5)
                            {
                                HardShutdown();
                            }
                            else
                            {
                                SoftShutdown();
                            }
                            yield return new WaitForSeconds(1);
                            timer++;
                        }
                    }
                }

                yield return new WaitForSeconds(10);
            }
        }

        internal void LaunchHelp()
        {
            StateManager.ChangeState(State.InGame, "Help");
            LEDs.ChangeSequence(GCCL.LEDSequence.HELP_SCREEN);
            NavigateHelp(0);
        }
        internal void NavigateHelp(int direction)
        {
            if (HelpSprites.Length == 0)
            {
                ButtonManager.SetButtonTypeInteractable(ButtonType.HelpLeft, false);
                ButtonManager.SetButtonTypeInteractable(ButtonType.HelpRight, false);
                return;
            }

            int previousHelpPage = currentHelpPage;
            if (direction == 0)
            {
                currentHelpPage = 0;
            }
            else
            {
                currentHelpPage = currentHelpPage + direction;
            }
            Transform targetPage = HelpSystemScreen.GetChild(1);
            targetPage.SetAsFirstSibling();
            targetPage.GetComponent<Image>().sprite = HelpSprites[currentHelpPage];
            targetPage.name = "Page " + currentHelpPage;
            targetPage.GetComponent<GCGDKFader>().SetActive(true, 0);
            HelpSystemScreen.GetChild(1).GetComponent<GCGDKFader>().SetActive(false, defaultPanelFadeTime);
            HelpSystemScreen.GetChild(1).name = "Page " + previousHelpPage;

            Dbg.Trace("Current Page :" + currentHelpPage);

            ButtonManager.SetButtonTypeInteractable(ButtonType.HelpLeft, currentHelpPage != 0);
            ButtonManager.SetButtonTypeInteractable(ButtonType.HelpRight, currentHelpPage != HelpSprites.Length - 1);

            StateManager.timeoutDuration = 20f;
        }
        internal void Back()
        {
            StateManager.ChangeState(StateManager.previousState, StateManager.previousSubstate);
        }

        /// <summary>
        /// Sometimes it's useful to wait a frame for Codespace or the Cartridge to update before performing an action, but you don't want to convert a whole method to an IEnumerator.
        /// Pass the method as a string. Only works with methods with no parameters and no output.
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        internal IEnumerator WaitOneFrame(Action action)
        {
            yield return null;
            action();
        }

        internal void Quit()
        {
            GCCLController.Shutdown();
            LEDs.Shutdown();
            gameObject.SendMessage("OnQuit");
#if !UNITY_EDITOR
            StartCoroutine(QuitWait());
#endif
        }

        /*IEnumerator QuitWait()
        {
            yield return null;
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        } */

        private void SoftShutdown()
        {
            Dbg.Trace("Soft Shutdown");
            GameParamsLink.HardResetGame();
            bootTime = DateTime.Now;
        }
        private void HardShutdown()
        {
            if (WindowManager.CartridgeHandle == IntPtr.Zero || gameHandle == "") { return; }

            Dbg.Trace("Hard Shutdown");
            SendMessage(WindowManager.CartridgeHandle, WM_DESTROY, IntPtr.Zero, IntPtr.Zero);
            bootTime = DateTime.Now;
        }

        internal bool TMPInstalled(string Namespace)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.Namespace == "TMPro")
                        return true;
                }
            }
            return false;
        }
    }

}
