﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using GCGDK;

namespace GCGDK
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(CanvasGroup))]
    public class GCGDKButton : Button, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public ButtonType thisButton;

        public int tokenLevel;
        public int tokenCost;
        public int denomLevel;

        public Manager Manager;

        public AudioClip buttonActiveSFX, buttonInactiveSFX;
        private AudioSource buttonAudioSource;

        private bool selected;
        private bool mouseOver;

        private GCGDKText[] allText;

        protected override void Awake()
        {
            base.Awake();

            Manager = GameObject.Find("Manager").GetComponent<Manager>();
            if (Manager == null)
            {
                Manager = FindObjectOfType<Manager>();
            }
        }

        protected override void Start()
        {
            base.Start();

            allText = GetComponentsInChildren<GCGDKText>(true);

            if (thisButton == ButtonType.BetDenom)
            {
                StartCoroutine(DenomText());
            }
            else if (thisButton == ButtonType.UseToken)
            {
                StartCoroutine(TokenCostText());
            }
        }

        public IEnumerator TokenCostText()
        {
            yield return new WaitUntil(() => Manager);

            tokenCost = Manager.tokenLevelCost[tokenLevel - 1];

            for (int i = 0; i < allText.Length; i++)
            {
                if (allText[i].content == DynamicTextCategory.ContextBased)
                {
                    allText[i].UpdateText(tokenCost + " TOKEN" + (tokenCost != 1 ? "S" : ""));
                }
            }
        }

        private IEnumerator DenomText()
        {
            // Wait for the math model to be loaded and parsed...
            yield return new WaitWhile(() => !Manager.MathModel);

            yield return new WaitWhile(() => Manager.MathModel.denoms.Length == 0);

            //Currency setting delay
            yield return new WaitForSeconds(0.2f);

            Manager.betButtonTextPre = Manager.betButtonTextPre == "" ? "" : Manager.betButtonTextPre.Trim() + " ";

            for (int i = 0; i < allText.Length; i++)
            {
                if (allText[i].content == DynamicTextCategory.ContextBased)
                {
                    allText[i].UpdateText(Manager.betButtonTextPre + Manager.Currencify(Manager.MathModel.denoms[denomLevel]));
                }
            }
        }

        private void DoAction()
        {
            if (buttonActiveSFX || buttonInactiveSFX)
            {
                if (Manager.PopulatedAudioSourceObject == null)
                {
                    Manager.PopulatedAudioSourceObject = new GameObject("Audio Source Group");
                }
                if (buttonAudioSource == null)
                {
                    GameObject newAudioObject = new GameObject("GCGDKButton " + name);
                    newAudioObject.transform.SetParent(Manager.PopulatedAudioSourceObject.transform);
                    buttonAudioSource = newAudioObject.AddComponent<AudioSource>();
                    buttonAudioSource.playOnAwake = false;
                }
                buttonAudioSource.Stop();
            }

            if (interactable)
            {
                Manager.ButtonManager.DoButtonAction(this);

                if (buttonActiveSFX)
                {
                    buttonAudioSource.clip = buttonActiveSFX;
                    buttonAudioSource.Play();
                } 

                Dbg.Trace(thisButton.ToString() + " Pressed");
            }
            else
            {
                Manager.ButtonManager.DoButtonUnavailableAction(this);

                if (buttonInactiveSFX)
                {
                    buttonAudioSource.clip = buttonInactiveSFX;
                    buttonAudioSource.Play();
                }
            }
        }

        public void SelectDenom()
        {
            if (thisButton != ButtonType.BetDenom) { return; }
            selected = true;

            DoStateTransition(SelectionState.Highlighted, false);
        }

        public void DeSelectDenom()
        {
            selected = false;

            if (thisButton != ButtonType.BetDenom) { return; }

            DoStateTransition(SelectionState.Normal, true);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            DoAction();
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            mouseOver = true;
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            mouseOver = false;
        }

        protected override void OnDisable()
        {
            SelectionState transitionState = currentSelectionState;
            if (IsActive() && !IsInteractable())
                transitionState = SelectionState.Disabled;

            DoStateTransition(transitionState, true);
        }

        protected override void OnEnable()
        {
            SelectionState transitionState = currentSelectionState;
            if (IsActive() && !IsInteractable())
                transitionState = SelectionState.Disabled;

            DoStateTransition(transitionState, true);
        }

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            if (!IsInteractable())
            {
                state = SelectionState.Disabled;
            }
            else if (selected)
            {
                state = SelectionState.Highlighted;
            }
            else
            {
                state = SelectionState.Normal;
            }

            base.DoStateTransition(state, instant);
        }

        void OnGCUIDisable()
        {
            DoStateTransition(SelectionState.Normal, true);
        }

        void OnGCUIEnable()
        {
            DoStateTransition(SelectionState.Normal, true);
        }
    }
}
