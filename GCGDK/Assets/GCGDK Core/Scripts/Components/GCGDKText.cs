﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text.RegularExpressions;
using System.Globalization;
using GCGDK;
using System;
using UnityEngine.UI;

namespace GCGDK
{
    [DisallowMultipleComponent]
    public class GCGDKText : MonoBehaviour
    {
        [HideInInspector]
        public DynamicTextCategory content;

        [HideInInspector]
        public bool monoSpace;
        [HideInInspector]
        public float monoSpaceWidth = 2.5f;
        string mspaceWidthString;
        [HideInInspector]
        public string preText = "";
        [HideInInspector]
        public TextMeshProUGUI thisTMPUGUI;
        [HideInInspector]
        public TextMeshPro thisTMP;
        [HideInInspector]
        public Text thisTXT;

        bool interrupt;

        Manager Manager;

        Coroutine bang;

        [HideInInspector]
        public bool interruptBang = false;

        [HideInInspector]
        public bool banging = false;

        [HideInInspector]
        public bool currencifyBool;
        [HideInInspector]
        public float delayFloat;
        public float bangOverride1, bangOverride2, bangOverride3;
        public AudioClip bangSFX;
        internal AudioSource textAudioSource;

        [HideInInspector]
        public BangEnum bangEnum;

        //Variables for MonoSpace Visualization
        private string textWithoutMS;
        [HideInInspector]
        public bool isTMPAttached;

        private void AddTextComponent()
        {
            if (GetComponent<TextMeshPro>())
            {
                thisTMP = GetComponent<TextMeshPro>();
            }
            else if (GetComponent<TextMeshProUGUI>())
            {
                thisTMPUGUI = GetComponent<TextMeshProUGUI>();
            }
            else if (GetComponent<Text>())
            {
                thisTXT = GetComponent<Text>();
            }
            else
            {
                if (Manager.TMPInstalled("TMPro"))
                {
                    if (GetComponent<CanvasRenderer>() || GetComponent<RectTransform>())
                    {
                        thisTMPUGUI = gameObject.AddComponent<TextMeshProUGUI>();
                    }
                    else
                    {
                        thisTMP = gameObject.AddComponent<TextMeshPro>();
                    }
                }
                else
                {
                    Debug.Log("Text Mesh Pro is not Installed. Attaching <Text>.");
                    thisTXT = gameObject.AddComponent<Text>();
                }
            }
        }

        // Use this for initialization
        void Awake()
        {
            AddTextComponent();

            Manager = GameObject.Find("Manager").GetComponent<Manager>();

            //oldmonoSpaceWidth = monoSpaceWidth;
            mspaceWidthString = monoSpaceWidth.ToString().Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, ".");
        }

        public void UpdateText(string newText)
        {
            mspaceWidthString = monoSpaceWidth.ToString().Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, ".");

            try
            {
                StopCoroutine(bang);
                banging = false;
            }
            catch (System.Exception)
            {
            }

            SetText(preText + (preText == "" ? "" : " ") + newText);
        }

        public void TestUpdateText(bool monoSpace)
        {
            if (!Application.isPlaying)
            {
                mspaceWidthString = monoSpaceWidth.ToString().Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, ".");

                string currentString = "";
                if (GetComponent<TextMeshPro>())
                {
                    currentString = GetComponent<TextMeshPro>().text;
                }
                else if (GetComponent<TextMeshProUGUI>())
                {
                    currentString = GetComponent<TextMeshProUGUI>().text;
                }

                if (currentString.Length > 0)
                {
                    if (Convert.ToString(currentString[0]) != "<")
                    {
                        textWithoutMS = currentString;
                    }
                if (textWithoutMS != null) {
                    
                    // Checking if the string is in fact a numerical value, if so; Currencify!
                    if (textWithoutMS.Length >= 2 && int.TryParse(textWithoutMS, out int result))
                    { 
                        if (monoSpace && (GetComponent<TextMeshProUGUI>() || GetComponent<TextMeshPro>()))
                        {
                            string currencify = ((decimal)((float)result / 100)).ToString("C2").Replace(" ", string.Empty);
                            SetText(("<mspace=&&&em>" + currencify)
        .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, "</mspace>" + CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator + "<mspace=&&&em>")
        .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator, "</mspace>" + CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator + "<mspace=&&&em>")
        .Replace("&&&", mspaceWidthString));
                        } else
                        {
                            SetText(textWithoutMS);
                        }
                    } else {
                        string displayText = "<mspace=" + monoSpaceWidth + "em>";
                        if (monoSpace)
                        {
                            int monospaceInt = 0;
                            for (int i = 0; i < textWithoutMS.Length; i++)
                            {
                                if (monospaceInt == 0)
                                {
                                    displayText += Convert.ToString(textWithoutMS[i]) + "</mspace>";
                                    monospaceInt = 1;
                                }
                                else
                                {
                                    displayText += Convert.ToString(textWithoutMS[i]) + "<mspace=" + monoSpaceWidth + "em>";
                                    monospaceInt = 0;
                                }
                            }
                            SetText(displayText);
                        }
                        else
                        {
                            SetText(textWithoutMS);
                        }
                    }
                }
                interruptBang = true;
                }
            }
        }

        public void UpdateText(int newAmount, float duration = 99, bool currencify = true, bool bangDown = false, string preText = "", string postText = "", AudioClip bangSFX = null)
        {
            mspaceWidthString = monoSpaceWidth.ToString().Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, ".");

            if (!gameObject.activeInHierarchy) { return; }

            try
            {
                StopCoroutine(bang);
                banging = false;
            }
            catch (System.Exception)
            {
            }

            duration = duration == 99 ? Manager.defaultBangTextTime : duration;

            if (newAmount == 0)
            {
                SetText("");
                return;
            }

            if (duration == 0)
            {
                if (currencify)
                {
                    if (monoSpace && (GetComponent<TextMeshProUGUI>() || GetComponent<TextMeshPro>()))
                    {
                        SetText(("<mspace=&&&em>" + Manager.Currencify(newAmount))
                            .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, "</mspace>" + CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator + "<mspace=&&&em>")
                            .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator, "</mspace>" + CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator + "<mspace=&&&em>")
                            .Replace("&&&", mspaceWidthString)); 
                    }
                    else
                    {
                        SetText(Manager.Currencify(newAmount));
                    }
                }
                else
                {
                    SetText(newAmount.ToString("N0"));
                }

                return;
            }

            bang = StartCoroutine(BangText(newAmount, duration, currencify, preText, postText, bangDown, bangSFX));
        }

        IEnumerator BangText(int newAmount, float duration, bool currencify, string preText, string postText, bool bangDown, AudioClip bangSFX)
        {
            int textValue = 0;

            string currentText = "";

            if (thisTMP)
            {
                currentText = thisTMP.text;
            }
            else if (thisTMPUGUI)
            {
                currentText = thisTMPUGUI.text;
            } else if (thisTXT)
            {
                currentText = thisTXT.text;
            }

            if (currentText != "")
            {
                float current = 0;

                string toParse = Regex.Replace(currentText, @"<(.*?)>", "")
                    .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, "")
                    .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator, "");

                float.TryParse(toParse, out current);

                textValue = Mathf.RoundToInt(current * (currencify ? 100 : 1));
            }

            bool up = newAmount > textValue;

            string display = "";

            if (up || bangDown)
            {
                banging = true;

                float textValueTemp = textValue;
                float delta = newAmount - textValue;
                float change = delta / duration;

                interruptBang = false;

                if (bangSFX != null)
                {
                    if (Manager.PopulatedAudioSourceObject == null)
                    {
                        Manager.PopulatedAudioSourceObject = new GameObject("Audio Source Group");
                    }
                    if (textAudioSource == null)
                    {
                        GameObject newAudioObject = new GameObject("GCGDKText " + name);
                        newAudioObject.transform.SetParent(Manager.PopulatedAudioSourceObject.transform);
                        textAudioSource = newAudioObject.AddComponent<AudioSource>();
                        textAudioSource.playOnAwake = false;
                    }
                    textAudioSource.Stop();
                    textAudioSource.clip = bangSFX;
                    textAudioSource.loop = true;
                    textAudioSource.Play();
                } 

                while (!interruptBang && Mathf.Abs(newAmount - textValue) > Mathf.Abs(change * Time.deltaTime))
                {
                    yield return null;

                    textValueTemp += change * Time.deltaTime;

                    textValue = (int)textValueTemp;

                    if (currencify)
                    {
                        display = Manager.Currencify(textValue);
                    }
                    else
                    {
                        display = textValue.ToString("N0");
                    }

                    if (monoSpace)
                    {
                        display = display
                        .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, "</mspace>" + CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator + "<mspace=&&&em>")
                        .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator, "</mspace>" + CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator + "<mspace=&&&em>")
                            .Replace("&&&", mspaceWidthString);
                        display = "<mspace=" + mspaceWidthString + "em>" + display + "</mspace>";
                    }

                    SetText(preText + display + postText);
                }

                if (textAudioSource != null)
                {
                    textAudioSource.Stop();
                    Destroy(textAudioSource.gameObject);
                }

                banging = false;
            }

            if (newAmount == 0)
            {
                SetText("");
            }
            else
            {
                if (currencify)
                {
                    display = Manager.Currencify(newAmount);
                }
                else
                {
                    display = newAmount.ToString("N0");
                }

                if (monoSpace)
                {
                    display = display
                        .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, "</mspace>" + CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator + "<mspace=&&&em>")
                        .Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator, "</mspace>" + CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator + "<mspace=&&&em>")
                        .Replace("&&&", mspaceWidthString);
                    display = "<mspace=" + mspaceWidthString + "em>" + display + "</mspace>";
                }

                SetText(preText + display + postText);

                if (GetComponent<Animator>())
                {
                    GetComponent<Animator>().SetTrigger(up ? "ValueUp" : "ValueDown");
                }
            }

        }

        void SetText(string display)
        {
            if (thisTMPUGUI)
            {
                thisTMPUGUI.text = display;
            } else if (thisTMP)
            {
                thisTMP.text = display;
            } else if (GetComponent<TextMeshProUGUI>())
            {
                GetComponent<TextMeshProUGUI>().text = display;
            } else if (GetComponent<TextMeshPro>())
            {
                GetComponent<TextMeshPro>().text = display;
            } else if (thisTXT)
            {
                thisTXT.text = display;
            } else if (GetComponent<Text>())
            {
                GetComponent<Text>().text = display;
            }

        }
    }
}
