﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCGDK;

namespace GCGDK
{
    [RequireComponent(typeof(CanvasGroup))]
    public class GCGDKFader : MonoBehaviour
    {

        private Manager Manager;
        private bool active;
        private CanvasGroup CanvasGroup;
        private Coroutine Fading;

        void Awake()
        {
            Manager = GameObject.Find("Manager").GetComponent<Manager>();

            active = true;

            CanvasGroup = GetComponent<CanvasGroup>();

            if (CanvasGroup)
            {
                active = CanvasGroup.alpha == 1 ? true : false;

                CanvasGroup.blocksRaycasts = active;
                CanvasGroup.interactable = active;
                CanvasGroup.alpha = active ? 1 : 0;
            }
        }

        /// <summary>
        /// Disables interaction and allows raycasts to pass through instantly.
        /// Triggers fade up or fade down based on fadeTime variable (set in Settings).
        /// </summary>
        /// <param name="activate">True = active</param>
        public void SetActive(bool activate, float fadeTime = 99)
        {
            if (activate == active || !gameObject.activeInHierarchy) { return; }

            if (fadeTime != 0)
            {
                fadeTime = fadeTime == 99 ? Manager.defaultPanelFadeTime : fadeTime;
            }

            active = activate;

            if (GetComponent<CanvasGroup>())
            {
                CanvasGroup = GetComponent<CanvasGroup>();
            } else
            {
                CanvasGroup = gameObject.AddComponent<CanvasGroup>();
            }
            CanvasGroup.blocksRaycasts = activate;
            CanvasGroup.interactable = activate;

            if (Fading != null)
            {
                StopCoroutine(Fading);
            }

            if (fadeTime == 0)
            {
                CanvasGroup.alpha = activate ? 1 : 0;
            }
            else
            {
                Fading = StartCoroutine(DoFade(fadeTime));
            }

            // Yeah, I know using SendMessage is bad, but it's much more straightforward in this context than using a delegate/Event call. Maybe that's a ToDo.
            SendMessage(activate ? "OnGCUIEnable" : "OnGCUIDisable", SendMessageOptions.DontRequireReceiver);

            foreach (Transform item in transform)
            {
                item.SendMessage("OnGCUIParentEnable", SendMessageOptions.DontRequireReceiver);
            }
        }

        private IEnumerator DoFade(float fadeTimeLocal)
        {
            yield return null;
            yield return null;

            float targetAlpha = active ? 1 : 0;
            float changeInterval = (targetAlpha - CanvasGroup.alpha) * (1 / fadeTimeLocal);

            while (CanvasGroup.alpha != targetAlpha)
            {
                CanvasGroup.alpha += Time.deltaTime * changeInterval;
                yield return null;
            }
        }
    }
}
