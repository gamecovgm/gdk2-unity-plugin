﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCGDK;
using TMPro;
using System.Text.RegularExpressions;
using System.Globalization;
using System;
using System.Reflection;
using UnityEditor;
using GCCL;

namespace GCGDK
{
    [CustomEditor(typeof(GCGDKText))]
    public class GCGDKTextEditor : Editor
    {
        GCGDKText GCGDK_Text;

        //CodespaceParams Link;
        //ScriptableSettings Settings;

        /*[HideInInspector]
        public Texture2D GC_Logo; */

        private void OnEnable()
        {
            GCGDK_Text = (GCGDKText)target;
            //Settings = (ScriptableSettings)target;
            //GC_Logo = AssetDatabase.LoadAssetAtPath("Assets/GCUI Core/Resources/GC_Logo.png", typeof(Texture2D)) as Texture2D;

            /*if (FindObjectOfType<GCCLController>() != null)
            {
                Link = GCCLController.codespaceParams;
            } */

            //Debug.Log("on enable " + this.GetInstanceID());
        }

        /*private void OnDisable()
        {
            Debug.Log("on disable " + this.GetInstanceID());
        } */

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            /*if (GC_Logo)
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label(GC_Logo, GUILayout.Width(300), GUILayout.Height(100));
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            } */
            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Text Settings", EditorStyles.boldLabel);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Text Content", "Choose the content that you'd like this text component to display."));
            GCGDK_Text.content = (DynamicTextCategory)EditorGUILayout.EnumPopup(GCGDK_Text.content);
            serializedObject.FindProperty("content").enumValueIndex = (int)GCGDK_Text.content;
            GUILayout.EndHorizontal();

            if (GCGDK_Text.content == DynamicTextCategory.ContextBased)
            {
                GUILayout.BeginHorizontal();
                if (GCGDK_Text.transform.parent == null)
                {
                    EditorGUILayout.HelpBox("Contextual text content requires a parent with the GCGDK Button component ('UseToken' or 'BetDenom')", MessageType.Warning, true);
                } else
                {
                    if (GCGDK_Text.transform.parent.GetComponent<GCGDKButton>())
                    {
                        if (GCGDK_Text.transform.parent.GetComponent<GCGDKButton>().thisButton != ButtonType.UseToken && GCGDK_Text.transform.parent.GetComponent<GCGDKButton>().thisButton != ButtonType.BetDenom)
                        {
                            EditorGUILayout.HelpBox("Contextual text content requires its parent GCGDK Button to have the types; 'UseToken' or 'BetDenom'", MessageType.Warning, true);
                        } else
                        {
                            EditorGUILayout.HelpBox("Successfully connected to button parent " + GCGDK_Text.transform.parent.name + "!", MessageType.Info, true);
                        }
                    } else 
                    {
                        EditorGUILayout.HelpBox("Contextual text content requires a parent with the GCGDK Button component ('UseToken' or 'BetDenom')", MessageType.Warning, true);
                    } 
                }
                GUILayout.EndHorizontal();
            }

            GUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(GCGDK_Text.content != DynamicTextCategory.None);
            EditorGUILayout.LabelField(new GUIContent("Currencify", "If Generic, convert an int or a float into a currency value. This is automatically enabled for other content types. "));
            if (GCGDK_Text.content != DynamicTextCategory.None)
            {
                EditorGUILayout.Toggle(serializedObject.FindProperty("currencifyBool").boolValue = true);
            } else
            {
                serializedObject.FindProperty("currencifyBool").boolValue = EditorGUILayout.Toggle(serializedObject.FindProperty("currencifyBool").boolValue);
            }
            EditorGUI.EndDisabledGroup();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(GCGDK_Text.content == DynamicTextCategory.CashBalance || GCGDK_Text.content == DynamicTextCategory.ContextBased);
            EditorGUILayout.LabelField(new GUIContent("Delay", "Add a delay before the value is updated. This only applies to increases, and will not affect the 'Cash Balance' type."));
            if (GCGDK_Text.content == DynamicTextCategory.CashBalance || GCGDK_Text.content == DynamicTextCategory.ContextBased)
            {
                EditorGUILayout.FloatField(serializedObject.FindProperty("delayFloat").floatValue = 0);
            }
            else
            {
                serializedObject.FindProperty("delayFloat").floatValue = EditorGUILayout.FloatField(serializedObject.FindProperty("delayFloat").floatValue);
            }
            EditorGUI.EndDisabledGroup();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(GCGDK_Text.content != DynamicTextCategory.AchievedPrize);
            EditorGUILayout.LabelField(new GUIContent("Bang Time", "Only applicable to the 'Achieved Prize' type."));
            if (GCGDK_Text.content == DynamicTextCategory.AchievedPrize)
            {
                GCGDK_Text.bangEnum = (BangEnum)EditorGUILayout.EnumPopup(GCGDK_Text.bangEnum);
            } else {
                GCGDK_Text.bangEnum = (BangEnum)EditorGUILayout.EnumPopup(BangEnum.Disabled);
            }
            serializedObject.FindProperty("bangEnum").enumValueIndex = (int)GCGDK_Text.bangEnum;
            GUILayout.EndHorizontal();
            if (GCGDK_Text.bangEnum == BangEnum.Override)
            {
                EditorGUI.indentLevel = 1;
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Tier 1", "Ignored if value equals 0."));
                EditorGUI.indentLevel = 0;
                serializedObject.FindProperty("bangOverride1").floatValue = EditorGUILayout.FloatField(serializedObject.FindProperty("bangOverride1").floatValue);
                GUILayout.EndHorizontal();
                if (serializedObject.FindProperty("bangOverride1").floatValue > 0)
                {
                    EditorGUI.indentLevel = 1;
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Tier 2", "Ignored if value equals 0."));
                    EditorGUI.indentLevel = 0;
                    serializedObject.FindProperty("bangOverride2").floatValue = EditorGUILayout.FloatField(serializedObject.FindProperty("bangOverride2").floatValue);
                    GUILayout.EndHorizontal();
                    if (serializedObject.FindProperty("bangOverride2").floatValue > 0)
                    {
                        EditorGUI.indentLevel = 1;
                        GUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent("Tier 3", "Ignored if value equals 0."));
                        EditorGUI.indentLevel = 0;
                        serializedObject.FindProperty("bangOverride3").floatValue = EditorGUILayout.FloatField(serializedObject.FindProperty("bangOverride3").floatValue);
                        GUILayout.EndHorizontal();
                    } else
                    {
                        serializedObject.FindProperty("bangOverride2").floatValue = 0;
                        serializedObject.FindProperty("bangOverride3").floatValue = 0;
                    }
                } else
                {
                    serializedObject.FindProperty("bangOverride1").floatValue = 0;
                    serializedObject.FindProperty("bangOverride2").floatValue = 0;
                    serializedObject.FindProperty("bangOverride3").floatValue = 0;
                }
            } else
            {
                serializedObject.FindProperty("bangOverride1").floatValue = 0;
                serializedObject.FindProperty("bangOverride2").floatValue = 0;
                serializedObject.FindProperty("bangOverride3").floatValue = 0;
            }
            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel = 0;

            EditorGUI.BeginDisabledGroup(GCGDK_Text.bangEnum == BangEnum.Disabled);
            GUILayout.BeginHorizontal();
            EditorGUI.indentLevel = 1;
            EditorGUILayout.LabelField(new GUIContent("Bang SFX", "Ignored if empty."));
            EditorGUI.indentLevel = 0;
            GCGDK_Text.bangSFX = (AudioClip)EditorGUILayout.ObjectField(GCGDK_Text.bangSFX, typeof(AudioClip), true);
            GUILayout.EndHorizontal();
            EditorGUI.EndDisabledGroup();

            GUILayout.Space(15);
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Monospace Settings", EditorStyles.boldLabel);
            GUILayout.EndHorizontal();

            if (!GCGDK_Text.GetComponent<TextMeshPro>() && !GCGDK_Text.GetComponent<TextMeshProUGUI>())
            {
                GCGDK_Text.isTMPAttached = false;
            } else
            {
                GCGDK_Text.isTMPAttached = true;
            }
            GUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(!GCGDK_Text.isTMPAttached || Application.isPlaying);
            EditorGUILayout.LabelField(new GUIContent("Monospace", "Highly recommended. This keeps the numbers aligned when banging."));
            if (!GCGDK_Text.isTMPAttached)
            {
                EditorGUILayout.Toggle(serializedObject.FindProperty("monoSpace").boolValue = false);
            } else
            {
                serializedObject.FindProperty("monoSpace").boolValue = EditorGUILayout.Toggle(serializedObject.FindProperty("monoSpace").boolValue);
            }
            EditorGUI.EndDisabledGroup();
            GUILayout.EndHorizontal();

            if (serializedObject.FindProperty("monoSpace").boolValue)
            {
                /*GUILayout.BeginHorizontal();
                EditorGUI.BeginDisabledGroup(Application.isPlaying);
                EditorGUILayout.LabelField(new GUIContent("Monospace Width", "This value usually differs relative to the font and contents. Tweaking is required."));
                serializedObject.FindProperty("monoSpaceWidth").floatValue = EditorGUILayout.Slider(serializedObject.FindProperty("monoSpaceWidth").floatValue, 0f, 12f);
                EditorGUI.EndDisabledGroup();
                GUILayout.EndHorizontal(); */

                Rect position = EditorGUILayout.GetControlRect();
                EditorGUI.BeginProperty(position, new GUIContent(""), serializedObject.FindProperty("monoSpaceWidth"));
                EditorGUI.BeginDisabledGroup(Application.isPlaying);
                EditorGUI.BeginChangeCheck();
                serializedObject.FindProperty("monoSpaceWidth").floatValue = EditorGUI.Slider(position, new GUIContent("Width", "This value usually differs relative to the font and contents. Tweaking is required."), serializedObject.FindProperty("monoSpaceWidth").floatValue, 0f, 6f);
                if (EditorGUI.EndChangeCheck())
                {
                    GCGDK_Text.TestUpdateText(true);
                }
                EditorGUI.EndDisabledGroup();
                EditorGUI.EndProperty();
            } else
            {
                GCGDK_Text.TestUpdateText(false);
            }
            if (!GCGDK_Text.isTMPAttached)
            {
                GUILayout.BeginHorizontal();
                EditorGUILayout.HelpBox("Please attach a TextMesh Pro Text Component for Monospace functionality.", MessageType.Info, true);
                GUILayout.EndHorizontal();
            }
            else if (Application.isPlaying && GCGDK_Text.isTMPAttached && serializedObject.FindProperty("monoSpace").boolValue)
            {
                GUILayout.BeginHorizontal();
                EditorGUILayout.HelpBox("Monospace cannot be changed on Runtime.", MessageType.Info, true);
                GUILayout.EndHorizontal();
            }

            //Repaint();
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Updated");
                EditorUtility.SetDirty(target);
            }
            serializedObject.ApplyModifiedProperties();
        }

        }

}
