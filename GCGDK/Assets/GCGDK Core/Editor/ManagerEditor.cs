﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GCGDK;
using GCCL;

namespace GCGDK
{
    [CustomEditor(typeof(Manager))]
    public class ManagerEditor : Editor
    {
        Manager Manager;

        [HideInInspector]
        public Texture2D GC_Logo;

        private bool toggleAdditionalSettings, windowSettings;
        private bool tokencostbool;

        private List<string> errorList = new List<string>();
        private List<string> warningList = new List<string>();

        private void OnEnable()
        {
            Manager = (Manager)target;
            GC_Logo = AssetDatabase.LoadAssetAtPath("Assets/GCGDK Core/Resources/GC_Logo.png", typeof(Texture2D)) as Texture2D;

            //Debug.Log("on enable " + this.GetInstanceID());
        }

        public override void OnInspectorGUI()
        {
            if (GC_Logo)
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label(GC_Logo, GUILayout.Width(300), GUILayout.Height(100));
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }

            //base.OnInspectorGUI();

            EditorGUI.BeginChangeCheck();
            EditorStyles.label.normal.textColor = Color.white;
            float normalLabelWidth = EditorGUIUtility.labelWidth;

            if (Application.isPlaying)
            {
                EditorGUILayout.LabelField(new GUIContent("User Prompt: " + Manager.UserPrompt, Manager.UserPromptDescription));
                EditorGUILayout.LabelField("Math Model: " + Manager.currentMathModel);
            } else
            {
                EditorGUILayout.LabelField(new GUIContent("User Prompt: Offline", "This prompt will be populated with useful information at runtime."));
                EditorGUILayout.LabelField(new GUIContent("Math Model: Offline", "The Math Model will be updated at Runtime, once Codespace has initialized."));
            }
            EditorGUI.BeginDisabledGroup(Application.isPlaying);
            Manager.InstantBool = EditorGUILayout.ToggleLeft(new GUIContent("Instant Loop -For Testing-", "If enabled, Codespace will loop without input. This is always set to false in a build."), Manager.InstantBool);
            EditorGUI.EndDisabledGroup();

            GUILayout.Space(10);
            EditorGUILayout.LabelField("State Manager", EditorStyles.boldLabel);
            string GDKStateString = "";
            string CSStateString = "";
            if (!Application.isPlaying)
            {
                GDKStateString = "Offline";
                CSStateString = "Offline";
            } else
            {
                GDKStateString = Manager.CurrentState;
                CSStateString = Manager.CodespaceStateString;
            }
            EditorGUILayout.LabelField("Current GDK State: " + GDKStateString);
            EditorGUILayout.LabelField("Current CS State: " + CSStateString);
            EditorGUI.BeginDisabledGroup(Application.isPlaying);
            Manager.IgnoreMenuPopulation = EditorGUILayout.ToggleLeft(new GUIContent("Ignore Menu Population", "If enabled, stops the system from casting a new canvas and the default Menu screen."), Manager.IgnoreMenuPopulation);
            Manager.IgnorePaytablePopulation = EditorGUILayout.ToggleLeft(new GUIContent("Ignore Paytable Population", "If enabled, stops the system from casting a new canvas and the default Paytable screen."), Manager.IgnorePaytablePopulation);
            Manager.IgnoreHelpPopulation = EditorGUILayout.ToggleLeft(new GUIContent("Ignore Help Population", "If enabled, stops the system from casting a new canvas and the default Help screen."), Manager.IgnoreHelpPopulation);
            EditorGUI.EndDisabledGroup();
            EditorGUI.BeginDisabledGroup(Manager.IgnoreHelpPopulation);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("HelpSprites"), new GUIContent("Help Sprites", "Ignored if empty."), true);
            EditorGUI.EndDisabledGroup();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(new GUIContent("Default Pop Background", "Set as the background image when an appropriate state is populated. Ignored if empty."));
            Manager.DefaultBackgroundArt = (Sprite)EditorGUILayout.ObjectField(Manager.DefaultBackgroundArt, typeof(Sprite), true);
            EditorGUILayout.EndHorizontal();
            Manager.DefaultPopTextFont = (Font)EditorGUILayout.ObjectField(new GUIContent("Default Pop Font", "Arial if empty."), Manager.DefaultPopTextFont, typeof(Font), true);

            GUILayout.Space(10);
            windowSettings = EditorGUILayout.Foldout(windowSettings, "Window Manager", new GUIStyle(EditorStyles.foldout) { fontStyle = FontStyle.Bold });
            if (windowSettings)
            {
                Manager.CartridgeHidden = EditorGUILayout.ToggleLeft("Cartridge Hidden", Manager.CartridgeHidden);
                Manager.DualMonitor = EditorGUILayout.ToggleLeft("Dual Monitor", Manager.DualMonitor);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("SingleMonitorShow"), new GUIContent("Single Monitor Show", ""), true);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("SingleMonitorHide"), new GUIContent("Single Monitor Hide", ""), true);
                EditorGUILayout.LabelField(new GUIContent("Window offsets", "For cartridges that don't fill the screen."));
                EditorGUILayout.BeginHorizontal();
                normalLabelWidth = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = 30;
                Manager.CartridgeWindowLeftOffset = EditorGUILayout.IntField(new GUIContent("Left", "Cartridge Window Left Offset"), Manager.CartridgeWindowLeftOffset, GUILayout.MinWidth(45));
                EditorGUIUtility.labelWidth = 40;
                Manager.CartridgeWindowRightOffset = EditorGUILayout.IntField(new GUIContent("Right", "Cartridge Window Right Offset"), Manager.CartridgeWindowRightOffset, GUILayout.MinWidth(55));
                EditorGUIUtility.labelWidth = 30;
                Manager.CartridgeWindowTopOffset = EditorGUILayout.IntField(new GUIContent("Top", "Cartridge Window Top Offset"), Manager.CartridgeWindowTopOffset, GUILayout.MinWidth(45));
                EditorGUIUtility.labelWidth = 45;
                Manager.CartridgeWindowBottomOffset = EditorGUILayout.IntField(new GUIContent("Bottom", "Cartridge Window Bottom Offset"), Manager.CartridgeWindowBottomOffset, GUILayout.MinWidth(60));
                EditorGUILayout.EndHorizontal();
                EditorGUIUtility.labelWidth = normalLabelWidth;
                EditorGUILayout.PropertyField(serializedObject.FindProperty("IngameRegionRects"), new GUIContent("Ingame Region Rects", "Groups of regions to be displayed ingame for touchscreen games."), true);
            }

            GUILayout.Space(10);
            toggleAdditionalSettings = EditorGUILayout.Foldout(toggleAdditionalSettings, "Additional Settings", new GUIStyle(EditorStyles.foldout) { fontStyle = FontStyle.Bold });

            //Check Conditions Here
            Color currentStringFormatColor = Color.white;
            if (serializedObject.FindProperty("currencyStringFormat").stringValue == "")
            {
                currentStringFormatColor = Color.red;
                errorList.Add("Currency String Format is missing.");
            }

            if (toggleAdditionalSettings)
            {
                //EditorGUILayout.HelpBox("Any changes made during Runtime will not affect the current play session.", MessageType.Info, true);

                EditorStyles.label.normal.textColor = currentStringFormatColor;
                serializedObject.FindProperty("currencyStringFormat").stringValue = EditorGUILayout.TextField(new GUIContent("Currency String Format", "Currency Format Default is C2."), serializedObject.FindProperty("currencyStringFormat").stringValue);
                EditorStyles.label.normal.textColor = Color.white;

                serializedObject.FindProperty("defaultPanelFadeTime").floatValue = EditorGUILayout.Slider(new GUIContent("Default Panel Fade Time", "Can be overridden for individual instances."), serializedObject.FindProperty("defaultPanelFadeTime").floatValue, 0f, 2f);
                serializedObject.FindProperty("defaultBangTextTime").floatValue = EditorGUILayout.Slider(new GUIContent("Default Bang Text Time", "Can be overridden for individual instances."), serializedObject.FindProperty("defaultBangTextTime").floatValue, 0f, 10f);

                tokencostbool = EditorGUILayout.Foldout(tokencostbool, new GUIContent("Token Level Cost", "Token Level Cost?"));
                if (tokencostbool)
                {
                    EditorGUI.indentLevel = 1;
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Size");
                    serializedObject.FindProperty("tokenLevelCost").arraySize = EditorGUILayout.IntField(serializedObject.FindProperty("tokenLevelCost").arraySize);
                    GUILayout.EndHorizontal();
                    if (serializedObject.FindProperty("tokenLevelCost").arraySize > 0)
                    {
                        for (int i = 0; i < serializedObject.FindProperty("tokenLevelCost").arraySize; i++)
                        {
                            serializedObject.FindProperty("tokenLevelCost").GetArrayElementAtIndex(i).intValue = EditorGUILayout.IntField("Element " + i, serializedObject.FindProperty("tokenLevelCost").GetArrayElementAtIndex(i).intValue);
                        }
                    }
                }
                EditorGUI.indentLevel = 0;

                serializedObject.FindProperty("gameHandle").stringValue = EditorGUILayout.TextField(new GUIContent("Game Handle", "The handle of the game window."), serializedObject.FindProperty("gameHandle").stringValue);
                serializedObject.FindProperty("useController").boolValue = EditorGUILayout.Toggle(new GUIContent("Use Controller", "Does the game require a controller?"), serializedObject.FindProperty("useController").boolValue);
                serializedObject.FindProperty("cartridgeRestartTimeOutMinutes").floatValue = EditorGUILayout.FloatField(new GUIContent("Cartridge Restart Time", "Minutes before restarting cartridge."), serializedObject.FindProperty("cartridgeRestartTimeOutMinutes").floatValue);
                serializedObject.FindProperty("timeLimit").intValue = EditorGUILayout.IntField(new GUIContent("Time Limit", "Time limit per game (if applicable)."), serializedObject.FindProperty("timeLimit").intValue);
                serializedObject.FindProperty("useFMOD").boolValue = EditorGUILayout.Toggle(new GUIContent("Use FMOD", "Does the game use FMOD for audio?"), serializedObject.FindProperty("useFMOD").boolValue);
                serializedObject.FindProperty("betButtonTextPre").stringValue = EditorGUILayout.TextField(new GUIContent("Bet Button Pre-Text", "Optional text to insert before denom on bet buttons (eg 'BET')."), serializedObject.FindProperty("betButtonTextPre").stringValue);
                serializedObject.FindProperty("paytableWinTextPre").stringValue = EditorGUILayout.TextField(new GUIContent("Paytable Win Pre-Text", "Optional text to insert before win amounts on paytable (eg 'WIN')."), serializedObject.FindProperty("paytableWinTextPre").stringValue);
                serializedObject.FindProperty("missPoolZeroText").stringValue = EditorGUILayout.TextField(new GUIContent("Miss Pool Zero Text", "Optional text to show if there is no money in the miss pool."), serializedObject.FindProperty("missPoolZeroText").stringValue);
                serializedObject.FindProperty("logControllerInputs").boolValue = EditorGUILayout.Toggle(new GUIContent("Log Controller Inputs", "Turn off logging for controller heavy games (ex; Soul Calibur 2)."), serializedObject.FindProperty("logControllerInputs").boolValue);
                serializedObject.FindProperty("isMultiGame").boolValue = EditorGUILayout.Toggle(new GUIContent("Is MultiGame (For Testing)", "Bool for testing purposes. The build automatically sets this via the commandline."), serializedObject.FindProperty("isMultiGame").boolValue);

                Manager.GameDisplayMode = (GameDisplayMode)EditorGUILayout.EnumPopup(new GUIContent("Display Mode", "Choose game display mode."), Manager.GameDisplayMode);
                serializedObject.FindProperty("GameDisplayMode").enumValueIndex = (int)Manager.GameDisplayMode;
                Manager.culture = (Culture)EditorGUILayout.EnumPopup(new GUIContent("Culture", "Choose culture."), Manager.culture);
                serializedObject.FindProperty("culture").enumValueIndex = (int)Manager.culture;
            }

            if (warningList.Count > 0)
            {
                GUILayout.Space(5);
                string warningText = "";
                foreach (string item in warningList)
                {
                    if (warningText != "")
                    {
                        warningText += " " + item;
                    }
                    else
                    {
                        warningText = item;
                    }
                }
                EditorGUILayout.HelpBox(warningText, MessageType.Warning, true);
                warningList.Clear();
            }

            if (errorList.Count > 0)
            {
                GUILayout.Space(5);
                string errorText = "";
                foreach (string item in errorList)
                {
                    if (errorText != "")
                    {
                        errorText += " " + item;
                    }
                    else
                    {
                        errorText = item;
                    }
                }
                EditorGUILayout.HelpBox(errorText, MessageType.Error, true);
                errorList.Clear();
            }

            //Repaint();
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Updated");
                EditorUtility.SetDirty(target);
            }
            serializedObject.ApplyModifiedProperties();

        }
    }
}

