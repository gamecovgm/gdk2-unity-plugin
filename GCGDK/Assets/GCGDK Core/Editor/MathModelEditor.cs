﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GCGDK;

[CustomEditor(typeof(MathModel), true)]
public class MathModelEditor : Editor
{
    public static bool[] showData;

    private MathModel mathModel;
    private GUIStyle myFoldoutStyle;

    private void OnEnable()
    {
        mathModel = (MathModel)target;

        showData = new bool[mathModel.denoms.Length];

        myFoldoutStyle = new GUIStyle(EditorStyles.foldout)
        {
            fontStyle = FontStyle.Bold
        };

    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        var obj = FindObjectOfType<Manager>();
        var objEditor = Editor.CreateEditor(obj);
        var currencyString = objEditor.serializedObject.FindProperty("currencyStringFormat").stringValue;

        var CSobj = FindObjectOfType<Codespace>();

        if (CSobj)
        {
            var CSobjEditor = Editor.CreateEditor(CSobj);
            var selectedModel = CSobjEditor.serializedObject.FindProperty("selectedMathModel").stringValue;
            var selectedModule = CSobjEditor.serializedObject.FindProperty("selectedModule").stringValue;

            EditorGUILayout.LabelField("Selected Math: " + selectedModule + ", " + selectedModel);

            EditorGUILayout.Space();
        }

        for (int i = 0; i < mathModel.denoms.Length; i++)
        {
            showData[i] = EditorGUILayout.Foldout(showData[i], "Denom " + (i + 1) + ": " + ((float)mathModel.denoms[i] / 100).ToString(currencyString), myFoldoutStyle);

            if (showData[i])
            {
                for (int a = 0; a < mathModel.multipliers[i].Length; a++)
                {
                    EditorGUILayout.LabelField("--Distro " + (a + 1) + ": " + mathModel.multipliers[i][a] + "x, " + mathModel.odds[i][a].ToString("P2"));
                }
            }
        } 

    }
}
