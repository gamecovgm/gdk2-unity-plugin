﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UI;
using GCGDK;

[CustomEditor(typeof(GCGDKButton))]
[CanEditMultipleObjects]
public class GCGDKButtonEditor : SelectableEditor
{
    protected override void OnEnable()
    {
        base.OnEnable();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GCGDKButton button = (GCGDKButton)target;
    
        serializedObject.Update();
        // Show the custom GUI controls
        EditorGUILayout.Space();

        button.thisButton = (ButtonType)EditorGUILayout.EnumPopup("Button Type", button.thisButton);

        EditorGUILayout.Space();

        if (button.thisButton == ButtonType.UseToken)
        {
            var obj = FindObjectOfType<Manager>();
            var objEditor = Editor.CreateEditor(obj);
            var constraintValue = objEditor.serializedObject.FindProperty("tokenLevelCost").arraySize;

            string[] strings = new string[constraintValue];
            int[] ints = new int[constraintValue];

            for (int i = 0; i < constraintValue; i++)
            {
                strings[i] = "Token Level " + (i + 1);
                ints[i] = i + 1;
            }

            EditorGUI.BeginChangeCheck();
            int tokenLevel = EditorGUILayout.IntPopup("Token Level", button.tokenLevel, strings, ints);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Changed Token Level");
                button.tokenLevel = tokenLevel;
            }

            if (button.tokenLevel > 0)
            {
                button.tokenCost = objEditor.serializedObject.FindProperty("tokenLevelCost").GetArrayElementAtIndex(button.tokenLevel - 1).intValue;
                EditorGUILayout.LabelField("Cost: " + button.tokenCost + " Token" + (button.tokenCost == 1 ? "" : "s"));
            }
        }
        else if (button.thisButton == ButtonType.BetDenom)
        {
            EditorGUILayout.LabelField("Denom 1 is lowest");

            EditorGUI.BeginChangeCheck();
            int denomLevel = EditorGUILayout.IntPopup("Denom Level", button.denomLevel, new string[] { "Denom 1", "Denom 2", "Denom 3", "Denom 4", "Denom 5", "Denom 6", "Denom 7" }, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Changed Denom");
                button.denomLevel = denomLevel;
            }
        } else if (button.thisButton == ButtonType.Help && button.Manager.IgnoreHelpPopulation)
        {
            EditorGUILayout.HelpBox("Help Population will be ignored. Toggle this option off from the Manager script if you wish to use the Help button.", MessageType.Warning, true);
        } 
        else if (button.thisButton == ButtonType.Utility4 && button.Manager.IgnorePaytablePopulation)
        {
            EditorGUILayout.HelpBox("Paytable Population will be ignored. Toggle this option off from the Manager script if you wish to use the Paytable button.", MessageType.Warning, true);
        }
        else if (button.thisButton == ButtonType.Utility5 && button.Manager.IgnoreMenuPopulation)
        {
            EditorGUILayout.HelpBox("Menu Population will be ignored. Toggle this option off from the Manager script if you wish to use the Menu button.", MessageType.Warning, true);
        }

        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("On-Click Extras", EditorStyles.boldLabel);
        GUILayout.EndHorizontal();

        EditorGUI.BeginChangeCheck();

        GUILayout.BeginHorizontal();
        button.buttonActiveSFX = (AudioClip)EditorGUILayout.ObjectField(new GUIContent("Active SFX", "Ignored if empty."), button.buttonActiveSFX, typeof(AudioClip), true);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        button.buttonInactiveSFX = (AudioClip)EditorGUILayout.ObjectField(new GUIContent("Inactive SFX", "Ignored if empty."), button.buttonInactiveSFX, typeof(AudioClip), true);
        GUILayout.EndHorizontal();

        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Updated");
            EditorUtility.SetDirty(target);
        }

        serializedObject.ApplyModifiedProperties();
    }
}
