﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GCUI;
using UnityEngine.UI;
using TMPro;

namespace GCUI
{
    public delegate void StateChangeEventHandler(object source, StateArgs e);

    public class StateArgs : System.EventArgs
    {
        public State state;
        public StateArgs(State newState)
        {
            state = newState;
        }
    }

    [Serializable]
    public class SubStateObject
    {
        public string Name;
        public int Id;
        public List<CanvasGroup> CanvasObjects;

        public SubStateObject(string name, int id)
        {
            Name = name;
            Id = id;
            CanvasObjects = new List<CanvasGroup>();
        }

        public void SetActive(bool active, float fadeTime)
        {
            foreach (CanvasGroup item in CanvasObjects)
            {
                if (item)
                {
                    CanvasGroup itemCanvasGroup = null;
                    if (item.gameObject.GetComponent<CanvasGroup>())
                    {
                        itemCanvasGroup = item.gameObject.GetComponent<CanvasGroup>();
                    } else
                    {
                        itemCanvasGroup = item.gameObject.AddComponent<CanvasGroup>();
                    }
                    if (active)
                    {
                        itemCanvasGroup.alpha = 1;
                        itemCanvasGroup.interactable = true;
                        itemCanvasGroup.blocksRaycasts = true;
                        itemCanvasGroup.enabled = true;
                        item.gameObject.SetActive(true);
                    } else
                    {
                        itemCanvasGroup.alpha = 0;
                        itemCanvasGroup.interactable = false;
                        itemCanvasGroup.blocksRaycasts = false;
                        //itemCanvasGroup.enabled = false;
                    }
                    //item.SetActive(active, fadeTime);
                }
                else
                {
                    Dbg.Trace("WARNING: Missing Substate CanvasGroup " + Name);
                }
            }
        }
    }

    [Serializable]
    public class StateObject
    {
        public State State;
        public List<SubStateObject> SubStates;

        public StateObject(State state)
        {
            State = state;
            SubStates = new List<SubStateObject>();

            if (state == State.System)
            {
                SubStates.Add(new SubStateObject("Loading", 1));
                SubStates.Add(new SubStateObject("Tilt", 2));
                SubStates.Add(new SubStateObject("Handpay", 3));
                SubStates.Add(new SubStateObject("JackpotHandpay", 4));
                SubStates.Add(new SubStateObject("NonCashableCredit", 5));
                SubStates.Add(new SubStateObject("CashingOut", 6));
                SubStates.Add(new SubStateObject("Emulation", 7));
                SubStates.Add(new SubStateObject("Underfunded", 8));
                SubStates.Add(new SubStateObject("CashOutConfirm", 9));
                //SubStates.Add(new SubStateObject("Paytable", 10));
            }
            else if (state == State.InGame)
            {
                SubStates.Add(new SubStateObject("Attract", 1));
                SubStates.Add(new SubStateObject("Play", 2));
                SubStates.Add(new SubStateObject("Menu", 3));
                SubStates.Add(new SubStateObject("Help", 4));
                SubStates.Add(new SubStateObject("Paytable", 5));
                //SubStates.Add(new SubStateObject("Paytable", 6));
                //SubStates.Add(new SubStateObject("CashOutConfirm", 7));
            }
        }

        public void ClearAll(float fadeTime = 99)
        {
            foreach (SubStateObject item in SubStates)
            {
                item.SetActive(false, fadeTime);
            }
        }

        public void SetActive(bool active, int Id = 0, float fadeTime = 99)
        {
            ClearAll(fadeTime);

            if (!active) { return; }

            foreach (SubStateObject item in SubStates)
            {
                /*if ((item.Id == 0 && State != State.System) || item.Id == Id)
                {
                    item.SetActive(true, fadeTime);
                } */
                if (item.Id == Id)
                {
                    item.SetActive(true, fadeTime);
                }
            }
        }

        public void SetActive(bool active, string subState, float fadeTime = 99)
        {
            ClearAll(fadeTime);

            if (!active) { return; }

            foreach (SubStateObject item in SubStates)
            {
                /*if ((item.Id == 0 && State != State.System) || item.Name == subState)
                {
                    item.SetActive(true, fadeTime);
                } */
                if (item.Name == subState)
                {
                    item.SetActive(true, fadeTime);
                }
            }
        }
    }

    public class StateManager : MonoBehaviour
    {
        [HideInInspector]
        public Manager Manager;

        public StateObject InGame = new StateObject(State.InGame);
        public StateObject System = new StateObject(State.System);

        public State State;
        public State previousState;
        public string previousSubstate;
        public string currentStateString; // For the Inspector
        public string currentSubstate;

        internal Coroutine noCashTimeOut;

        public event StateChangeEventHandler StateEvent;

        internal float timeoutDuration = 20f;
        internal bool timeoutCountdownActive;

        private StateObject[] stateObjects;
        private List<SubStateObject> allSubStatesList = new List<SubStateObject>();
        private GameObject populatedCanvas;

        private List<GameObject> paytableButtons = new List<GameObject>();
        private List<GameObject> paytableDenomValues = new List<GameObject>();

        private void Awake()
        {
            Manager = GetComponent<Manager>();
            stateObjects = new StateObject[] { InGame, System };

            for (int i = 0; i < stateObjects[(int)State.System].SubStates.Count; i++)
            {
                allSubStatesList.Add(stateObjects[(int)State.System].SubStates[i]);
            }
            for (int i = 0; i < stateObjects[(int)State.InGame].SubStates.Count; i++)
            {
                allSubStatesList.Add(stateObjects[(int)State.InGame].SubStates[i]);
            }
            //Debug.Log("allSubStatesList count is " + allSubStatesList.Count);
        }

        /// <summary>
        /// Set all states to inactive, in preparation for switching.
        /// </summary>
        /// <param name="fadeTime"></param>
        void ClearAllStates(float fadeTime = 99)
        {
            foreach (StateObject state in stateObjects)
            {
                state.ClearAll(fadeTime);
            }
        }

        /// <summary>
        /// Change UI State. Changes UI.
        /// </summary>
        /// <param name="newState">State to change to</param>
        /// <param name="subStateId">Id of SubState. Defaults to 0, which is Primary.</param>
        /// <param name="fadeTime">Time for fade transition</param>
        public virtual void ChangeState(State newState, int subStateId = 0, float fadeTime = 99)
        {
            if (ChangeStateInternal(newState, "", fadeTime))
            {
                currentSubstate = "";

                if (populatedCanvas)
                {
                    Destroy(populatedCanvas);
                }

                SystemPopulate(stateObjects[(int)State.System].SubStates[subStateId].Name);

                stateObjects[(int)newState].SetActive(true, subStateId, fadeTime);

                Dbg.Trace("New State: " + newState + " SubState: " + subStateId);

                if (subStateId != 0)
                {
                    currentStateString += " - " + subStateId;
                }

                //Manager.ButtonManager.UpdateBetDenomButtons();

                StateEvent?.Invoke(this, new StateArgs(State));
            }
        }

        /// <summary>
        /// Change UI State. Changes UI.
        /// </summary>
        /// <param name="newState">State to change to</param>
        /// <param name="subStateName">Substate name</param>
        /// <param name="fadeTime">Time for fade transition</param>
        public virtual void ChangeState(State newState, string subStateName, float fadeTime = 99)
        {
            if (ChangeStateInternal(newState, subStateName, fadeTime))
            {
                if (populatedCanvas)
                {
                    Destroy(populatedCanvas);
                }

                SystemPopulate(subStateName);

                stateObjects[(int)newState].SetActive(true, subStateName, fadeTime);

                Dbg.Trace("New State: " + newState + " SubState: " + subStateName);

                previousSubstate = currentSubstate;
                currentStateString += " - " + subStateName;
                currentSubstate = subStateName;

                if (noCashTimeOut != null)
                {
                    StopCoroutine(noCashTimeOut);
                    timeoutCountdownActive = false;
                }

                if (subStateName == "Help")
                {
                    Debug.Log("NoCashTimeOut");
                    noCashTimeOut = StartCoroutine(NoCashTimeOut());
                }

                //Manager.ButtonManager.UpdateBetDenomButtons();

                StateEvent?.Invoke(this, new StateArgs(State));
            }
        }

        bool ChangeStateInternal(State newState, string subStateName = "", float fadeTime = 99)
        {
            if ((Manager.Codespace.tilted || (Manager.Codespace.Link.codespaceState > 8 && Manager.Codespace.Link.codespaceState != 14)) && newState != State.System)
            {
                Dbg.Trace("State must be System");
                return false;
            }

            if ((!Manager.Codespace.tilted && Manager.gameHandle != "") && Manager.Codespace.Link.codespaceState != 10 && Manager.Codespace.Link.codespaceState != 11 && subStateName != "Loading")
            {
                ClearAllStates(fadeTime);
                ChangeState(State.System, "Loading");
                return false;
            }

            if (subStateName == "Attract" && (Manager.Codespace.Link.totalValue > 0 || !Manager.PlayLoop.MachineClean()))
            {
                Dbg.Trace("Attract not appropriate");
                Manager.PlayLoop.StartSession();
                return false;
            }

            ClearAllStates(fadeTime);

            currentStateString = newState.ToString();

            previousState = State;
            State = newState;

            /*if (previousState == State.System && newState != State.System && populatedCanvas != null)
            {
                Destroy(populatedCanvas);
            } */

            return true;
        }

        public virtual IEnumerator NoCashTimeOut()
        {
            if (Manager.PlayLoop.underfundedCountdownInProgress || timeoutCountdownActive || Manager.Codespace.Link.totalValue > 0) { yield break; }

            timeoutCountdownActive = true;

            timeoutDuration = 20;

            while (Manager.Codespace.Link.totalValue == 0 && timeoutDuration > 0)
            {
                yield return null;
                if (Manager.Codespace.Link.codespaceState == 4)
                {
                    timeoutDuration -= Time.deltaTime;
                }
            }

            timeoutCountdownActive = false;

            if (Manager.Codespace.Link.totalValue == 0 && Manager.Codespace.Link.codespaceState == 4)
            {
                ChangeState(State.InGame, "Attract");
            }
        }

        void SystemPopulate(string subStateName)
        {
            int nullInt = 0;
            for (int i = 0; i < allSubStatesList.Count; i++)
            {
                if (allSubStatesList[i].Name == subStateName)
                {
                    if (allSubStatesList[i].CanvasObjects.Count > 0)
                    {
                        for (int a = 0; a < allSubStatesList[i].CanvasObjects.Count; a++)
                        {
                            if (allSubStatesList[i].CanvasObjects[a] == null)
                            {
                                //Debug.Log("Testing: SystemPopulate found zero SubObjects");
                                nullInt += 1;
                                break;
                            }
                        }
                    }
                    else
                    {
                        //Debug.Log("Testing: SystemPopulate found zero SubObjects");
                        nullInt += 1;
                        break;
                    }
                }
            }

            if (nullInt == 0)
            {
                return;
            }

            Canvas[] allActiveCanvases = null;
            allActiveCanvases = FindObjectsOfType<Canvas>();
            Vector2 ReferenceResolution = Vector2.zero;

            int targetSortOrder = 1001;
            if (allActiveCanvases.Length > 0)
            {
                int orderInt = -9999;
                int whichCanvasInt = 0;
                for (int i = 0; i < allActiveCanvases.Length; i++)
                {
                    if (ReferenceResolution == Vector2.zero && allActiveCanvases[i].targetDisplay == 0 && allActiveCanvases[i].GetComponent<CanvasScaler>())
                    {
                        ReferenceResolution = allActiveCanvases[i].GetComponent<CanvasScaler>().referenceResolution;
                    }
                    if (i == 0)
                    {
                        orderInt = allActiveCanvases[i].sortingOrder;
                    }
                    if (allActiveCanvases[i].sortingOrder >= orderInt && allActiveCanvases[i].targetDisplay == 0)
                    {
                        orderInt = allActiveCanvases[i].sortingOrder;
                        whichCanvasInt = i;
                    }
                }
                //Canvas chosenCanvas = allActiveCanvases[whichCanvasInt];
                targetSortOrder += allActiveCanvases[whichCanvasInt].sortingOrder;
                //Debug.Log("Chosen Canvas is " + chosenCanvas.gameObject.name + " and its Sort Order is " + chosenCanvas.sortingOrder);
            }

            populatedCanvas = new GameObject();
            populatedCanvas.name = "System Canvas";
            populatedCanvas.tag = "Untagged";
            populatedCanvas.layer = default;

            Canvas canvasComp = populatedCanvas.AddComponent<Canvas>();
            canvasComp.renderMode = RenderMode.ScreenSpaceOverlay;
            canvasComp.sortingOrder = targetSortOrder;
            canvasComp.targetDisplay = 0;
            canvasComp.additionalShaderChannels = AdditionalCanvasShaderChannels.TexCoord1;

            CanvasScaler scalerComp = populatedCanvas.AddComponent<CanvasScaler>();
            scalerComp.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            if (ReferenceResolution == Vector2.zero)
            {
                scalerComp.referenceResolution = new Vector2(1920, 1080);
            } else
            {
                scalerComp.referenceResolution = ReferenceResolution;
            }
            scalerComp.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
            scalerComp.referencePixelsPerUnit = 100;

            GraphicRaycaster raycasterComp = populatedCanvas.AddComponent<GraphicRaycaster>();
            CanvasGroup popCanvasGroupComp = populatedCanvas.AddComponent<CanvasGroup>();

            if (populatedCanvas.transform.childCount > 0)
            {
                int canvasChildrenCount = populatedCanvas.transform.childCount;
                for (int i = 0; i < canvasChildrenCount; i++)
                {
                    Destroy(populatedCanvas.transform.GetChild(0).gameObject);
                } 
            }

            GameObject systemScreen = new GameObject(subStateName);
            systemScreen.transform.SetParent(populatedCanvas.transform);
            RectTransform systemRect = systemScreen.AddComponent<RectTransform>();
            systemScreen.AddComponent<CanvasRenderer>();
            systemScreen.AddComponent<Image>().color = Color.gray;
            systemRect.anchorMin = Vector2.zero;
            systemRect.anchorMax = Vector2.one;
            systemRect.offsetMax = Vector2.zero;
            systemRect.offsetMin = Vector2.zero;

            GameObject systemHeader = new GameObject("Header");
            systemHeader.transform.SetParent(systemScreen.transform);
            RectTransform headerRect = systemHeader.AddComponent<RectTransform>();
            AddRectComp(headerRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, 480), default);
            systemHeader.AddComponent<CanvasRenderer>();

            Manager.DynamicText.populatedTextList.Clear();

            Font PopFont = Resources.GetBuiltinResource<Font>("Arial.ttf");
            /*if (Manager.TMPInstalled("TMPro"))
            {

            }
            else
            {

            } */

            if (subStateName == "Tilt")
            {
                GameObject systemBody = new GameObject("Body");
                systemBody.transform.SetParent(systemScreen.transform);
                RectTransform bodyRect = systemBody.AddComponent<RectTransform>();
                AddRectComp(bodyRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), Vector2.zero, default);
                //bodyRect.anchoredPosition = Vector2.zero;
                systemBody.AddComponent<CanvasRenderer>();

                Text systemBodyText = systemBody.AddComponent<Text>();
                AddTextComp(systemBodyText, PopFont, 70, TextAnchor.MiddleCenter);
                systemBody.AddComponent<GCGDKText>();
                systemBody.GetComponent<GCGDKText>().content = DynamicTextCategory.TiltBody;
                Text systemHeaderText = systemHeader.AddComponent<Text>();
                AddTextComp(systemHeaderText, PopFont, 70, TextAnchor.MiddleCenter);
                systemHeader.AddComponent<GCGDKText>();
                systemHeader.GetComponent<GCGDKText>().content = DynamicTextCategory.TiltHeader;

                GameObject cashoutPrompt = new GameObject("Cashout Prompt");
                cashoutPrompt.transform.SetParent(systemScreen.transform);
                RectTransform cashoutRect = cashoutPrompt.AddComponent<RectTransform>();
                AddRectComp(cashoutRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, -250), default);
                cashoutPrompt.AddComponent<CanvasRenderer>();
                Text cashoutPromptText = cashoutPrompt.AddComponent<Text>();
                AddTextComp(cashoutPromptText, PopFont, 70, TextAnchor.MiddleCenter);
                cashoutPrompt.AddComponent<GCGDKText>();
                cashoutPrompt.GetComponent<GCGDKText>().content = DynamicTextCategory.TiltCashoutPrompt;

                //Manager.DynamicText.populatedTextList.Clear();
                Manager.DynamicText.populatedTextList.Add(systemHeader.GetComponent<GCGDKText>());
                Manager.DynamicText.populatedTextList.Add(systemBody.GetComponent<GCGDKText>());
                Manager.DynamicText.populatedTextList.Add(cashoutPrompt.GetComponent<GCGDKText>());

                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.TiltHeader, Manager.Codespace.Link.codespaceStateString);
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.TiltBody, Manager.Codespace.Link.tiltMessages);
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.TiltCashoutPrompt, ((Manager.Codespace.Link.codespaceState == 25 || Manager.Codespace.Link.codespaceState == 26) && (Manager.Codespace.Link.cashableValue + Manager.Codespace.Link.nonRestrictedValue) > 0) ? "Please Cash Out\nCashable Balance: " + Manager.Currencify(Manager.Codespace.Link.cashableValue + Manager.Codespace.Link.nonRestrictedValue) : "");
            } else if (subStateName == "Emulation")
            {

                Text headerText = systemHeader.AddComponent<Text>();
                AddTextComp(headerText, PopFont, 70, TextAnchor.MiddleCenter, Color.white, "Emulation");

                GameObject toggleGroup = new GameObject("ToggleGroup");
                toggleGroup.transform.SetParent(systemScreen.transform);
                RectTransform toggleRect = toggleGroup.AddComponent<RectTransform>();
                toggleGroup.AddComponent<CanvasRenderer>();
                toggleRect.anchoredPosition = new Vector2(0, 0);
                ToggleGroup groupToggleComp = toggleGroup.AddComponent<ToggleGroup>();
                groupToggleComp.allowSwitchOff = true;

                GameObject toggleBonus = new GameObject("ToggleBonus");
                toggleBonus.transform.SetParent(toggleGroup.transform);
                RectTransform toggleBonusRect = toggleBonus.AddComponent<RectTransform>();
                toggleBonus.AddComponent<CanvasRenderer>();
                toggleBonusRect.anchoredPosition = new Vector2(-875, 360);
                toggleBonusRect.localScale = new Vector3(5, 5, 1);
                Toggle bonusToggleComp = toggleBonus.AddComponent<Toggle>();
                bonusToggleComp.toggleTransition = Toggle.ToggleTransition.Fade;
                bonusToggleComp.group = groupToggleComp;
                bonusToggleComp.onValueChanged.AddListener(Manager.Emulation.DoBonus);
                Manager.Emulation.bonusRoundToggle = bonusToggleComp;

                GameObject toggleMinigame = new GameObject("ToggleMinigame");
                toggleMinigame.transform.SetParent(toggleGroup.transform);
                RectTransform toggleMinigameRect = toggleMinigame.AddComponent<RectTransform>();
                toggleMinigame.AddComponent<CanvasRenderer>();
                toggleMinigameRect.anchoredPosition = new Vector2(-875, 50);
                toggleMinigameRect.localScale = new Vector3(5, 5, 1);
                Toggle minigameToggleComp = toggleMinigame.AddComponent<Toggle>();
                minigameToggleComp.toggleTransition = Toggle.ToggleTransition.Fade;
                minigameToggleComp.group = groupToggleComp;
                minigameToggleComp.onValueChanged.AddListener(Manager.Emulation.DoMiniGame);
                Manager.Emulation.miniGameToggle = minigameToggleComp;

                GameObject toggleCash = new GameObject("ToggleCash");
                toggleCash.transform.SetParent(toggleGroup.transform);
                RectTransform toggleCashRect = toggleCash.AddComponent<RectTransform>();
                toggleCash.AddComponent<CanvasRenderer>();
                toggleCashRect.anchoredPosition = new Vector2(-875, -190);
                toggleCashRect.localScale = new Vector3(5, 5, 1);
                Toggle cashToggleComp = toggleCash.AddComponent<Toggle>();
                cashToggleComp.toggleTransition = Toggle.ToggleTransition.Fade;
                cashToggleComp.group = groupToggleComp;
                cashToggleComp.onValueChanged.AddListener(Manager.Emulation.DoCash);
                Manager.Emulation.cashToggle = cashToggleComp;

                GameObject toggleTokens = new GameObject("ToggleTokens");
                toggleTokens.transform.SetParent(toggleGroup.transform);
                RectTransform toggleTokensRect = toggleTokens.AddComponent<RectTransform>();
                toggleTokens.AddComponent<CanvasRenderer>();
                toggleTokensRect.anchoredPosition = new Vector2(-875, -270);
                toggleTokensRect.localScale = new Vector3(5, 5, 1);
                Toggle tokensToggleComp = toggleTokens.AddComponent<Toggle>();
                tokensToggleComp.toggleTransition = Toggle.ToggleTransition.Fade;
                tokensToggleComp.group = groupToggleComp;
                tokensToggleComp.onValueChanged.AddListener(Manager.Emulation.DoTokens);
                Manager.Emulation.tokensToggle = tokensToggleComp;

                GameObject toggleProgressive = new GameObject("ToggleProgressive");
                toggleProgressive.transform.SetParent(toggleGroup.transform);
                RectTransform toggleProgressiveRect = toggleProgressive.AddComponent<RectTransform>();
                toggleProgressive.AddComponent<CanvasRenderer>();
                toggleProgressiveRect.anchoredPosition = new Vector2(-875, -350);
                toggleProgressiveRect.localScale = new Vector3(5, 5, 1);
                Toggle tokensProgressiveComp = toggleProgressive.AddComponent<Toggle>();
                tokensProgressiveComp.toggleTransition = Toggle.ToggleTransition.Fade;
                tokensProgressiveComp.group = groupToggleComp;
                tokensProgressiveComp.onValueChanged.AddListener(Manager.Emulation.DoProgressive);
                Manager.Emulation.progressiveToggle = tokensProgressiveComp;

                List<GameObject> toggleList = new List<GameObject> { toggleBonus, toggleMinigame, toggleCash, toggleTokens, toggleProgressive };

                foreach (GameObject toggleItem in toggleList)
                {
                    GameObject background = new GameObject("Background");
                    background.transform.SetParent(toggleItem.transform);
                    RectTransform backgroundRect = background.AddComponent<RectTransform>();
                    AddRectComp(backgroundRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), Vector2.zero, new Vector2(50, 50));
                    background.AddComponent<CanvasRenderer>();
                    Image backgroundImage = background.AddComponent<Image>();
                    toggleItem.GetComponent<Toggle>().targetGraphic = backgroundImage;

                    GameObject checkmark = new GameObject("Checkmark");
                    checkmark.transform.SetParent(background.transform);
                    RectTransform checkmarkRect = checkmark.AddComponent<RectTransform>();
                    AddRectComp(checkmarkRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), Vector2.zero, new Vector2(50, 50));
                    checkmark.AddComponent<CanvasRenderer>();
                    Image checkmarkImage = checkmark.AddComponent<Image>();
                    checkmarkImage.color = Color.black;
                    toggleItem.GetComponent<Toggle>().graphic = checkmarkImage;

                    GameObject descriptionText = new GameObject("Description Text");
                    descriptionText.transform.SetParent(toggleItem.transform);
                    RectTransform descriptionRect = descriptionText.AddComponent<RectTransform>();
                    AddRectComp(descriptionRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(112, 0), new Vector2(Screen.width, 200));
                    descriptionText.AddComponent<CanvasRenderer>();
                    Text textComp = descriptionText.AddComponent<Text>();
                    textComp.raycastTarget = false;
                    AddTextComp(textComp, PopFont, 40, TextAnchor.MiddleLeft);
                    if (toggleItem == toggleBonus)
                    {
                        textComp.text = "Bonus (Dunk) Game Rounds";
                    } 
                    else if (toggleItem == toggleMinigame)
                    {
                        textComp.text = "Shoot Mini-Game Rounds";
                    }
                    else if (toggleItem == toggleCash)
                    {
                        textComp.text = "Cash Multiplier";
                    }
                    else if (toggleItem == toggleTokens)
                    {
                        textComp.text = "Tokens";
                    } else if (toggleItem == toggleProgressive)
                    {
                        textComp.text = "WIN PROGRESSIVE";
                    }
                }

                GameObject cancelButton = new GameObject("Cancel");
                cancelButton.transform.SetParent(systemScreen.transform);
                RectTransform cancelButtonRect = cancelButton.AddComponent<RectTransform>();
                AddRectComp(cancelButtonRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-(Screen.width / 3), -Screen.height), new Vector2(320, 80));
                cancelButton.AddComponent<CanvasRenderer>();
                Image cancelImage = cancelButton.AddComponent<Image>();
                Button cancelComp = cancelButton.AddComponent<Button>();
                cancelComp.targetGraphic = cancelImage;
                cancelComp.onClick.AddListener(Manager.Emulation.Cancel);

                GameObject cancelText = new GameObject("Cancel Text");
                cancelText.transform.SetParent(cancelButton.transform);
                RectTransform cancelTextRect = cancelText.AddComponent<RectTransform>();
                AddRectComp(cancelTextRect, Vector2.zero, Vector2.one, Vector2.zero, default);
                Text cancelTextComp = cancelText.AddComponent<Text>();
                AddTextComp(cancelTextComp, PopFont, 70, TextAnchor.MiddleCenter, Color.black, "Cancel");

                GameObject setButton = new GameObject("Set");
                setButton.transform.SetParent(systemScreen.transform);
                RectTransform setButtonRect = setButton.AddComponent<RectTransform>();
                AddRectComp(setButtonRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(Screen.width / 3, -Screen.height), new Vector2(320, 80));
                setButton.AddComponent<CanvasRenderer>();
                Image setImage = setButton.AddComponent<Image>();
                Button setComp = setButton.AddComponent<Button>();
                setComp.targetGraphic = setImage;
                setComp.onClick.AddListener(Manager.Emulation.Set);

                GameObject setText = new GameObject("Set Text");
                setText.transform.SetParent(setButton.transform);
                RectTransform setTextRect = setText.AddComponent<RectTransform>();
                AddRectComp(setTextRect, Vector2.zero, Vector2.one, Vector2.zero, default);
                Text setTextComp = setText.AddComponent<Text>();
                AddTextComp(setTextComp, PopFont, 70, TextAnchor.MiddleCenter, Color.black, "Set");

                Manager.Emulation.bonusDropdownsTier1.Clear();
                Manager.Emulation.bonusDropdownsTier2.Clear();
                Manager.Emulation.bonusDropdownsTier3.Clear();
                Manager.Emulation.miniGameDropdowns.Clear();
                for (int i = 0; i < 4; i++)
                {
                    GameObject mainDropdown = null;
                    RectTransform mainDropdownRect = null;
                    HorizontalLayoutGroup mainHorizontalLayout = null;

                    if (i == 3)
                    {
                        mainDropdown = new GameObject("Minigame Dropdowns");
                        mainDropdown.transform.SetParent(systemScreen.transform);
                        mainDropdownRect = mainDropdown.AddComponent<RectTransform>();
                        AddRectComp(mainDropdownRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, -60), new Vector2(1800, 120));
                    }
                    else
                    {
                        mainDropdown = new GameObject("Bonus Dropdowns Tier " + (i + 1));
                        mainDropdown.transform.SetParent(systemScreen.transform);
                        mainDropdownRect = mainDropdown.AddComponent<RectTransform>();
                        AddRectComp(mainDropdownRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, (260 - (i * 80))), new Vector2(1800, 120));
                    }

                    mainHorizontalLayout = mainDropdown.AddComponent<HorizontalLayoutGroup>();
                    mainHorizontalLayout.spacing = 15;
                    mainHorizontalLayout.childAlignment = TextAnchor.UpperLeft;
                    mainHorizontalLayout.childControlWidth = true;
                    mainHorizontalLayout.childControlHeight = false;
                    mainHorizontalLayout.childForceExpandHeight = true;
                    mainHorizontalLayout.childForceExpandHeight = true;

                    for (int a = 0; a < 11; a++)
                    {
                        GameObject DropdownObj = null;
                        RectTransform DropdownRect = null;
                        Image DropdownImage = null;
                        Dropdown DropdownComp = null;
                        if (i == 3)
                        {
                            if (a < 5)
                            {
                                DropdownObj = new GameObject("MinigameDropdown " + (a + 1));
                                DropdownObj.transform.SetParent(mainDropdown.transform);
                                DropdownRect = DropdownObj.AddComponent<RectTransform>();
                                AddRectComp(DropdownRect, new Vector2(0.5f, 1), new Vector2(0.5f, 1), new Vector2(100 + (i * 250), -55), new Vector2(210, 120));
                                DropdownObj.AddComponent<CanvasRenderer>();
                                DropdownImage = DropdownObj.AddComponent<Image>();
                                DropdownComp = DropdownObj.AddComponent<Dropdown>();
                                DropdownComp.targetGraphic = DropdownImage;
                                if (a == 0)
                                {
                                    DropdownComp.AddOptions(Manager.Emulation.minigameFirstDropdownOptionsList);
                                }
                                else
                                {
                                    DropdownComp.AddOptions(Manager.Emulation.minigameDropdownOptionsList);
                                }
                                Manager.Emulation.miniGameDropdowns.Add(DropdownObj.transform);
                            }
                            else if (a == 5)
                            {
                                DropdownObj = new GameObject("Minigame Rounds Dropdown");
                                DropdownObj.transform.SetParent(systemScreen.transform);
                                DropdownRect = DropdownObj.AddComponent<RectTransform>();
                                AddRectComp(DropdownRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-20, 50), new Vector2(120, 65));
                                DropdownObj.AddComponent<CanvasRenderer>();
                                DropdownImage = DropdownObj.AddComponent<Image>();
                                DropdownComp = DropdownObj.AddComponent<Dropdown>();
                                DropdownComp.targetGraphic = DropdownImage;
                                DropdownComp.AddOptions(Manager.Emulation.minigameRoundsList);
                                DropdownComp.onValueChanged.AddListener(Manager.Emulation.MiniGameRounds);
                                Manager.Emulation.miniGameRoundsDropdown = DropdownComp;
                            }
                            else if (a == 6)
                            {
                                DropdownObj = new GameObject("Cash Dropdown");
                                DropdownObj.transform.SetParent(systemScreen.transform);
                                DropdownRect = DropdownObj.AddComponent<RectTransform>();
                                AddRectComp(DropdownRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-200, -185), new Vector2(300, 65));
                                DropdownObj.AddComponent<CanvasRenderer>();
                                DropdownImage = DropdownObj.AddComponent<Image>();
                                DropdownComp = DropdownObj.AddComponent<Dropdown>();
                                DropdownComp.targetGraphic = DropdownImage;
                                DropdownComp.AddOptions(Manager.Emulation.cashMultiplierList);
                                DropdownComp.onValueChanged.AddListener(Manager.Emulation.CashMultiplier);
                                Manager.Emulation.cashDropdown = DropdownComp;
                            }
                            else if (a == 7)
                            {
                                DropdownObj = new GameObject("Token Dropdown");
                                DropdownObj.transform.SetParent(systemScreen.transform);
                                DropdownRect = DropdownObj.AddComponent<RectTransform>();
                                AddRectComp(DropdownRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-330, -270), new Vector2(300, 65));
                                DropdownObj.AddComponent<CanvasRenderer>();
                                DropdownImage = DropdownObj.AddComponent<Image>();
                                DropdownComp = DropdownObj.AddComponent<Dropdown>();
                                DropdownComp.targetGraphic = DropdownImage;
                                DropdownComp.AddOptions(Manager.Emulation.tokenMultiplierList);
                                DropdownComp.onValueChanged.AddListener(Manager.Emulation.TokensMultiplier);
                                Manager.Emulation.tokenDropdown = DropdownComp;

                                GameObject TokenDescription = new GameObject("Token Description");
                                TokenDescription.transform.SetParent(DropdownObj.transform);
                                RectTransform TokenDescRect = TokenDescription.AddComponent<RectTransform>();
                                AddRectComp(TokenDescRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(685, 0), new Vector2(1400, 80));
                                TokenDescription.AddComponent<CanvasRenderer>();
                                Text tokenDescText = TokenDescription.AddComponent<Text>();
                                AddTextComp(tokenDescText, PopFont, 40, TextAnchor.MiddleCenter, Color.white, "(Actual value 1x, 2x, 4x, 6x, 10x - based on selected bet)");
                            }
                            else { break; }

                        } else if (a == 10 && i == 2)
                        {
                            DropdownObj = new GameObject("Bonus Rounds Dropdown");
                            DropdownObj.transform.SetParent(systemScreen.transform);
                            DropdownRect = DropdownObj.AddComponent<RectTransform>();
                            AddRectComp(DropdownRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-20, 370), new Vector2(120, 65));
                            DropdownObj.AddComponent<CanvasRenderer>();
                            DropdownImage = DropdownObj.AddComponent<Image>();
                            DropdownComp = DropdownObj.AddComponent<Dropdown>();
                            DropdownComp.targetGraphic = DropdownImage;
                            DropdownComp.AddOptions(Manager.Emulation.bonusRoundsList);
                            DropdownComp.onValueChanged.AddListener(Manager.Emulation.BonusRounds);
                            Manager.Emulation.bonusRoundsDropdown = DropdownComp;
                        } else if (a < 10 && i < 3)
                        {
                            DropdownObj = new GameObject("BonusDropdown " + (a + 1));
                            DropdownObj.transform.SetParent(mainDropdown.transform);
                            DropdownRect = DropdownObj.AddComponent<RectTransform>();
                            AddRectComp(DropdownRect, new Vector2(0.5f, 1), new Vector2(0.5f, 1), new Vector2(100 + (i * 250), -55), new Vector2(210, 60));
                            DropdownObj.AddComponent<CanvasRenderer>();
                            DropdownImage = DropdownObj.AddComponent<Image>();
                            DropdownComp = DropdownObj.AddComponent<Dropdown>();
                            DropdownComp.targetGraphic = DropdownImage;
                            DropdownComp.AddOptions(Manager.Emulation.bonusDropdownOptionsList);

                            if (i == 0)
                            {
                                Manager.Emulation.bonusDropdownsTier1.Add(DropdownObj.transform);
                            } else if (i == 1)
                            {
                                Manager.Emulation.bonusDropdownsTier2.Add(DropdownObj.transform);
                            } else if (i == 2)
                            {
                                Manager.Emulation.bonusDropdownsTier3.Add(DropdownObj.transform);
                            }
                        }

                        if (a > 9 && i < 2) { break; }

                        GameObject bonusDropdownLabel = new GameObject("Label");
                        bonusDropdownLabel.transform.SetParent(DropdownObj.transform);
                        RectTransform bonusLabelRect = bonusDropdownLabel.AddComponent<RectTransform>();
                        AddRectComp(bonusLabelRect, Vector2.zero, Vector2.one, Vector2.zero, default);
                        bonusLabelRect.offsetMin = new Vector2(0, 0);
                        bonusLabelRect.offsetMax = new Vector2(0, 0);
                        bonusDropdownLabel.AddComponent<CanvasRenderer>();
                        Text bonusLabelText = bonusDropdownLabel.AddComponent<Text>();
                        AddTextComp(bonusLabelText, PopFont, 50, TextAnchor.MiddleCenter, Color.black, "1.5x");
                        DropdownComp.captionText = bonusLabelText;

                        GameObject bonusTemplate = new GameObject("Template");
                        bonusTemplate.transform.SetParent(DropdownObj.transform);
                        bonusTemplate.SetActive(false);
                        RectTransform bonusTemplateRect = bonusTemplate.AddComponent<RectTransform>();
                        bonusTemplateRect.pivot = new Vector2(0.5f, 1);
                        if (i == 3) {

                            AddRectComp(bonusTemplateRect, Vector2.zero, new Vector2(1, 0), Vector2.zero, new Vector2(0, 450));
                        } else
                        {
                            AddRectComp(bonusTemplateRect, Vector2.zero, new Vector2(1, 0), Vector2.zero, new Vector2(0, 700));
                        }
                        Image bonusTemplateImage = bonusTemplate.AddComponent<Image>();
                        ScrollRect bonusTemplateScrollRect = bonusTemplate.AddComponent<ScrollRect>();
                        bonusTemplateScrollRect.horizontal = false;
                        bonusTemplateScrollRect.vertical = true;
                        bonusTemplateScrollRect.movementType = ScrollRect.MovementType.Clamped;
                        bonusTemplateScrollRect.inertia = true;
                        bonusTemplateScrollRect.decelerationRate = 0.135f;
                        bonusTemplateScrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
                        bonusTemplateScrollRect.verticalScrollbarSpacing = -3;
                        DropdownComp.template = bonusTemplateRect;

                        GameObject viewport = new GameObject("Viewport");
                        viewport.transform.SetParent(bonusTemplate.transform);
                        RectTransform viewportRect = viewport.AddComponent<RectTransform>();
                        viewportRect.pivot = new Vector2(0, 1);
                        viewportRect.offsetMin = new Vector2(0, 0);
                        viewportRect.offsetMax = new Vector2(0, 0);
                        AddRectComp(viewportRect, Vector2.zero, Vector2.one, Vector2.zero, Vector2.zero);
                        viewport.AddComponent<CanvasRenderer>();
                        viewport.AddComponent<Mask>();
                        viewport.AddComponent<Image>();
                        bonusTemplateScrollRect.viewport = viewportRect;

                        GameObject content = new GameObject("Content");
                        content.transform.SetParent(viewport.transform);
                        RectTransform contentRect = content.AddComponent<RectTransform>();
                        if (i == 3)
                        {
                            contentRect.pivot = new Vector2(0.5f, 1);
                        }
                        AddRectComp(contentRect, new Vector2(0, 1), Vector2.one, Vector2.zero, new Vector2(0, 70));
                        bonusTemplateScrollRect.content = contentRect;

                        GameObject item = new GameObject("Item");
                        item.transform.SetParent(content.transform);
                        RectTransform itemRect = item.AddComponent<RectTransform>();
                        AddRectComp(itemRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), Vector2.zero, new Vector2(0, 70));
                        Toggle itemToggle = item.AddComponent<Toggle>();
                        itemToggle.toggleTransition = Toggle.ToggleTransition.Fade;

                        GameObject itemBackground = new GameObject("Item Background");
                        itemBackground.transform.SetParent(item.transform);
                        RectTransform itemBackgroundRect = itemBackground.AddComponent<RectTransform>();
                        AddRectComp(itemBackgroundRect, Vector2.zero, Vector2.one, Vector2.zero, default);
                        itemBackgroundRect.offsetMin = Vector2.zero;
                        itemBackgroundRect.offsetMax = Vector2.zero;
                        itemBackground.AddComponent<CanvasRenderer>();
                        Image itemBackgroundImage = itemBackground.AddComponent<Image>();
                        itemToggle.targetGraphic = itemBackgroundImage;

                        GameObject itemCheckmark = new GameObject("Item Checkmark");
                        itemCheckmark.transform.SetParent(item.transform);
                        RectTransform itemCheckmarkRect = itemCheckmark.AddComponent<RectTransform>();
                        AddRectComp(itemCheckmarkRect, new Vector2(0, 0.5f), new Vector2(0, 0.5f), new Vector2(28, 0), new Vector2(55, 68));
                        itemCheckmark.AddComponent<CanvasRenderer>();
                        Image itemCheckmarkImage = itemCheckmark.AddComponent<Image>();
                        itemCheckmarkImage.color = Color.black;
                        itemToggle.graphic = itemCheckmarkImage;

                        GameObject itemLabel = new GameObject("Item Label");
                        itemLabel.transform.SetParent(item.transform);
                        RectTransform itemLabelRect = itemLabel.AddComponent<RectTransform>();
                        AddRectComp(itemLabelRect, Vector2.zero, Vector2.one, new Vector2(50, 0), default);
                        itemLabelRect.offsetMin = new Vector2(56, 2);
                        itemLabelRect.offsetMax = new Vector2(10, 1);
                        itemLabel.AddComponent<CanvasRenderer>();
                        Text itemLabelText = itemLabel.AddComponent<Text>();
                        AddTextComp(itemLabelText, PopFont, 40, TextAnchor.MiddleCenter, Color.black, "Option A");
                        DropdownComp.itemText = itemLabelText;

                        if (i > 2)
                        {
                            GameObject scrollbar = new GameObject("ScrollBar");
                            scrollbar.transform.SetParent(bonusTemplate.transform);
                            RectTransform scrollbarRect = scrollbar.AddComponent<RectTransform>();
                            AddRectComp(scrollbarRect, new Vector2(1, 0), Vector2.one, new Vector2 (-10, 0), new Vector2(20, 0));
                            scrollbar.AddComponent<CanvasRenderer>();
                            scrollbar.AddComponent<Image>();
                            Scrollbar scrollbarComp = scrollbar.AddComponent<Scrollbar>();
                            scrollbarComp.direction = Scrollbar.Direction.BottomToTop;
                            scrollbarComp.value = 0;
                            scrollbarComp.size = 0.5f;
                            scrollbarComp.numberOfSteps = 0;
                            bonusTemplateScrollRect.verticalScrollbar = scrollbarComp;

                            GameObject slidingArea = new GameObject("Sliding Area");
                            slidingArea.transform.SetParent(scrollbar.transform);
                            RectTransform slidingAreaRect = slidingArea.AddComponent<RectTransform>();
                            AddRectComp(slidingAreaRect, Vector2.zero, Vector2.one, default, default);
                            slidingAreaRect.offsetMin = new Vector2(10, 10);
                            slidingAreaRect.offsetMax = new Vector2(10, 10);

                            GameObject handle = new GameObject("Handle");
                            handle.transform.SetParent(slidingArea.transform);
                            RectTransform handleRect = handle.AddComponent<RectTransform>();
                            AddRectComp(handleRect, Vector2.zero, Vector2.one, default, default);
                            handleRect.offsetMin = new Vector2(-10, -10);
                            handleRect.offsetMax = new Vector2(-10, -10);
                            handle.AddComponent<CanvasRenderer>();
                            Image handleImage = handle.AddComponent<Image>();
                            handleImage.color = Color.black;
                            scrollbarComp.targetGraphic = handleImage;
                            scrollbarComp.handleRect = handleRect;
                        }

                    }
                }
            } else if (subStateName == "Handpay" || subStateName == "JackpotHandpay")
            {
                headerRect.anchoredPosition = new Vector2(0, 150);

                Text systemHeaderText = systemHeader.AddComponent<Text>();
                DynamicTextCategory handpayCategory = DynamicTextCategory.None;
                if (subStateName == "Handpay")
                {
                    AddTextComp(systemHeaderText, PopFont, 70, TextAnchor.MiddleCenter, Color.white, "Handpay");
                    handpayCategory = DynamicTextCategory.HandpayAmount;
                } else if (subStateName == "JackpotHandpay")
                {
                    AddTextComp(systemHeaderText, PopFont, 70, TextAnchor.MiddleCenter, Color.white, "Jackpot Handpay");
                    handpayCategory = DynamicTextCategory.JackpotHandpayAmount;
                }

                GameObject systemBody = new GameObject("Body");
                systemBody.transform.SetParent(systemScreen.transform);
                RectTransform bodyRect = systemBody.AddComponent<RectTransform>();
                AddRectComp(bodyRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), Vector2.zero, default);
                systemBody.AddComponent<CanvasRenderer>();
                Text systemBodyText = systemBody.AddComponent<Text>();
                AddTextComp(systemBodyText, PopFont, 90, TextAnchor.MiddleCenter, Color.white);
                systemBody.AddComponent<GCGDKText>();
                systemBody.GetComponent<GCGDKText>().content = handpayCategory;

                //Manager.DynamicText.populatedTextList.Clear();
                Manager.DynamicText.populatedTextList.Add(systemBody.GetComponent<GCGDKText>());

                Manager.Codespace.HandpayValueChanged();
            } else if (subStateName == "Underfunded")
            {
                Text systemHeaderText = systemHeader.AddComponent<Text>();
                AddTextComp(systemHeaderText, PopFont, 70, TextAnchor.MiddleCenter, Color.white, "Underfunded");

                GameObject systemBody = new GameObject("Body");
                systemBody.transform.SetParent(systemScreen.transform);
                RectTransform bodyRect = systemBody.AddComponent<RectTransform>();
                AddRectComp(bodyRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, 150), new Vector2(1800, 300));
                systemBody.AddComponent<CanvasRenderer>();
                Text systemBodyText = systemBody.AddComponent<Text>();
                AddTextComp(systemBodyText, PopFont, 100, TextAnchor.MiddleCenter, Color.white, "ADD MONEY TO CONTINUE PLAYING");

                GameObject Dissuade = new GameObject("Dissuade");
                Dissuade.transform.SetParent(systemScreen.transform);
                RectTransform dissuadeRect = Dissuade.AddComponent<RectTransform>();
                AddRectComp(dissuadeRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), Vector2.zero, new Vector2(1800, 300));
                Dissuade.AddComponent<CanvasRenderer>();
                Text dissuadeText = Dissuade.AddComponent<Text>();
                AddTextComp(dissuadeText, PopFont, 80, TextAnchor.MiddleCenter, Color.white);
                Dissuade.AddComponent<GCGDKText>();
                Dissuade.GetComponent<GCGDKText>().content = DynamicTextCategory.DiscourageAbandonment;

                GameObject Timer = new GameObject("Timer");
                Timer.transform.SetParent(systemScreen.transform);
                RectTransform timerRect = Timer.AddComponent<RectTransform>();
                AddRectComp(timerRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, -150), new Vector2(1800, 300));
                Timer.AddComponent<CanvasRenderer>();
                Text timerText = Timer.AddComponent<Text>();
                AddTextComp(timerText, PopFont, 80, TextAnchor.MiddleCenter, Color.red);
                Timer.AddComponent<GCGDKText>();
                Timer.GetComponent<GCGDKText>().content = DynamicTextCategory.Timer;

                //Manager.DynamicText.populatedTextList.Clear();
                Manager.DynamicText.populatedTextList.Add(Dissuade.GetComponent<GCGDKText>());
                Manager.DynamicText.populatedTextList.Add(Timer.GetComponent<GCGDKText>());

                //StartCoroutine(Manager.PlayLoop.Underfunded());
                //Manager.Cashout.DiscourageAbandonment();
            }
            else if (subStateName == "NonCashableCredit")
            {
                Text systemHeaderText = systemHeader.AddComponent<Text>();
                AddTextComp(systemHeaderText, PopFont, 70, TextAnchor.MiddleCenter, Color.white, "Non-Cashable Credit");

                GameObject info1 = new GameObject("Info 1");
                info1.transform.SetParent(systemScreen.transform);
                RectTransform info1Rect = info1.AddComponent<RectTransform>();
                AddRectComp(info1Rect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, 250), new Vector2(1800, 300));
                info1.AddComponent<CanvasRenderer>();
                Text info1Text = info1.AddComponent<Text>();
                AddTextComp(info1Text, PopFont, 80, TextAnchor.MiddleCenter, Color.white, "A non-cashable balance of");

                GameObject Amount = new GameObject("Amount");
                Amount.transform.SetParent(systemScreen.transform);
                RectTransform amountRect = Amount.AddComponent<RectTransform>();
                AddRectComp(amountRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, 150), new Vector2(1800, 300));
                Amount.AddComponent<CanvasRenderer>();
                Text amountText = Amount.AddComponent<Text>();
                AddTextComp(amountText, PopFont, 100, TextAnchor.MiddleCenter, Color.white);
                Amount.AddComponent<GCGDKText>();
                Amount.GetComponent<GCGDKText>().content = DynamicTextCategory.NonCashableBalance;

                GameObject info2 = new GameObject("Info 2");
                info2.transform.SetParent(systemScreen.transform);
                RectTransform info2Rect = info2.AddComponent<RectTransform>();
                AddRectComp(info2Rect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, 50), new Vector2(1800, 300));
                info2.AddComponent<CanvasRenderer>();
                Text info2Text = info2.AddComponent<Text>();
                AddTextComp(info2Text, PopFont, 80, TextAnchor.MiddleCenter, Color.white, "Exists on this machine.");

                GameObject info3 = new GameObject("Info 3");
                info3.transform.SetParent(systemScreen.transform);
                RectTransform info3Rect = info3.AddComponent<RectTransform>();
                AddRectComp(info3Rect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, -200), new Vector2(1800, 300));
                info3.AddComponent<CanvasRenderer>();
                Text info3Text = info3.AddComponent<Text>();
                AddTextComp(info3Text, PopFont, 80, TextAnchor.MiddleCenter, Color.white, "This money cannot be cashed out, only wagered.");

                GameObject okbutton = new GameObject("OK");
                okbutton.transform.SetParent(systemScreen.transform);
                RectTransform okbuttonRect = okbutton.AddComponent<RectTransform>();
                AddRectComp(okbuttonRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, -Screen.height), new Vector2(400, 100));
                okbutton.AddComponent<CanvasRenderer>();
                Image okImage = okbutton.AddComponent<Image>();
                GCGDKButton okbuttonComp = okbutton.AddComponent<GCGDKButton>();
                okbuttonComp.targetGraphic = okImage;
                okbuttonComp.thisButton = ButtonType.Confirm;

                //Manager.DynamicText.populatedTextList.Clear();
                Manager.DynamicText.populatedTextList.Add(Amount.GetComponent<GCGDKText>());
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.NonCashableBalance, Manager.Codespace.Link.restrictedValue);
            }
            else if (subStateName == "CashOutConfirm")
            {
                Text systemHeaderText = systemHeader.AddComponent<Text>();
                AddTextComp(systemHeaderText, PopFont, 70, TextAnchor.MiddleCenter, Color.white, "Cashout Confirm");

                GameObject Question = new GameObject("Body");
                Question.transform.SetParent(systemScreen.transform);
                RectTransform questionRect = Question.AddComponent<RectTransform>();
                AddRectComp(questionRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, 200), new Vector2(1800, 300));
                Question.AddComponent<CanvasRenderer>();
                Text questionText = Question.AddComponent<Text>();
                AddTextComp(questionText, PopFont, 80, TextAnchor.MiddleCenter, Color.white, "ARE YOU SURE YOU WANT TO CASH OUT?");

                GameObject Dissuade = new GameObject("Dissuade");
                Dissuade.transform.SetParent(systemScreen.transform);
                RectTransform dissuadeRect = Dissuade.AddComponent<RectTransform>();
                AddRectComp(dissuadeRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, 100), new Vector2(1800, 300));
                Dissuade.AddComponent<CanvasRenderer>();
                Text dissuadeText = Dissuade.AddComponent<Text>();
                AddTextComp(dissuadeText, PopFont, 80, TextAnchor.MiddleCenter, Color.white);
                Dissuade.AddComponent<GCGDKText>();
                Dissuade.GetComponent<GCGDKText>().content = DynamicTextCategory.DiscourageAbandonment;

                GameObject Amount = new GameObject("Cashout Amount");
                Amount.transform.SetParent(systemScreen.transform);
                RectTransform amountRect = Amount.AddComponent<RectTransform>();
                AddRectComp(amountRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, -100), new Vector2(1800, 300));
                Amount.AddComponent<CanvasRenderer>();
                Text amountText = Amount.AddComponent<Text>();
                AddTextComp(amountText, PopFont, 100, TextAnchor.MiddleCenter, Color.white);
                Amount.AddComponent<GCGDKText>();
                Amount.GetComponent<GCGDKText>().content = DynamicTextCategory.CashoutAmount;

                GameObject cashoutButton = new GameObject("Continue");
                cashoutButton.transform.SetParent(systemScreen.transform);
                RectTransform cashoutButtonRect = cashoutButton.AddComponent<RectTransform>();
                AddRectComp(cashoutButtonRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-(Screen.width / 3), -Screen.height), new Vector2(400, 100));
                cashoutButton.AddComponent<CanvasRenderer>();
                Image cashoutImage = cashoutButton.AddComponent<Image>();
                GCGDKButton cashoutComp = cashoutButton.AddComponent<GCGDKButton>();
                cashoutComp.targetGraphic = cashoutImage;
                cashoutComp.thisButton = ButtonType.Cashout;

                GameObject cashoutText = new GameObject("Cashout Text");
                cashoutText.transform.SetParent(cashoutButton.transform);
                RectTransform cashoutTextRect = cashoutText.AddComponent<RectTransform>();
                AddRectComp(cashoutTextRect, Vector2.zero, Vector2.one, Vector2.zero, default);
                Text cashoutTextComp = cashoutText.AddComponent<Text>();
                AddTextComp(cashoutTextComp, PopFont, 70, TextAnchor.MiddleCenter, Color.black, "Cashout");

                GameObject cancelButton = new GameObject("Cancel");
                cancelButton.transform.SetParent(systemScreen.transform);
                RectTransform cancelButtonRect = cancelButton.AddComponent<RectTransform>();
                AddRectComp(cancelButtonRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(Screen.width / 3, -Screen.height), new Vector2(400, 100));
                cancelButton.AddComponent<CanvasRenderer>();
                Image cancelImage = cancelButton.AddComponent<Image>();
                GCGDKButton cancelComp = cancelButton.AddComponent<GCGDKButton>();
                cancelComp.targetGraphic = cancelImage;
                cancelComp.thisButton = ButtonType.Cancel;

                GameObject cancelText = new GameObject("Cancel Text");
                cancelText.transform.SetParent(cancelButton.transform);
                RectTransform cancelTextRect = cancelText.AddComponent<RectTransform>();
                AddRectComp(cancelTextRect, Vector2.zero, Vector2.one, Vector2.zero, default);
                Text cancelTextComp = cancelText.AddComponent<Text>();
                AddTextComp(cancelTextComp, PopFont, 70, TextAnchor.MiddleCenter, Color.black, "Cancel");

                //Manager.DynamicText.populatedTextList.Clear();
                Manager.DynamicText.populatedTextList.Add(Dissuade.GetComponent<GCGDKText>());
                Manager.DynamicText.populatedTextList.Add(Amount.GetComponent<GCGDKText>());

                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.CashoutAmount, Manager.Currencify(Manager.Codespace.Link.cashableValue + Manager.Codespace.Link.nonRestrictedValue));
                //Manager.Cashout.CashoutRequested();
                //Manager.Cashout.DiscourageAbandonment();

            } else if (subStateName == "CashingOut")
            {
                Text systemHeaderText = systemHeader.AddComponent<Text>();
                AddTextComp(systemHeaderText, PopFont, 70, TextAnchor.MiddleCenter, Color.white, "Cashing Out");

                GameObject cashingOut = new GameObject("Body");
                cashingOut.transform.SetParent(systemScreen.transform);
                RectTransform cashingOutRect = cashingOut.AddComponent<RectTransform>();
                AddRectComp(cashingOutRect, new Vector2(0, 0.5f), new Vector2(1, 0.5f), new Vector2(0, 50), new Vector2(1800, 300));
                cashingOut.AddComponent<CanvasRenderer>();
                Text cashingOutText = cashingOut.AddComponent<Text>();
                AddTextComp(cashingOutText, PopFont, 100, TextAnchor.MiddleCenter, Color.white, "THANK YOU FOR PLAYING!");
            } else if (subStateName == "Loading")
            {
                Text systemHeaderText = systemHeader.AddComponent<Text>();
                AddTextComp(systemHeaderText, PopFont, 100, TextAnchor.MiddleCenter, Color.white, "Loading...");
                AddRectComp(systemHeader.GetComponent<RectTransform>(), new Vector2(0, 0.5f), new Vector2(1, 0.5f), Vector2.zero, new Vector2(1800, 300));
            } else if (subStateName == "Paytable")
            {
                Text systemHeaderText = systemHeader.AddComponent<Text>();
                AddTextComp(systemHeaderText, PopFont, 80, TextAnchor.MiddleCenter, Color.white, "-Paytable-");
                AddRectComp(systemHeader.GetComponent<RectTransform>(), new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2 (-500, 480), new Vector2(1800, 300));

                GameObject headers = new GameObject("Headers");
                headers.transform.SetParent(systemScreen.transform);
                RectTransform headersRect = headers.AddComponent<RectTransform>();
                AddRectComp(headersRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), Vector2.zero, new Vector2(100, 100));

                for (int i = 0; i < 11; i++)
                {
                    GameObject paytableHeader = new GameObject();
                    paytableHeader.transform.SetParent(headers.transform);
                    RectTransform paytableHeaderRect = paytableHeader.AddComponent<RectTransform>();
                    paytableHeader.AddComponent<CanvasRenderer>();
                    Text paytableHeaderText = paytableHeader.AddComponent<Text>();
                    if (i == 0) {
                        paytableHeader.name = "Cash Header";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-620, 350), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 50, TextAnchor.MiddleCenter, Color.white, "Cash Prizes");
                    } else if (i == 1)
                    {
                        paytableHeader.name = "Minigame Header";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, 350), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 50, TextAnchor.MiddleCenter, Color.white, "Shoot Game Prizes");
                    } else if (i == 2)
                    {
                        paytableHeader.name = "Minigame Sub Header";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, 290), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 40, TextAnchor.MiddleCenter, Color.white, "(Per shot, 2 to 5 shots)");
                    }
                    else if (i == 3)
                    {
                        paytableHeader.name = "Minigame Main Header";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, 210), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 40, TextAnchor.MiddleCenter, Color.white, "Initial Shot(s)");
                    } else if (i == 4)
                    {
                        paytableHeader.name = "Minigame Final Header";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, -50), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 40, TextAnchor.MiddleCenter, Color.white, "Final Shot");
                    } else if (i == 5)
                    {
                        paytableHeader.name = "Bonus Header";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(620, 350), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 50, TextAnchor.MiddleCenter, Color.white, "Bonus Game Prizes");
                    }
                    else if (i == 6)
                    {
                        paytableHeader.name = "Bonus Sub Header";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(620, 290), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 40, TextAnchor.MiddleCenter, Color.white, "(Per bonus, 3 to 10 bonus)");
                    }
                    else if (i == 7)
                    {
                        paytableHeader.name = "Bonus Tier 1";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(620, 210), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 40, TextAnchor.MiddleCenter, Color.white, "Tier 1");
                    }
                    else if (i == 8)
                    {
                        paytableHeader.name = "Bonus Tier 2";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(620, 55), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 40, TextAnchor.MiddleCenter, Color.white, "Tier 2");
                    }
                    else if (i == 9)
                    {
                        paytableHeader.name = "Bonus Tier 3";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(620, -155), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 40, TextAnchor.MiddleCenter, Color.white, "Tier 3");
                    }
                    else if (i == 10)
                    {
                        paytableHeader.name = "Tokens";
                        AddRectComp(paytableHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-620, -260), new Vector2(600, 150));
                        AddTextComp(paytableHeaderText, PopFont, 50, TextAnchor.MiddleCenter, Color.white, "Token Prizes");
                    }
                }

                paytableDenomValues.Clear();
                paytableButtons.Clear();
                Manager.MathModel.PopMainGameValues.Clear();
                Manager.MathModel.PopMiniGameValues.Clear();
                Manager.MathModel.PopMiniGameFinalValues.Clear();
                Manager.MathModel.PopBonusRoundTier1Values.Clear();
                Manager.MathModel.PopBonusRoundTier2Values.Clear();
                Manager.MathModel.PopBonusRoundTier3Values.Clear();
                Manager.MathModel.PopTokenValues.Clear();
                Manager.MathModel.PTDenomButtonText.Clear();
                Manager.MathModel.PTDenomHighlightedButtonText.Clear();
                for (int i = 0; i < 5; i++)
                {
                    GameObject denomObject = new GameObject("Denom " + (i + 1) + " Values");
                    denomObject.transform.SetParent(systemScreen.transform);
                    RectTransform denomObjectRect = denomObject.AddComponent<RectTransform>();
                    AddRectComp(denomObjectRect, Vector2.zero, Vector2.one, Vector2.zero, default);
                    denomObject.AddComponent<CanvasGroup>();
                    paytableDenomValues.Add(denomObject);

                    GameObject cashValues = new GameObject("Cash Values");
                    cashValues.transform.SetParent(denomObject.transform);
                    RectTransform cashValuesRect = cashValues.AddComponent<RectTransform>();
                    AddRectComp(cashValuesRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-600, -85), new Vector2(600, 800));
                    GridLayoutGroup cashValuesGrid = cashValues.AddComponent<GridLayoutGroup>();
                    cashValuesGrid.cellSize = new Vector2(140, 48);
                    Manager.MathModel.PopMainGameValues.Add(cashValuesRect);

                    for (int a = 0; a < 44; a++)
                    {
                        GameObject cashMultiplier = new GameObject("Multiplier " + (a + 1));
                        cashMultiplier.transform.SetParent(cashValues.transform);
                        cashMultiplier.AddComponent<RectTransform>();
                        Text cashMultiplierText = cashMultiplier.AddComponent<Text>();
                        AddTextComp(cashMultiplierText, PopFont, 35, TextAnchor.MiddleCenter, Color.white, "$500.00");
                    }

                    GameObject minigamePrizes = new GameObject("Minigame Prizes");
                    minigamePrizes.transform.SetParent(denomObject.transform);
                    RectTransform minigamePrizesRect = minigamePrizes.AddComponent<RectTransform>();
                    AddRectComp(minigamePrizesRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, 90), new Vector2(500, 200));
                    GridLayoutGroup minigamePrizesGrid = minigamePrizes.AddComponent<GridLayoutGroup>();
                    minigamePrizesGrid.cellSize = new Vector2(160, 50);
                    Manager.MathModel.PopMiniGameValues.Add(minigamePrizesRect);

                    for (int a = 0; a < 12; a++)
                    {
                        GameObject minigameMultiplier = new GameObject("Multiplier " + (a + 1));
                        minigameMultiplier.transform.SetParent(minigamePrizes.transform);
                        minigameMultiplier.AddComponent<RectTransform>();
                        Text minigameMultiplierText = minigameMultiplier.AddComponent<Text>();
                        AddTextComp(minigameMultiplierText, PopFont, 35, TextAnchor.MiddleCenter, Color.white, "$500.00");
                    }

                    GameObject minigameFinal = new GameObject("Minigame Prizes Final");
                    minigameFinal.transform.SetParent(denomObject.transform);
                    RectTransform minigameFinalRect = minigameFinal.AddComponent<RectTransform>();
                    AddRectComp(minigameFinalRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, -170), new Vector2(500, 200));
                    GridLayoutGroup minigameFinalGrid = minigameFinal.AddComponent<GridLayoutGroup>();
                    minigameFinalGrid.cellSize = new Vector2(160, 50);
                    Manager.MathModel.PopMiniGameFinalValues.Add(minigameFinalRect);

                    for (int a = 0; a < 12; a++)
                    {
                        GameObject minigameFinalMultiplier = new GameObject("Multiplier " + (a + 1));
                        minigameFinalMultiplier.transform.SetParent(minigameFinal.transform);
                        minigameFinalMultiplier.AddComponent<RectTransform>();
                        Text minigameFinalMultiplierText = minigameFinalMultiplier.AddComponent<Text>();
                        AddTextComp(minigameFinalMultiplierText, PopFont, 35, TextAnchor.MiddleCenter, Color.white, "$500.00");
                    }

                    GameObject bonusTier1 = new GameObject("Bonus Tier 1 Prizes");
                    bonusTier1.transform.SetParent(denomObject.transform);
                    RectTransform bonusTier1Rect = bonusTier1.AddComponent<RectTransform>();
                    AddRectComp(bonusTier1Rect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(620, 90), new Vector2(500, 200));
                    GridLayoutGroup bonusTier1Grid = bonusTier1.AddComponent<GridLayoutGroup>();
                    bonusTier1Grid.cellSize = new Vector2(160, 50);
                    Manager.MathModel.PopBonusRoundTier1Values.Add(bonusTier1Rect);

                    for (int a = 0; a < 5; a++)
                    {
                        GameObject tier1Multiplier = new GameObject("Multiplier " + (a + 1));
                        tier1Multiplier.transform.SetParent(bonusTier1.transform);
                        tier1Multiplier.AddComponent<RectTransform>();
                        Text tier1MultiplierText = tier1Multiplier.AddComponent<Text>();
                        AddTextComp(tier1MultiplierText, PopFont, 35, TextAnchor.MiddleCenter, Color.white, "$500.00");
                    }

                    GameObject bonusTier2 = new GameObject("Bonus Tier 2 Prizes");
                    bonusTier2.transform.SetParent(denomObject.transform);
                    RectTransform bonusTier2Rect = bonusTier2.AddComponent<RectTransform>();
                    AddRectComp(bonusTier2Rect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(620, -65), new Vector2(500, 200));
                    GridLayoutGroup bonusTier2Grid = bonusTier2.AddComponent<GridLayoutGroup>();
                    bonusTier2Grid.cellSize = new Vector2(160, 50);
                    Manager.MathModel.PopBonusRoundTier2Values.Add(bonusTier2Rect);

                    for (int a = 0; a < 9; a++)
                    {
                        GameObject tier2Multiplier = new GameObject("Multiplier " + (a + 1));
                        tier2Multiplier.transform.SetParent(bonusTier2.transform);
                        tier2Multiplier.AddComponent<RectTransform>();
                        Text tier2MultiplierText = tier2Multiplier.AddComponent<Text>();
                        AddTextComp(tier2MultiplierText, PopFont, 35, TextAnchor.MiddleCenter, Color.white, "$500.00");
                    }

                    GameObject bonusTier3 = new GameObject("Bonus Tier 3 Prizes");
                    bonusTier3.transform.SetParent(denomObject.transform);
                    RectTransform bonusTier3Rect = bonusTier3.AddComponent<RectTransform>();
                    AddRectComp(bonusTier3Rect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(620, -275), new Vector2(500, 200));
                    GridLayoutGroup bonusTier3Grid = bonusTier3.AddComponent<GridLayoutGroup>();
                    bonusTier3Grid.cellSize = new Vector2(160, 50);
                    Manager.MathModel.PopBonusRoundTier3Values.Add(bonusTier3Rect);

                    for (int a = 0; a < 12; a++)
                    {
                        GameObject tier3Multiplier = new GameObject("Multiplier " + (a + 1));
                        tier3Multiplier.transform.SetParent(bonusTier3.transform);
                        tier3Multiplier.AddComponent<RectTransform>();
                        Text tier3MultiplierText = tier3Multiplier.AddComponent<Text>();
                        AddTextComp(tier3MultiplierText, PopFont, 35, TextAnchor.MiddleCenter, Color.white, "$500.00");
                    }

                    GameObject tokenValues = new GameObject("Token Values");
                    tokenValues.transform.SetParent(denomObject.transform);
                    RectTransform tokenValuesRect = tokenValues.AddComponent<RectTransform>();
                    AddRectComp(tokenValuesRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-620, -330), new Vector2(700, 70));
                    GridLayoutGroup tokenValuesGrid = tokenValues.AddComponent<GridLayoutGroup>();
                    tokenValuesGrid.cellSize = new Vector2(75, 40);
                    tokenValuesGrid.childAlignment = TextAnchor.UpperCenter;
                    Manager.MathModel.PopTokenValues.Add(tokenValuesRect);

                    for (int a = 0; a < 5; a++)
                    {
                        GameObject tokenMultiplier = new GameObject("Multiplier " + (a + 1));
                        tokenMultiplier.transform.SetParent(tokenValues.transform);
                        tokenMultiplier.AddComponent<RectTransform>();
                        Text tokenMultiplierText = tokenMultiplier.AddComponent<Text>();
                        AddTextComp(tokenMultiplierText, PopFont, 35, TextAnchor.MiddleCenter, Color.white, "5");
                    }
                }

                GameObject buttonDescription = new GameObject("Button Description");
                buttonDescription.transform.SetParent(systemScreen.transform);
                RectTransform buttonDescriptionRect = buttonDescription.AddComponent<RectTransform>();
                AddRectComp(buttonDescriptionRect, Vector2.one, Vector2.one, new Vector2(-620, -30), new Vector2(900, 100));
                Text buttonDescriptionText = buttonDescription.AddComponent<Text>();
                AddTextComp(buttonDescriptionText, PopFont, 35, TextAnchor.MiddleCenter, Color.white, "Select Bet to see available Win values");

                GameObject Buttons = new GameObject("Buttons");
                Buttons.transform.SetParent(systemScreen.transform);
                RectTransform ButtonsRect = Buttons.AddComponent<RectTransform>();
                AddRectComp(ButtonsRect, Vector2.one, Vector2.one, new Vector2(-620, -100), new Vector2(950, 75));
                HorizontalLayoutGroup buttonsLayoutGroup = Buttons.AddComponent<HorizontalLayoutGroup>();
                buttonsLayoutGroup.childAlignment = TextAnchor.UpperLeft;
                buttonsLayoutGroup.childControlWidth = false;
                buttonsLayoutGroup.childControlHeight = false;
                buttonsLayoutGroup.childForceExpandWidth = true;
                buttonsLayoutGroup.childForceExpandHeight = true;

                for (int i = 0; i < 5; i++)
                {
                    GameObject denom = new GameObject("Denom " + (i + 1));
                    denom.transform.SetParent(Buttons.transform);
                    RectTransform denomRect = denom.AddComponent<RectTransform>();
                    denomRect.sizeDelta = new Vector2(170, 80);
                    denom.AddComponent<CanvasRenderer>();
                    Image denomImage = denom.AddComponent<Image>();
                    Button denomButton = denom.AddComponent<Button>();
                    denomButton.targetGraphic = denomImage;

                    // Lambda expression requires to hard-code the values. So stupid, I know. But I tried everything I could think of.
                    if (i == 0)
                    {
                        denomButton.onClick.AddListener(() => PaytableButton(0));
                    }
                    else if (i == 1)
                    {
                        denomButton.onClick.AddListener(() => PaytableButton(1));
                    }
                    else if (i == 2)
                    {
                        denomButton.onClick.AddListener(() => PaytableButton(2));
                    }
                    else if (i == 3)
                    {
                        denomButton.onClick.AddListener(() => PaytableButton(3));
                    }
                    else if (i == 4)
                    {
                        denomButton.onClick.AddListener(() => PaytableButton(4));
                    } 

                    GameObject denomValue = new GameObject("Text");
                    denomValue.transform.SetParent(denom.transform);
                    RectTransform denomValueRect = denomValue.AddComponent<RectTransform>();
                    denomValueRect.anchorMin = Vector2.zero;
                    denomValueRect.anchorMax = Vector2.one;
                    denomValueRect.offsetMin = Vector2.zero;
                    denomValueRect.offsetMax = Vector2.zero;
                    denomValue.AddComponent<CanvasRenderer>();
                    Text denomValueText = denomValue.AddComponent<Text>();
                    AddTextComp(denomValueText, PopFont, 45, TextAnchor.MiddleCenter, Color.black, "$5.00");
                    Manager.MathModel.PTDenomButtonText.Add(denomValueRect);

                    GameObject Selected = new GameObject("Selected");
                    Selected.transform.SetParent(denom.transform);
                    RectTransform SelectedRect = Selected.AddComponent<RectTransform>();
                    AddRectComp(SelectedRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), Vector2.zero, new Vector2(200, 85));
                    Selected.AddComponent<CanvasRenderer>();
                    Image SelectedImage = Selected.AddComponent<Image>();
                    SelectedImage.color = Color.black;
                    if (i != 0)
                    {
                        Selected.SetActive(false);
                    }
                    paytableButtons.Add(Selected);

                    GameObject selectedText = new GameObject("White Text");
                    selectedText.transform.SetParent(Selected.transform);
                    RectTransform selectedTextRect = selectedText.AddComponent<RectTransform>();
                    AddRectComp(selectedTextRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), Vector2.zero, new Vector2(200, 85));
                    selectedText.AddComponent<CanvasRenderer>();
                    Text selectedTextComp = selectedText.AddComponent<Text>();
                    AddTextComp(selectedTextComp, PopFont, 45, TextAnchor.MiddleCenter, Color.white, "$5.00");
                    Manager.MathModel.PTDenomHighlightedButtonText.Add(selectedTextRect);
                }

                GameObject backButton = new GameObject("Back Button");
                backButton.transform.SetParent(systemScreen.transform);
                RectTransform backRect = backButton.AddComponent<RectTransform>();
                AddRectComp(backRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2 (0, -460), new Vector2(300, 120));
                backButton.AddComponent<CanvasRenderer>();
                Image backImage = backButton.AddComponent<Image>();
                GCGDKButton backButtonComp = backButton.AddComponent<GCGDKButton>();
                backButtonComp.targetGraphic = backImage;
                backButtonComp.thisButton = ButtonType.Cancel;

                GameObject backText = new GameObject("Back Text");
                backText.transform.SetParent(backButton.transform);
                RectTransform backTextRect = backText.AddComponent<RectTransform>();
                AddRectComp(backTextRect, Vector2.zero, Vector2.one, default, default);
                backTextRect.offsetMin = Vector2.zero;
                backTextRect.offsetMax = Vector2.zero;
                backText.AddComponent<CanvasRenderer>();
                Text backTextComp = backText.AddComponent<Text>();
                AddTextComp(backTextComp, PopFont, 65, TextAnchor.MiddleCenter, Color.black, "BACK");

                GameObject Caveat = new GameObject("Caveat");
                Caveat.transform.SetParent(systemScreen.transform);
                RectTransform CaveatRect = Caveat.AddComponent<RectTransform>();
                AddRectComp(CaveatRect, new Vector2 (1, 0), new Vector2(1, 0), new Vector2 (-360, 60), new Vector2(650, 80));
                Caveat.AddComponent<CanvasRenderer>();
                Text CaveatText = Caveat.AddComponent<Text>();
                AddTextComp(CaveatText, PopFont, 30, TextAnchor.MiddleRight, Color.white, "Mini-Games may give higher awards due to suboptimal play." +
                    "See 'Rules' for more details.");

                GameObject Progressive = new GameObject("Progressive");
                Progressive.transform.SetParent(systemScreen.transform);
                RectTransform ProgressiveRect = Progressive.AddComponent<RectTransform>();
                AddRectComp(ProgressiveRect, Vector2.zero, Vector2.zero, new Vector2(270, 70), new Vector2(500, 120));

                GameObject ProgressiveHeader = new GameObject("Header");
                ProgressiveHeader.transform.SetParent(Progressive.transform);
                RectTransform ProgHeaderRect = ProgressiveHeader.AddComponent<RectTransform>();
                AddRectComp(ProgHeaderRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, 50), new Vector2(500, 50));
                ProgressiveHeader.AddComponent<CanvasRenderer>();
                Text ProgHeaderText = ProgressiveHeader.AddComponent<Text>();
                AddTextComp(ProgHeaderText, PopFont, 35, TextAnchor.MiddleLeft, Color.white, "Progressive Parameters:");

                GameObject ProgressiveReset = new GameObject("Reset");
                ProgressiveReset.transform.SetParent(Progressive.transform);
                RectTransform ProgResetRect = ProgressiveReset.AddComponent<RectTransform>();
                AddRectComp(ProgResetRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, 15), new Vector2(500, 40));
                ProgressiveReset.AddComponent<CanvasRenderer>();
                Text ProgResetText = ProgressiveReset.AddComponent<Text>();
                AddTextComp(ProgResetText, PopFont, 30, TextAnchor.MiddleLeft, Color.white, "Reset: " + Manager.Currencify(250000));

                GameObject ProgIncrement = new GameObject("Increment");
                ProgIncrement.transform.SetParent(Progressive.transform);
                RectTransform ProgIncrementRect = ProgIncrement.AddComponent<RectTransform>();
                AddRectComp(ProgIncrementRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, -15), new Vector2(500, 40));
                ProgIncrement.AddComponent<CanvasRenderer>();
                Text ProgIncrementText = ProgIncrement.AddComponent<Text>();
                AddTextComp(ProgIncrementText, PopFont, 30, TextAnchor.MiddleLeft, Color.white, "Increment: " + Manager.Currencify(1));

                GameObject ProgAmount = new GameObject("Prog");
                ProgAmount.transform.SetParent(Progressive.transform);
                RectTransform ProgAmountRect = ProgAmount.AddComponent<RectTransform>();
                AddRectComp(ProgAmountRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, -45), new Vector2(500, 40));
                ProgAmount.AddComponent<CanvasRenderer>();
                Text ProgAmountText = ProgAmount.AddComponent<Text>();
                AddTextComp(ProgAmountText, PopFont, 30, TextAnchor.MiddleLeft, Color.white, "Actual Available Amount:");

                GameObject ProgValue = new GameObject("Prog Value");
                ProgValue.transform.SetParent(Progressive.transform);
                RectTransform ProgValueRect = ProgValue.AddComponent<RectTransform>();
                AddRectComp(ProgValueRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(330, -45), new Vector2(500, 40));
                ProgValue.AddComponent<CanvasRenderer>();
                Text ProgValueText = ProgValue.AddComponent<Text>();
                AddTextComp(ProgValueText, PopFont, 35, TextAnchor.MiddleLeft, Color.white, Manager.Currencify(Manager.Codespace.Link.progressiveSingle));
                /*GCGDKText ProgValueComp = ProgValue.AddComponent<GCGDKText>();
                ProgValueComp.content = DynamicTextCategory.MegaProgressiveAmount;
                Manager.DynamicText.populatedTextList.Clear();
                Manager.DynamicText.populatedTextList.Add(ProgValueComp);
                Manager.Codespace.MegaValueChanged();
                Manager.Codespace.SingleProgressiveValueChanged(); */

                PaytableButton(Manager.ButtonManager.activeDenom);
                Manager.MathModel.CreatePaytable();
            } else if (subStateName == "Menu")
            {
                Text systemHeaderText = systemHeader.AddComponent<Text>();
                AddTextComp(systemHeaderText, PopFont, 80, TextAnchor.MiddleCenter, Color.white, "Menu");

                GameObject backButton = new GameObject("Back Button");
                backButton.transform.SetParent(systemScreen.transform);
                RectTransform backRect = backButton.AddComponent<RectTransform>();
                AddRectComp(backRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, -475), new Vector2(250, 100));
                backButton.AddComponent<CanvasRenderer>();
                Image backImage = backButton.AddComponent<Image>();
                GCGDKButton backButtonComp = backButton.AddComponent<GCGDKButton>();
                backButtonComp.targetGraphic = backImage;
                backButtonComp.thisButton = ButtonType.Cancel;

                GameObject backText = new GameObject("Back Text");
                backText.transform.SetParent(backButton.transform);
                RectTransform backTextRect = backText.AddComponent<RectTransform>();
                AddRectComp(backTextRect, Vector2.zero, Vector2.one, default, default);
                backTextRect.offsetMin = Vector2.zero;
                backTextRect.offsetMax = Vector2.zero;
                backText.AddComponent<CanvasRenderer>();
                Text backTextComp = backText.AddComponent<Text>();
                AddTextComp(backTextComp, PopFont, 50, TextAnchor.MiddleCenter, Color.black, "BACK");

                GameObject cashoutButton = new GameObject("Cashout");
                cashoutButton.transform.SetParent(systemScreen.transform);
                RectTransform cashoutRect = cashoutButton.AddComponent<RectTransform>();
                AddRectComp(cashoutRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-420, 100), new Vector2(400, 160));
                cashoutButton.AddComponent<CanvasRenderer>();
                Image cashoutImage = cashoutButton.AddComponent<Image>();
                GCGDKButton cashoutButtonComp = cashoutButton.AddComponent<GCGDKButton>();
                cashoutButtonComp.targetGraphic = cashoutImage;
                cashoutButtonComp.thisButton = ButtonType.Cashout;

                GameObject cashoutText = new GameObject("Cashout Text");
                cashoutText.transform.SetParent(cashoutButton.transform);
                RectTransform cashoutTextRect = cashoutText.AddComponent<RectTransform>();
                AddRectComp(cashoutTextRect, Vector2.zero, Vector2.one, default, default);
                cashoutTextRect.offsetMin = Vector2.zero;
                cashoutTextRect.offsetMax = Vector2.zero;
                cashoutText.AddComponent<CanvasRenderer>();
                Text cashoutTextComp = cashoutText.AddComponent<Text>();
                AddTextComp(cashoutTextComp, PopFont, 65, TextAnchor.MiddleCenter, Color.black, "CASHOUT");

                GameObject rulesButton = new GameObject("Rules");
                rulesButton.transform.SetParent(systemScreen.transform);
                RectTransform rulesRect = rulesButton.AddComponent<RectTransform>();
                AddRectComp(rulesRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, 100), new Vector2(400, 160));
                rulesButton.AddComponent<CanvasRenderer>();
                Image rulesImage = rulesButton.AddComponent<Image>();
                GCGDKButton rulesButtonComp = rulesButton.AddComponent<GCGDKButton>();
                rulesButtonComp.targetGraphic = rulesImage;
                rulesButtonComp.thisButton = ButtonType.Help;

                GameObject rulesText = new GameObject("Rules Text");
                rulesText.transform.SetParent(rulesButton.transform);
                RectTransform rulesTextRect = rulesText.AddComponent<RectTransform>();
                AddRectComp(rulesTextRect, Vector2.zero, Vector2.one, default, default);
                rulesTextRect.offsetMin = Vector2.zero;
                rulesTextRect.offsetMax = Vector2.zero;
                rulesText.AddComponent<CanvasRenderer>();
                Text rulesTextComp = rulesText.AddComponent<Text>();
                AddTextComp(rulesTextComp, PopFont, 65, TextAnchor.MiddleCenter, Color.black, "RULES");

                GameObject paytableButton = new GameObject("Paytable");
                paytableButton.transform.SetParent(systemScreen.transform);
                RectTransform paytableButtonRect = paytableButton.AddComponent<RectTransform>();
                AddRectComp(paytableButtonRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(420, 100), new Vector2(400, 160));
                paytableButton.AddComponent<CanvasRenderer>();
                Image paytableButtonImage = paytableButton.AddComponent<Image>();
                GCGDKButton paytableButtonComp = paytableButton.AddComponent<GCGDKButton>();
                paytableButtonComp.targetGraphic = paytableButtonImage;
                paytableButtonComp.thisButton = ButtonType.Utility4;

                GameObject paytableText = new GameObject("Paytable Text");
                paytableText.transform.SetParent(paytableButton.transform);
                RectTransform paytableTextRect = paytableText.AddComponent<RectTransform>();
                AddRectComp(paytableTextRect, Vector2.zero, Vector2.one, default, default);
                paytableTextRect.offsetMin = Vector2.zero;
                paytableTextRect.offsetMax = Vector2.zero;
                paytableText.AddComponent<CanvasRenderer>();
                Text paytableTextComp = paytableText.AddComponent<Text>();
                AddTextComp(paytableTextComp, PopFont, 65, TextAnchor.MiddleCenter, Color.black, "PAYTABLE");

                if (Manager.isMultiGame)
                {
                    GameObject multigameButton = new GameObject("Multigame Button");
                    multigameButton.transform.SetParent(systemScreen.transform);
                    RectTransform multigameButtonRect = multigameButton.AddComponent<RectTransform>();
                    AddRectComp(multigameButtonRect, Vector2.zero, Vector2.zero, new Vector2(180, 200), new Vector2(250, 150));
                    multigameButton.AddComponent<CanvasRenderer>();
                    Image multigameButtonImage = multigameButton.AddComponent<Image>();
                    GCGDKButton multigameButtonComp = multigameButton.AddComponent<GCGDKButton>();
                    multigameButtonComp.targetGraphic = multigameButtonImage;
                    multigameButtonComp.thisButton = ButtonType.MultiGameExit;

                    GameObject multigameText = new GameObject("Multigame Text");
                    multigameText.transform.SetParent(multigameButton.transform);
                    RectTransform multigameTextRect = multigameText.AddComponent<RectTransform>();
                    AddRectComp(multigameTextRect, Vector2.zero, Vector2.one, default, default);
                    multigameTextRect.offsetMin = Vector2.zero;
                    multigameTextRect.offsetMax = Vector2.zero;
                    multigameText.AddComponent<CanvasRenderer>();
                    Text multigameTextComp = multigameText.AddComponent<Text>();
                    AddTextComp(multigameTextComp, PopFont, 50, TextAnchor.MiddleCenter, Color.black, "MORE GAMES");
                }

            } else if (subStateName == "Help")
            {
                Text systemHeaderText = systemHeader.AddComponent<Text>();
                AddTextComp(systemHeaderText, PopFont, 80, TextAnchor.MiddleCenter, Color.white, "Help");

                Manager.HelpScreen = systemScreen.GetComponent<Image>();
                if (Manager.HelpSprites.Length > 0)
                {
                    Destroy(systemHeader);
                    systemScreen.GetComponent<Image>().color = Color.white;
                    systemScreen.GetComponent<Image>().sprite = Manager.HelpSprites[0];
                }

                GameObject backButton = new GameObject("Back Button");
                backButton.transform.SetParent(systemScreen.transform);
                RectTransform backRect = backButton.AddComponent<RectTransform>();
                AddRectComp(backRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0, -475), new Vector2(250, 100));
                backButton.AddComponent<CanvasRenderer>();
                Image backImage = backButton.AddComponent<Image>();
                GCGDKButton backButtonComp = backButton.AddComponent<GCGDKButton>();
                backButtonComp.targetGraphic = backImage;
                backButtonComp.thisButton = ButtonType.HelpHome;

                GameObject backText = new GameObject("Back Text");
                backText.transform.SetParent(backButton.transform);
                RectTransform backTextRect = backText.AddComponent<RectTransform>();
                AddRectComp(backTextRect, Vector2.zero, Vector2.one, default, default);
                backTextRect.offsetMin = Vector2.zero;
                backTextRect.offsetMax = Vector2.zero;
                backText.AddComponent<CanvasRenderer>();
                Text backTextComp = backText.AddComponent<Text>();
                AddTextComp(backTextComp, PopFont, 50, TextAnchor.MiddleCenter, Color.black, "BACK");

                GameObject leftButton = new GameObject("Left");
                leftButton.transform.SetParent(systemScreen.transform);
                RectTransform leftRect = leftButton.AddComponent<RectTransform>();
                AddRectComp(leftRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(-200, -475), new Vector2(100, 100));
                leftButton.AddComponent<CanvasRenderer>();
                Image leftImage = leftButton.AddComponent<Image>();
                GCGDKButton leftButtonComp = leftButton.AddComponent<GCGDKButton>();
                leftButtonComp.targetGraphic = leftImage;
                leftButtonComp.thisButton = ButtonType.HelpLeft;

                GameObject rightButton = new GameObject("Right");
                rightButton.transform.SetParent(systemScreen.transform);
                RectTransform rightRect = rightButton.AddComponent<RectTransform>();
                AddRectComp(rightRect, new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(200, -475), new Vector2(100, 100));
                rightButton.AddComponent<CanvasRenderer>();
                Image rightImage = rightButton.AddComponent<Image>();
                GCGDKButton rightButtonComp = rightButton.AddComponent<GCGDKButton>();
                rightButtonComp.targetGraphic = rightImage;
                rightButtonComp.thisButton = ButtonType.HelpRight;
            }

            //Manager.DynamicText.ResetAllDynamicText();
            Manager.ButtonManager.UpdateAllButtons();
        }

        internal void PaytableButton(int denomInt)
        {
            for (int i = 0; i < 5; i++)
            {
                if (denomInt == i)
                {
                    paytableDenomValues[i].SetActive(true);
                    paytableButtons[i].SetActive(true);
                } else
                {
                    paytableDenomValues[i].SetActive(false);
                    paytableButtons[i].SetActive(false);
                }
            }
        }

        private void AddRectComp(RectTransform rectTransform, Vector2 anchorMin, Vector2 anchorMax, Vector2 anchoredPos, Vector2 sizeDelta)
        {
            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;
            rectTransform.anchoredPosition = anchoredPos;
            if (sizeDelta != default) { rectTransform.sizeDelta = sizeDelta; }
        }

        private void AddTextComp(Text textComp, Font font = null, int fontSize = 70, TextAnchor alignment = TextAnchor.MiddleCenter, Color color = default, string text = "")
        {
            textComp.font = font;
            textComp.fontSize = fontSize;
            textComp.alignment = alignment;
            if (color == default) { color = Color.white; }
            textComp.color = color;
            textComp.text = text;
        }
    }
}
