﻿using System.Collections;
using System.Collections.Generic;
//using System;
using UnityEngine;
using GCCL;
using UnityEngine.UI;
using TMPro;
using GCGDK;

namespace GCGDK
{
    [DisallowMultipleComponent]
    public class PlayLoop : MonoBehaviour
    {
        [HideInInspector]
        public Manager Manager;

        internal Coroutine readyCo;
        internal Coroutine startCo;
        internal Coroutine resultCo;
        internal Coroutine minigameCo;
        internal Coroutine bonusCo;
        //internal Coroutine playCo;
        //internal Coroutine tutorialCo;
        internal Coroutine underfundedCo;
        //internal Coroutine characterSelectCo;

        internal bool forfeit;
        internal bool tutorialSkipped;
        internal bool gameInProgress;
        internal bool isUnderfunded;
        internal bool newGame;
        internal bool underfundedCountdownInProgress;
        internal bool buttonPressed;
        internal bool[] buttonsPressed;

        private State menuEnterState;
        private string menuEnterSubstate;

        bool allWin, instantWin;

        int emulateCardPick;
        int[] emulateBigBonusPick;
        bool emulate;
        bool boolPicked;
        internal int currentPick;
        private bool CanProceedToNewRoundBool;

        private bool bigBonusesActive;
        private int[][] bigBonuses;
        private int[] bigBonusPick;

        // Accessible Bonus Values - Only for user to pull
        [HideInInspector]
        internal int TotalBonusWin;
        [HideInInspector]
        public int BonusRoundsCount = 0;
        [HideInInspector]
        public List<int> BonusValues = new List<int>();

        // Accessible Minigame Values - Only for user to pull
        [HideInInspector]
        public int TotalMinigameWin = 0;
        [HideInInspector]
        public int MinigameRoundsCount = 0;
        [HideInInspector]
        public List<int> MinigameValues = new List<int>();

        private bool InstantBool = false;      // Private Bool for testing - makes it loop instantly without input, set true or false from Manager at Awake
        private bool ComputingBool;   // This is very important. This assures that the user can't call anything until the current task is finished.

        // Pick-3 Game Specific List
        internal List<DynamicTextCategory> Pick3List = new List<DynamicTextCategory>() { DynamicTextCategory.Pick3Result0, DynamicTextCategory.Pick3Result1, DynamicTextCategory.Pick3Result2 };
        [HideInInspector]
        public string Pick3MinigameString, Pick3MinigameMissedString, Pick3BonusString, Pick3BonusMissedString, Pick3EmptyWinString, Pick3EmptyWinMissedString;

        private void Awake()
        {
            Manager = GetComponent<Manager>();

            InstantBool = false;
#if UNITY_EDITOR
            InstantBool = Manager.InstantBool;
#endif
        }

        // Use this for initialization
        void Start()
        {
            // Useful event handlers
            Manager.ButtonManager.ButtonPressEvent += new ButtonPressEventHandler(ScreenButtonPressed);
            Manager.ButtonManager.BetChangeEvent += new BetChangeEventHandler(BetChanged);
            Manager.InputManager.ButtonEvent += new ControllerButtonEventHandler(ControllerButtonPressed);
            Manager.InputManager.MoveEvent += new ControllerMoveEventHandler(Move);
            Manager.Codespace.BalanceChangeEvent += new CodespaceBalanceEventHandler(BalanceChange);
            Manager.StateManager.StateEvent += new StateChangeEventHandler(StateChange);
            Manager.TokenManager.TokenSpendEvent += new TokenSpendEventHandler(TokenSpend);

            Init();
        }

        /// <summary>
        /// Used to initialize - replacement for (and called by) Start, which can't be overridden (I think?)
        /// </summary>
        internal void Init()
        {
            Manager.TokenManager.tokensSpent = 0;

            QualitySettings.vSyncCount = 0;  // VSync must be disabled or disable in quality manually 
            Application.targetFrameRate = 60;
            //Screen.SetResolution(1920, 1080, true, 60);

            buttonsPressed = new bool[3];

            Manager.GameParamsLink.isGameConnected = true;
            Manager.GameParamsLink.state = GCCL.ConnectedGameState.Waiting;
            Manager.loaded = true;

            newGame = true;

            Manager.ButtonManager.activeDenom = 2;
            Manager.ButtonManager.CheckBetViable();

            Manager.ButtonManager.UpdateBetDenomButtons();
        }


        /// <summary>
        /// Money in, prepare to play.
        /// </summary>
        internal void StartSession()
        {
            Manager.StateManager.ChangeState(State.InGame);

            if (newGame)
            {
                newGame = false;
                Manager.ButtonManager.activeDenom = 2;
                Manager.ButtonManager.CheckBetViable();
            }
        }


        /// <summary>
        /// Play begins - tutorial, character select, or straight into game?
        /// </summary>
        internal void HitPlay()
        {
            if (Manager.GameParamsLink.firstPlay)
            {
                Manager.GameParamsLink.firstPlay = false;
                //tutorial = StartCoroutine(DoTutorial(true));
            }

            if (readyCo != null)
            {
                StopCoroutine(readyCo);
                gameInProgress = false;
                Dbg.Trace("ReadyGame Stopped");
            }

            TotalMinigameWin = 0;
            MinigameRoundsCount = 0;
            MinigameValues.Clear();
            TotalBonusWin = 0;
            BonusRoundsCount = 0;
            BonusValues.Clear();
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, "");
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MinigameResult, "");
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.BonusResult, "");
            Manager.ButtonManager.SetButtonTypeInteractable(ButtonType.Utility5, true);

            /*if (Manager.MathType == MathType.Gdk2)
            {
                StartCoroutine(ReadyGDK());
            } else
            {
                readyCo = StartCoroutine(ReadyGame());
            } */
            readyCo = StartCoroutine(ReadyGame());
        }

        internal IEnumerator ReadyGame()
        {
            ComputingBool = true;
            Manager.UserPrompt = "-Readying Game-"; Manager.UserPromptDescription = "Checking that CS is in State 4, the bet is viable, and the user isn't underfunded.";

            TotalMinigameWin = 0;
            MinigameRoundsCount = 0;
            MinigameValues.Clear();
            TotalBonusWin = 0;
            BonusRoundsCount = 0;
            BonusValues.Clear();
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.BonusResult, "");
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MinigameResult, "");
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, "");
            for (int i = 0; i < Pick3List.Count; i++)
            {
                Manager.DynamicText.UpdateDynamicText(Pick3List[i], "");
                // also description text?
            }

            Manager.StateManager.ChangeState(State.InGame, "Play");

            if (gameInProgress) { yield break; }
            gameInProgress = true;

            yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState == 4);
            Manager.CodespaceStateInt = Manager.Codespace.Link.codespaceState; // Only for the Editor

            Manager.ButtonManager.SetButtonTypeInteractable(ButtonType.Utility5, true);

            Dbg.Trace("Starting Play Round");

            if (newGame)
            {
            }
            newGame = false;

            Manager.ButtonManager.CheckBetViable();
            Manager.ButtonManager.UpdateBetChangeButtons();
            if (Manager.Codespace.Link.totalValue < Manager.MathModel.denoms[0])
            {
                Dbg.Trace("Underfunded!");
                underfundedCo = StartCoroutine(Underfunded());
                gameInProgress = false;
                yield break;
            }

            if (Manager.StateManager.State != State.InGame) { yield break; }

            ResetButtonPress();

            boolPicked = false;
            ComputingBool = false;
            Manager.UserPrompt = ">Make a pick!"; Manager.UserPromptDescription = "Game is ready. Call 'LeftDown', 'MiddleDown' or 'RightDown' from the Manager to make a bet.";
        }

        /*private IEnumerator ReadyGDK()
        {
            ComputingBool = true;
            Manager.UserPrompt = "-Readying Gdk-"; Manager.UserPromptDescription = "Checking that CS is in State 4, the bet is viable, and the user isn't underfunded.";

            TotalBonusWin = 0;
            BonusRoundsCount = 0;
            BonusValues.Clear();
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.BonusResult, "");
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MinigameResult, "");
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, "");

            Manager.ButtonManager.SetButtonTypeInteractable(ButtonType.Utility5, false);
            Manager.StateManager.ChangeState(State.InGame, "Play");

            if (gameInProgress) { yield break; }
            gameInProgress = true;

            yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState == 4);
            Manager.CodespaceStateInt = Manager.Codespace.Link.codespaceState; // Only for the Editor

            Manager.ButtonManager.SetButtonTypeInteractable(ButtonType.Utility5, true);

            Dbg.Trace("Starting Play Round");

            if (newGame)
            {
            }
            newGame = false;

            //if (Manager.StateManager.State != State.InGame) { yield break; }

            ResetButtonPress();

            boolPicked = false;
            ComputingBool = false;
            Manager.UserPrompt = ">GDK Action!"; Manager.UserPromptDescription = "GDK Game is waiting for an Action Call.";
        }

        public void GDKAction()
        {
            if (Manager.Codespace.Link.codespaceState == 4 && boolPicked == false)
            {
                Manager.Codespace.Link.PostTilesCommand("NON_SKILL TOKENS GEM_ANY");
                StartCoroutine(GdkMathLoop());
            }
        }
        internal IEnumerator GdkMathLoop()
        {
            ComputingBool = true;
            boolPicked = true;
            Manager.UserPrompt = "-Starting GDK Round-"; Manager.UserPromptDescription = "Action was called.";

            yield return new WaitForSeconds(0.5f);

            if (Manager.Codespace.Link.currentBet != 100)
            {
                Dbg.Trace("Bet set to: " + Manager.Codespace.Link.currentBet);
                Manager.Codespace.Link.PostSetBetCommand(100);
                yield return new WaitUntil(() => Manager.Codespace.Link.currentBet == 100);
            }

            Manager.Codespace.Link.ssPickValuesLoaded = false;
            Manager.Codespace.Link.bonus = 0;
            Manager.Codespace.playCountUpdated = false;

            // if method C, then PostMatchCommand(1-5) with performance tier. 5 is a total miss, 3 is ok, 1 is the best
            //Manager.Codespace.Link.PostMatchCommand(1);
            //yield return new WaitForSeconds(1);

            // the bet amount must be equal to TotalWager

            Debug.Log("gdktest. before postplaygamecommand, the cs state is " + Manager.Codespace.Link.codespaceState);
            //Manager.Codespace.Link.PostPlayGameCommand(100);
            Debug.Log("gdktest. currentbet is " + (uint)Manager.Codespace.Link.currentBet);
            Manager.Codespace.Link.PostPlayGameCommand((uint)Manager.Codespace.Link.currentBet);
            Debug.Log("gdktest. postplaygamecommand is called");
            yield return new WaitUntil(() => Manager.Codespace.playCountUpdated);

            // state is 6, now parse all the parameters.

            Debug.Log("gdktest. after postplaygamecommand, the cs state is " + Manager.Codespace.Link.codespaceState);
            int ssPickType = Manager.Codespace.Link.SSPickTypes[0];
            int ssPickValue = Manager.Codespace.Link.SSPickValues[0];
            Debug.Log("gdktest. sspicktypes[0] is " + Manager.Codespace.Link.SSPickTypes[0]);
            Debug.Log("gdktest. sspickvalues[0] is " + Manager.Codespace.Link.SSPickValues[0]);

            // do snapshots if you'd like after the result is revealed

            Manager.Codespace.Link.PostDisplayCompleteCommand(); // advance to state 7

            if (ssPickType == 0)
            {
                // no such thing as picktype equaling 0 apparently

            } else if (ssPickType == 1)
            {
                // action stage ??
                Manager.Codespace.Link.PostSendRewardCommand(0);

                Manager.UserPrompt = "-Parsing Bonus Values NOT-"; Manager.UserPromptDescription = "";
                //Debug.Log("gdktest. pickType was 1. this is before SSMaxSelectable");
                //yield return new WaitUntil(() => Manager.Codespace.Link.SSMaxSelectable > 1);
                Debug.Log("gdktest. pickType was 1. current balance is " + Manager.Codespace.Link.totalValue);

                Manager.Codespace.Link.PostSendRewardCommand((uint)288); // N = 256 * 1 + 32 = 288 is the special number to collect all bonus rewards
                yield return new WaitForSeconds(1);
                Debug.Log("gdktest. pickType was 1. for loop is completed. bonus should have been awarded. balance is " + Manager.Codespace.Link.totalValue);
            }
            else if (ssPickType == 255)
            {
                // this means No Bonus

            }

            Debug.Log("gdktest. before postsendrewardcommand for progressive. cs state is  " + Manager.Codespace.Link.codespaceState);
            Manager.Codespace.Link.PostSendRewardCommand(0); // this is to evaluate the progressive??
            yield return new WaitForSeconds(1);
            Debug.Log("gdktest. AFTER postsendrewardcommand for progressive. cs state is  " + Manager.Codespace.Link.codespaceState);

            Manager.Codespace.Link.PostDisplayCompleteCommand(); // advance back to state 4

            yield return new WaitForSeconds(1);
            Debug.Log("gdktest. AFTER last displaycomplete call. cs state is  " + Manager.Codespace.Link.codespaceState);

            yield break;
        } */

        void MadeAPick(int Pick)
        {
            if (Manager.Codespace.Link.codespaceState != 4 || ComputingBool == true)
            {
                string exceptionString = System.Reflection.MethodBase.GetCurrentMethod().Name + " was called incorrectly.";
                if (Manager.Codespace.Link.codespaceState != 4)
                {
                    exceptionString += "CS State was " + Manager.Codespace.Link.codespaceState + ". It should be 4.";
                }
                if (ComputingBool)
                {
                    exceptionString += " + ComputingBool was true. This means that another operation was underway.";
                }
                Dbg.Trace(exceptionString);
                throw new System.Exception(exceptionString);
                //Debug.LogError(exceptionString); return;
            }
            ComputingBool = true;

            currentPick = Pick;
            boolPicked = true;

            if (startCo != null)
            {
                StopCoroutine(startCo);
                Dbg.Trace("StartRound Stopped");
            }

            ComputingBool = false;
            startCo = StartCoroutine(StartRound());
        }

        private IEnumerator StartRound()
        {
            ComputingBool = true;
            boolPicked = true;
            Manager.UserPrompt = "-Starting Round-"; Manager.UserPromptDescription = "A pick was made. Awaiting results from Codespace.";

            if (emulate && emulateCardPick != 99)
            {
                currentPick = emulateCardPick;
            }

            Manager.ButtonManager.SetButtonTypeInteractable(ButtonType.Utility5, false);

            if (Manager.Codespace.Link.currentBet != Manager.MathModel.denoms[Manager.ButtonManager.activeDenom])
            {
                Dbg.Trace("Bet set to: " + Manager.Codespace.Link.currentBet);
                Manager.Codespace.Link.PostSetBetCommand(Manager.MathModel.denoms[Manager.ButtonManager.activeDenom]);
                yield return new WaitUntil(() => Manager.Codespace.Link.currentBet == Manager.MathModel.denoms[Manager.ButtonManager.activeDenom]);
            }

            Manager.Codespace.Link.ssPickValuesLoaded = false;
            Manager.Codespace.Link.bonus = 0;
            Manager.Codespace.playCountUpdated = false;

            while (!Manager.Codespace.playCountUpdated)
            {
                Manager.Codespace.Link.PostPlayGameCommand();
                //yield return new WaitUntil(() => Manager.Codespace.playCountUpdated);
                yield return null;
                Manager.Codespace.playCountUpdated = Manager.Codespace.Link.codespaceState == 6;
                if (!Manager.Codespace.playCountUpdated)
                {
                    yield return new WaitForSeconds(0.1f);
                }
            }

            //yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState == 6);

            Manager.ButtonManager.UpdateBetChangeButtons();

            yield return new WaitUntil(() => Manager.Codespace.Link.ssPickValuesLoaded);

            // Check for Progressive
            if (Manager.Codespace.Link.bonus > 0)
            {
                Dbg.Trace("Progressive Bonus: " + Manager.Codespace.Link.bonus);
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.ProgressiveBonusWonAmount, "");

                yield return new WaitForSeconds(1);

                Manager.DynamicText.InterruptTextBang(DynamicTextCategory.ProgressiveBonusWonAmount);
                Manager.DynamicText.InterruptTextBang(DynamicTextCategory.AchievedPrize);

                yield return new WaitForSeconds(1);
            }

            ComputingBool = false;
            Manager.UserPrompt = ">RevealUnselectedResults, >RevealSelectedResult, >SnapshotToProceedToState7"; Manager.UserPromptDescription = "Results have been parsed successfully. You may call these 3 methods from the PlayLoop. You can't reveal the results once CS proceeds to State 7.";
            if (InstantBool && Manager.Codespace.Link.codespaceState == 6)
            {
                RevealUnselectedResults("MINIGAME MISSED", "BONUS MISSED", "");
            }
        }

        public void RevealUnselectedResults(string minigameMissString, string bonusMissString, string noWinMissString)
        {
            if (!Manager.Codespace.Link.ssPickValuesLoaded || Manager.Codespace.Link.codespaceState != 6 || ComputingBool == true)
            {
                string exceptionString = System.Reflection.MethodBase.GetCurrentMethod().Name + " was called incorrectly.";
                if (Manager.Codespace.Link.codespaceState != 6)
                {
                    exceptionString += "CS State was " + Manager.Codespace.Link.codespaceState + ". It should be 6.";
                }
                if (ComputingBool)
                {
                    exceptionString += " + ComputingBool was true. This means that another operation was underway.";
                }
                Dbg.Trace(exceptionString);
                throw new System.Exception(exceptionString);
                //Debug.LogError(exceptionString); return;
            }
            ComputingBool = true;

            for (int i = 0; i < Pick3List.Count; i++)
            {
                if (currentPick != i)
                {
                    if (Manager.Codespace.Link.SSPickTypes[i] == 1)
                    {
                        Manager.DynamicText.UpdateDynamicText(Pick3List[i], Manager.Codespace.Link.SSPickValues[i], 0);
                    }
                    else if (Manager.Codespace.Link.SSPickTypes[i] == 2)
                    {
                        Manager.DynamicText.UpdateDynamicText(Pick3List[i], Manager.Codespace.Link.SSPickValues[i] + " TOKEN" + (Manager.Codespace.Link.SSPickValues[i] == 1 ? "" : "S"));
                    }
                    else if (Manager.Codespace.Link.SSPickTypes[i] == 3)
                    {
                        Manager.DynamicText.UpdateDynamicText(Pick3List[i], minigameMissString);
                    }
                    else if (Manager.Codespace.Link.SSPickTypes[i] == 4)
                    {
                        Manager.DynamicText.UpdateDynamicText(Pick3List[i], bonusMissString);
                    }
                    else if (Manager.Codespace.Link.SSPickTypes[i] == 5)
                    {
                        Manager.DynamicText.UpdateDynamicText(Pick3List[i], noWinMissString);
                    }
                }
            }

            ComputingBool = false;
            if (InstantBool && Manager.Codespace.Link.codespaceState == 6)
            {
                RevealSelectedResult("MINIGAME!", "BONUS!", "TRY AGAIN", 1, 2, 3, 4, 5, 1, 0);
            }
        }

        public void RevealSelectedResult(string minigameString, string bonusString, string noWinString, float smallCashBangTime, float mediumCashBangTime, float bigCashBangTime, float hugeCashBangTime, float massiveCashBangTime, float tokenBangTime, float noWinBangTime)
        {
            if (!Manager.Codespace.Link.ssPickValuesLoaded || Manager.Codespace.Link.codespaceState != 6 || ComputingBool == true)
            {
                string exceptionString = "RevealSelectedResult was called incorrectly.";
                if (Manager.Codespace.Link.codespaceState != 6)
                {
                    exceptionString += "CS State was " + Manager.Codespace.Link.codespaceState + ". It should be 6.";
                }
                if (ComputingBool)
                {
                    exceptionString += " + ComputingBool was true. This means that another operation was underway.";
                }
                Dbg.Trace(exceptionString);
                throw new System.Exception(exceptionString);
                //Debug.LogError(exceptionString); yield break;
            }

            if (resultCo != null)
            {
                StopCoroutine(resultCo);
                Dbg.Trace("ResultCo Stopped");
            }
            resultCo = StartCoroutine(RevealSelectedResultCo(minigameString, bonusString, noWinString, smallCashBangTime, mediumCashBangTime, bigCashBangTime, hugeCashBangTime, massiveCashBangTime, tokenBangTime, noWinBangTime));
        }

        private IEnumerator RevealSelectedResultCo(string minigameString, string bonusString, string noWinString, float smallCashBangTime, float mediumCashBangTime, float bigCashBangTime, float hugeCashBangTime, float massiveCashBangTime, float tokenBangTime, float noWinBangTime)
        {
            ComputingBool = true;
            Manager.UserPrompt = "-Revealing Selected Result-"; Manager.UserPromptDescription = "If testing with InstantLoop, the bang time will be determined from the Inspector.";

            float currentBangTime = 0;
            if (Manager.Codespace.Link.SSPickTypes[currentPick] == 1)
            {
                float winMultiplier = Manager.Codespace.Link.SSPickValues[currentPick] / Manager.MathModel.denoms[Manager.ButtonManager.activeDenom];
                if (winMultiplier < 1)
                {
                    currentBangTime = smallCashBangTime;
                    Manager.LEDs.ChangeSequence(LEDSequence.SMALL_WIN);
                }
                else if (winMultiplier < 10)
                {
                    currentBangTime = mediumCashBangTime;
                    Manager.LEDs.ChangeSequence(LEDSequence.MEDIUM_WIN);
                }
                else if (winMultiplier < 20)
                {
                    currentBangTime = bigCashBangTime;
                    Manager.LEDs.ChangeSequence(LEDSequence.BIG_WIN);
                }
                else if (winMultiplier < 30)
                {
                    currentBangTime = hugeCashBangTime;
                    Manager.LEDs.ChangeSequence(LEDSequence.BIG_WIN);
                }
                else
                {
                    currentBangTime = massiveCashBangTime;
                    Manager.LEDs.ChangeSequence(LEDSequence.BIG_WIN);
                }
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 2)
            {
                currentBangTime = tokenBangTime;
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 5)
            {
                currentBangTime = noWinBangTime;
            }
            else
            {
                currentBangTime = 0;
            }

            string targetString = "";
            if (Manager.Codespace.Link.SSPickTypes[currentPick] == 1)
            {
                Manager.DynamicText.UpdateDynamicText(Pick3List[currentPick], Manager.Codespace.Link.SSPickValues[currentPick], currentBangTime, bangSFX: Manager.DefaultCashBangSFX);
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, Manager.Codespace.Link.SSPickValues[currentPick], currentBangTime);
                targetString = Manager.Currencify(Manager.Codespace.Link.SSPickValues[currentPick]);
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 2)
            {
                Manager.DynamicText.UpdateDynamicText(Pick3List[currentPick], Manager.Codespace.Link.SSPickValues[currentPick], currentBangTime, false, postText: " TOKEN" + (Manager.Codespace.Link.SSPickValues[currentPick] == 1 ? "" : "S"), bangSFX: Manager.DefaultTokenBangSFX);
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, Manager.Codespace.Link.SSPickValues[currentPick], currentBangTime, false, postText: " TOKEN" + (Manager.Codespace.Link.SSPickValues[currentPick] == 1 ? "" : "S"));
                targetString = Manager.Codespace.Link.SSPickValues[currentPick] + " TOKEN" + (Manager.Codespace.Link.SSPickValues[currentPick] == 1 ? "" : "S");
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 3)
            {
                Manager.DynamicText.UpdateDynamicText(Pick3List[currentPick], minigameString);
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 4)
            {
                Manager.DynamicText.UpdateDynamicText(Pick3List[currentPick], bonusString);
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 5)
            {
                Manager.DynamicText.UpdateDynamicText(Pick3List[currentPick], noWinString);
            }

            ResetButtonPress();
            float timer0 = 0;
            while (timer0 < currentBangTime && !buttonPressed)
            {
                yield return null;
                timer0 += Time.deltaTime;
            }
            if (buttonPressed)
            {
                Manager.DynamicText.InterruptTextBang(Pick3List[currentPick]);
                Manager.DynamicText.UpdateDynamicText(Pick3List[currentPick], targetString);
                Manager.DynamicText.InterruptTextBang(DynamicTextCategory.AchievedPrize);
                if (Manager.Codespace.Link.SSPickTypes[currentPick] == 1 || Manager.Codespace.Link.SSPickTypes[currentPick] == 2)
                {
                    Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, targetString);
                }
            }

            ComputingBool = false;
            Manager.UserPrompt = ">RevealUnselectedResults, >RevealSelectedResult, >SnapshotToProceedToState7"; Manager.UserPromptDescription = "Results have been parsed successfully. You may call these 3 methods from the PlayLoop. You can't reveal the results once CS proceeds to State 7.";
            if (InstantBool && Manager.Codespace.Link.codespaceState == 6)
            {
                ProceedToState7();
            }
        }

        public void ProceedToState7()
        {
            if (Manager.Codespace.Link.codespaceState != 6 || Manager.Codespace.Link.ssPickValuesLoaded == false || ComputingBool == true)
            {
                string exceptionString = System.Reflection.MethodBase.GetCurrentMethod().Name + " was called incorrectly.";
                if (Manager.Codespace.Link.codespaceState != 6)
                {
                    exceptionString += "CS State was " + Manager.Codespace.Link.codespaceState + ". It should be 6.";
                }
                if (ComputingBool)
                {
                    exceptionString += " + ComputingBool was true. This means that another operation was underway.";
                }
                Dbg.Trace(exceptionString);
                throw new System.Exception(exceptionString);
                //Debug.LogError(exceptionString); return;
            }
            ComputingBool = true;
            Manager.Codespace.Link.PostDisplayCompleteCommand();
            ComputingBool = false;

            if (InstantBool)
            {
                StartCoroutine(InstantPostSendReward());
            } else
            {
                Manager.UserPrompt = ">PostSendRewardInState7"; Manager.UserPromptDescription = "Snapshot taken. Now send the reward to the user and, if they got a Minigame or a Bonus Round; parse those values.";
            }
        }

        private IEnumerator InstantPostSendReward()
        {
            Manager.UserPrompt = "-Instant PostSendReward-"; Manager.UserPromptDescription = "Waiting to reach CS state 7. This takes a few frames.";
            ComputingBool = true;
            yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState == 7);
            ComputingBool = false;
            PostSendRewardInState7();
        }

        public void PostSendRewardInState7()
        {
            if (Manager.Codespace.Link.codespaceState != 7)
            {
                return;
            }

            if (Manager.Codespace.Link.ssPickValuesLoaded == false || ComputingBool == true)
            {
                string exceptionString = System.Reflection.MethodBase.GetCurrentMethod().Name + " was called incorrectly.";
                if (Manager.Codespace.Link.ssPickValuesLoaded == false)
                {
                    exceptionString += " Pick Values were not loaded.";
                }
                if (ComputingBool)
                {
                    exceptionString += " + ComputingBool was true. This means that another operation was underway.";
                }
                Dbg.Trace(exceptionString);
                throw new System.Exception(exceptionString);
                //Debug.LogError(exceptionString); return;
            }
            ComputingBool = true;
            string snapShotString = "";
            if (Manager.Codespace.Link.SSPickTypes[currentPick] == 1)
            {
                snapShotString = "Cash Win: " + Manager.Currencify(Manager.Codespace.Link.SSPickValues[currentPick]);
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 2)
            {
                snapShotString = "Token Win: " + Manager.Codespace.Link.SSPickValues[currentPick];
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 3)
            {
                snapShotString = "Minigame Entry";
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 4)
            {
                snapShotString = "Bonus Entry";
            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 5)
            {
                snapShotString = "No Win";
            }
            Manager.Codespace.Link.PostSnapShotCommand(false, snapShotString);

            Manager.Codespace.Link.ssPickValuesLoaded = false;
            Manager.Codespace.Link.PostSendRewardCommand((uint)currentPick);

            ComputingBool = false;
            StartCoroutine(ParseMinigameAndBonus());
        }

        private IEnumerator ParseMinigameAndBonus()
        {
            ComputingBool = true;

            if (Manager.Codespace.Link.SSPickTypes[currentPick] == 3)
            {
                Manager.UserPrompt = "-Parsing Minigame Values-"; Manager.UserPromptDescription = "";
                yield return new WaitUntil(() => Manager.Codespace.Link.SSMaxSelectable > 1);

                MinigameRoundsCount = Manager.Codespace.Link.SSMaxSelectable;
                TotalMinigameWin = 0;
                MinigameValues.Clear();
                for (int i = 0; i < Manager.Codespace.Link.SSMaxSelectable; i++)
                {
                    TotalMinigameWin += Manager.Codespace.Link.SSPickValues[i + 3];
                    MinigameValues.Add(Manager.Codespace.Link.SSPickValues[i + 3]);
                }

                string MinigameReport = "MinigameRoundsCount: " + MinigameRoundsCount + "; MinigameValues: { ";
                for (int i = 0; i < MinigameValues.Count; i++)
                {
                    MinigameReport += MinigameValues[i];
                    if (i != MinigameValues.Count - 1)
                    {
                        MinigameReport += ", ";
                    }
                }
                MinigameReport += " }; TotalMinigameWin: " + TotalMinigameWin;
                Dbg.Trace(MinigameReport);

                ComputingBool = false;
                Manager.UserPrompt = ">RewardMinigame"; Manager.UserPromptDescription = "Parsed successfully. You may access these variables before rewarding the win: " + MinigameReport;
                if (InstantBool)
                {
                    RewardMinigame(8);
                }

            }
            else if (Manager.Codespace.Link.SSPickTypes[currentPick] == 4)
            {
                Manager.UserPrompt = "-Parsing Bonus Values-"; Manager.UserPromptDescription = "";
                yield return new WaitUntil(() => Manager.Codespace.Link.SSMaxSelectable > 1);

                ParseMathResults();

                while (bigBonuses == null)
                {
                    Debug.Log("Waiting for Bonus Parse");
                    ParseMathResults();
                    yield return null;
                }

                TotalBonusWin = 0;
                BonusRoundsCount = bigBonuses.Length;
                BonusValues.Clear();
                for (int i = 0; i < bigBonuses.Length; i++)
                {
                    int prizeLevel = emulate ? emulateBigBonusPick[i] : bigBonusPick[i];
                    int prize = bigBonuses[i][prizeLevel];
                    TotalBonusWin += prize;
                    BonusValues.Add(prize);
                }

                string BonusReport = "BonusRoundsCount: " + BonusRoundsCount + "; BonusValues: { ";
                for (int i = 0; i < BonusValues.Count; i++)
                {
                    BonusReport += BonusValues[i];
                    if (i != BonusValues.Count - 1)
                    {
                        BonusReport += ", ";
                    }
                }
                BonusReport += " }; TotalBonusWin: " + TotalBonusWin;
                Dbg.Trace(BonusReport);

                ComputingBool = false;
                Manager.UserPrompt = ">RewardBonus"; Manager.UserPromptDescription = "Parsed successfully. You may access these variables before rewarding the win: " + BonusReport;
                if (InstantBool)
                {
                    RewardBonus(10);
                }

            }
            else
            {
                CanProceedToNewRoundBool = true;
                ComputingBool = false;
                Manager.UserPrompt = ">FinishHand"; Manager.UserPromptDescription = "Call FinishHand to reset Codespace to State 4 and restart the round.";
                Debug.Log("waiting for finishhand, state is " + Manager.Codespace.Link.codespaceState);
                if (InstantBool)
                {
                    FinishHand();
                }
            }
        }

        public void RewardMinigame(float minigameBangTime)
        {
            if (Manager.Codespace.Link.codespaceState != 7 || Manager.Codespace.Link.SSPickTypes[currentPick] != 3 || Manager.Codespace.Link.SSMaxSelectable < 1 || ComputingBool == true)
            {
                string exceptionString = System.Reflection.MethodBase.GetCurrentMethod().Name + " was called incorrectly.";
                if (Manager.Codespace.Link.codespaceState != 7)
                {
                    exceptionString += " CS State was " + Manager.Codespace.Link.codespaceState + ". It should be 7.";
                } else if (Manager.Codespace.Link.SSPickTypes != null)
                {
                    if (Manager.Codespace.Link.SSPickTypes[currentPick] != 3)
                    {
                        exceptionString += " Picked Type was not 'Minigame' (3).";
                    }
                }
                if (ComputingBool)
                {
                    exceptionString += " + ComputingBool was true. This means that another operation was underway.";
                }
                Dbg.Trace(exceptionString);
                throw new System.Exception(exceptionString);
                //Debug.LogError(exceptionString); return;
            }
            ComputingBool = true;

            if (minigameCo != null)
            {
                StopCoroutine(minigameCo);
                Dbg.Trace("Minigame Stopped");
            }
            ComputingBool = false;
            minigameCo = StartCoroutine(DoMinigame(minigameBangTime));
        }

        private IEnumerator DoMinigame(float minigameBangTime)
        {
            ComputingBool = true;

            string targetString = "";

            Manager.DynamicText.UpdateDynamicText(Pick3List[currentPick], "");
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MinigameResult, TotalMinigameWin, minigameBangTime);
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, TotalMinigameWin, minigameBangTime);
            targetString = Manager.Currencify(TotalMinigameWin);

            ResetButtonPress();
            float timer = 0;
            while (timer < minigameBangTime && !buttonPressed)
            {
                yield return null;
                timer += Time.deltaTime;
            }
            if (buttonPressed || minigameBangTime == 0)
            {
                Manager.DynamicText.InterruptTextBang(DynamicTextCategory.MinigameResult);
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.MinigameResult, targetString);
                Manager.DynamicText.InterruptTextBang(DynamicTextCategory.AchievedPrize);
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, targetString);
            }
            yield return null;
            Manager.Codespace.Link.PostSnapShotCommand(false, "Minigame Win: " + targetString);

            for (int i = 0; i < Manager.Codespace.Link.SSMaxSelectable; i++)
            {
                Manager.Codespace.Link.PostSendRewardCommand((uint)1);
                yield return null;
            }

            CanProceedToNewRoundBool = true;
            ComputingBool = false;
            Manager.UserPrompt = ">FinishHand"; Manager.UserPromptDescription = "Call FinishHand to reset Codespace to State 4 and restart the round.";
            if (InstantBool && Manager.Codespace.Link.codespaceState == 7)
            {
                FinishHand();
            }
        }

        public void RewardBonus(float bonusBangTime)
        {
            if (Manager.Codespace.Link.codespaceState != 7 || Manager.Codespace.Link.SSPickTypes[currentPick] != 4 || bigBonuses == null || ComputingBool == true)
            {
                string exceptionString = System.Reflection.MethodBase.GetCurrentMethod().Name + " was called incorrectly.";
                if (Manager.Codespace.Link.codespaceState != 7)
                {
                    exceptionString += " CS State was " + Manager.Codespace.Link.codespaceState + ". It should be 7.";
                }
                else if (Manager.Codespace.Link.SSPickTypes != null)
                {
                    if (Manager.Codespace.Link.SSPickTypes[currentPick] != 4)
                    {
                        exceptionString += " Picked Type was not 'Bonus' (4).";
                    }
                }
                if (ComputingBool)
                {
                    exceptionString += " + ComputingBool was true. This means that another operation was underway.";
                }
                Dbg.Trace(exceptionString);
                throw new System.Exception(exceptionString);
                //Debug.LogError(exceptionString); return;
            }
            ComputingBool = true;

            if (bonusCo != null)
            {
                StopCoroutine(bonusCo);
                Dbg.Trace("Bonus Stopped");
            }
            ComputingBool = false;
            bonusCo = StartCoroutine(DoBonus(bonusBangTime));
        }

        private IEnumerator DoBonus(float bonusBangTime)
        {
            ComputingBool = true;

            string targetString = "";

            Manager.DynamicText.UpdateDynamicText(Pick3List[currentPick], "");
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.BonusResult, TotalBonusWin, bonusBangTime);
            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, TotalBonusWin, bonusBangTime);
            targetString = Manager.Currencify(TotalBonusWin);

            ResetButtonPress();
            float timer = 0;
            while (timer < bonusBangTime && !buttonPressed)
            {
                yield return null;
                timer += Time.deltaTime;
            }
            if (buttonPressed)
            {
                Manager.DynamicText.InterruptTextBang(DynamicTextCategory.BonusResult);
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.BonusResult, targetString);
                Manager.DynamicText.InterruptTextBang(DynamicTextCategory.AchievedPrize);
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, targetString);
            }
            yield return null;
            Manager.Codespace.Link.PostSnapShotCommand(false, "Bonus Win: " + targetString);

            for (int i = 0; i < bigBonuses.Length; i++)
            {
                int prizeLevel = emulate ? emulateBigBonusPick[i] : bigBonusPick[i];

                Manager.Codespace.Link.PostSendRewardCommand((uint)(prizeLevel + 11));
                //yield return new WaitUntil(() => Manager.Codespace.Link.SSPhase == i + 2 || Manager.Codespace.Link.SSPhase == bigBonuses.Length);
                yield return null;
            }

            CanProceedToNewRoundBool = true;
            ComputingBool = false;
            Manager.UserPrompt = ">FinishHand"; Manager.UserPromptDescription = "Call FinishHand to reset Codespace to State 4 and restart the round.";
            if (InstantBool && Manager.Codespace.Link.codespaceState == 7)
            {
                FinishHand();
            }
        }

        public void FinishHand()
        {
            if (CanProceedToNewRoundBool == false || ComputingBool == true)
            {
                string exceptionString = System.Reflection.MethodBase.GetCurrentMethod().Name + " was called incorrectly.";
                if (CanProceedToNewRoundBool == false)
                {
                    exceptionString += "CanProceedToNewRoundBool is false. Have you rewarded this round correctly?";
                }
                if (ComputingBool)
                {
                    exceptionString += " + ComputingBool was true. This means that another operation was underway.";
                }
                Dbg.Trace(exceptionString);
                throw new System.Exception(exceptionString);
                //Debug.LogError(exceptionString); return;
            } 
            StartCoroutine(FinishCo());
        }

        private IEnumerator FinishCo()
        {
            ComputingBool = true;
            Manager.UserPrompt = "-Restarting Round-"; Manager.UserPromptDescription = "FinishHand called successfully. Round is complete!";

            Manager.Codespace.Link.progressiveTier = 0;
            //Manager.Codespace.Link.PostSnapShotCommand();
            Manager.Codespace.Link.PostDisplayCompleteCommand();
            CanProceedToNewRoundBool = false;
            yield return new WaitForSeconds(0.25f);

            Manager.Emulation.DeleteFile();
            emulate = false;
            gameInProgress = false;

            if (readyCo != null)
            {
                StopCoroutine(readyCo);
                Dbg.Trace("ReadyGame Stopped");
            }

            ComputingBool = false;
            readyCo = StartCoroutine(ReadyGame());

            yield break;
        }

        /// <summary>
        /// Stop everything and bring to a state where it can be easily restarted.
        /// </summary>
        internal virtual void InterruptPlay()
        {
            Manager.GameParamsLink.StopGame();

            try
            {
                StopCoroutine(startCo);
            }
            catch (System.Exception)
            {
            }

            try
            {
                StopCoroutine(readyCo);
            }
            catch (System.Exception)
            {
            }

            try
            {
                StopCoroutine(resultCo);
            }
            catch (System.Exception)
            {
            }

            try
            {
                StopCoroutine(minigameCo);
            }
            catch (System.Exception)
            {
            }

            try
            {
                StopCoroutine(bonusCo);
            }
            catch (System.Exception)
            {
            }

            try
            {
                StopCoroutine(underfundedCo);
                underfundedCountdownInProgress = false;
            }
            catch (System.Exception)
            {
            }

            Manager.Codespace.Link.PostGameConnectedCommand();

            gameInProgress = false;
        }

        internal void Emulation(int cardPick, int[] emulatedBonusPicks)
        {
            Dbg.Trace("EMULATING");

            emulateCardPick = cardPick;
            emulateBigBonusPick = emulatedBonusPicks;
            emulate = true;
        }

        internal void Recovery()
        {
            /*if (Manager.MathType == MathType.Gc8002)
            {
                StartCoroutine(Recover8002FromInterruption());
            } else if (Manager.MathType == MathType.Gdk2)
            {

            } */
            StartCoroutine(Recover8002FromInterruption());
        }

        private IEnumerator Recover8002FromInterruption()
        {
            ComputingBool = true;
            Manager.UserPrompt = "-Recovering-"; Manager.UserPromptDescription = "Something went wrong. Trying to recover to State 4.";

            yield return new WaitForSeconds(1);

            Manager.Codespace.Link.bonus = 0;

            Manager.Codespace.updateBalancesAutomatically = true;

            while (Manager.Codespace.Link.codespaceState != 4)
            {
                yield return new WaitForSeconds(0.2f);

                if (Manager.Codespace.Link.codespaceState == 5)
                {
                    Manager.Codespace.Link.PostPlayGameCommand((uint)Manager.Codespace.Link.currentBet);

                    yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState != 5);
                }

                if (Manager.Codespace.Link.codespaceState == 6)
                {
                    //Manager.Codespace.Link.PostSnapShotCommand();
                    Manager.Codespace.Link.PostDisplayCompleteCommand();
                    yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState != 6);
                }

                if (Manager.Codespace.Link.codespaceState == 7)
                {
                    bigBonusesActive = false;
                    ParseMathResults();

                    if (Manager.Codespace.Link.SSMaxSelectable > 1 || bigBonusesActive)
                    {
                        int startingShot = Manager.Codespace.Link.SSPhase - 1;

                        if (bigBonusesActive)
                        {
                            for (int i = startingShot; i < bigBonuses.Length; i++)
                            {
                                int prizeLevel = emulate ? emulateBigBonusPick[i] : bigBonusPick[i];
                                int prize = bigBonuses[i][prizeLevel];

                                Dbg.Trace("Choosing reward " + prize + " for dunk " + Manager.Codespace.Link.SSPhase);

                                Manager.Codespace.Link.PostSendRewardCommand((uint)(prize + 11));
                                yield return new WaitUntil(() => Manager.Codespace.Link.SSPhase == i + 2 || Manager.Codespace.Link.SSPhase == bigBonuses.Length);
                            }
                        }
                        else
                        {
                            for (int i = startingShot; i < Manager.Codespace.Link.SSMaxSelectable; i++)
                            {
                                Dbg.Trace("Sending reward for Shot " + Manager.Codespace.Link.SSPhase);
                                Manager.Codespace.Link.PostSendRewardCommand((uint)1);
                                yield return new WaitUntil(() => Manager.Codespace.Link.SSPhase == i + 2 || Manager.Codespace.Link.SSPhase == Manager.Codespace.Link.SSMaxSelectable);
                            }
                        }
                    }
                    else
                    {
                        int pick = 0;

                        for (int a = 0; a < 3; a++)
                        {
                            if (Manager.Codespace.Link.SSPickTypes[a] == 1)
                            {
                                pick = a;
                            }
                        }

                        Manager.Codespace.Link.PostSendRewardCommand((uint)pick);

                    }

                    yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState != 7);

                }

                if (Manager.Codespace.Link.codespaceState == 8)
                {
                    //Manager.Codespace.Link.PostSnapShotCommand();
                    Manager.Codespace.Link.PostDisplayCompleteCommand();
                    yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState != 8);
                }
            }

            Manager.TokenManager.CodespaceTokenTotalChanged();

            if (Manager.StateManager.currentSubstate != "Play" && Manager.StateManager.currentSubstate != "Menu")
            {
                Manager.StateManager.ChangeState(State.InGame, "Attract");
            }

            Manager.Emulation.DeleteFile();

            emulate = false;

            ComputingBool = false;

            yield return null;
        }

        
        /// <summary>
        /// Optional - do the character select sequence.
        /// </summary>
        /// <returns></returns>
        internal IEnumerator DoCharacterSelect()
        {
            yield break;
        }

        /// <summary>
        /// Run the tutorial sequence.
        /// </summary>
        /// <param name="LeadIntoGame">When finished go straight into a new level.</param>
        /// <returns></returns>
        internal IEnumerator DoTutorial(bool LeadIntoGame)
        {
            yield break;
        }
        

        /// <summary>
        /// The player does not have enough cash to make the minimum bet. They may or may not have some cash, or the ability to spend tokens to win the miss pool.
        /// </summary>
        /// <returns></returns>
        internal IEnumerator Underfunded()
        {
            if (MachineClean() && Manager.Codespace.Link.totalValue == 0)
            {
                StartCoroutine(EndSession());
                yield break;
            }

            Manager.PlayLoop.isUnderfunded = true;

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.Timer, "");

            Manager.Cashout.DiscourageAbandonment();

            Manager.StateManager.ChangeState(State.System, "Underfunded");

            Manager.TokenManager.CheckTokenButtonsAvailable();

            if (Manager.Codespace.Link.totalValue == 0)
            {
                if (underfundedCountdownInProgress)
                {
                    yield break;
                }

                underfundedCountdownInProgress = true;

                int time = 60;

                while (time > 0 && Manager.Codespace.Link.totalValue < Manager.MathModel.denoms[0])
                {
                    if (Manager.Codespace.Link.totalValue == 0)
                    {
                        Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.Timer, time, 0, false);
                        yield return new WaitUntil(() => Manager.Codespace.Link.codespaceState == 4);
                        time--;
                    }
                    else
                    {
                        Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.Timer, "");
                    }

                    yield return new WaitForSeconds(1);
                }

                underfundedCountdownInProgress = false;

                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.Timer, "");

                if (Manager.Codespace.Link.totalValue == 0)
                {
                    StartCoroutine(EndSession());
                }
                else
                {
                    Manager.PlayLoop.isUnderfunded = false;
                    Manager.StateManager.ChangeState(State.InGame);
                }
            }
            else
            {
                yield break;
            }
        }

        /*internal void ChangeCharacter(int chosen)
        {
            return;
        } */

        /// <summary>
        /// Checks if there is any progress or tokens on the machine.
        /// </summary>
        /// <returns>True if there are tokensd or progress on the machine</returns>
        internal virtual bool MachineClean()
        {
            bool clean = Manager.Codespace.Link.progressiveTokens == 0 && Manager.Codespace.Link.progressivePool == 0 && Manager.Codespace.Link.currGameInRound == 0;
            Dbg.Trace("Machine Clean: " + clean);
            return clean;
        }

        /// <summary>
        /// Cashed out or run out of money, reset all variables and animations, clear tokens and progress.
        /// </summary>
        internal virtual IEnumerator EndSession()
        {
            Manager.Emulation.DeleteFile();

            Manager.Codespace.Link.PostResetMathCommand();

            yield return new WaitForSeconds(0.25f); // Need to wait for PostResetMathCommand to take effect and feed back updated params

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.CashBalance, 0, 0);

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.TokenBalance, 0, currencify: false);

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, "");

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AvailablePrize, "");

            Manager.GameParamsLink.SetFirstPlay(true);

            Manager.StateManager.ChangeState(State.InGame, "Attract");

            newGame = true;

            gameInProgress = false;
        }

        /// <summary>
        /// Set Reset trigger on all animations in toReset, ensuring interrupted animations don't leave any elements hanging around.
        /// </summary>
        /*public virtual void ResetAnimations()
        {
            foreach (Animator Animator in toReset)
            {
                Animator.SetTrigger("Reset");
            }
        } */

        /// <summary>
        /// Event handler called when a button has been pressed on the touchscreen, called after the main functionality in ButtonManager (if any) has been called.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        internal virtual void ScreenButtonPressed(object source, ScreenButtonArgs e)
        {
            switch (e.pressedButton)
            {
                case ButtonType.Play:
                    break;
                case ButtonType.Cashout:
                    break;
                case ButtonType.BetDenom:
                    break;
                case ButtonType.BetUp:
                    break;
                case ButtonType.BetDown:
                    break;
                case ButtonType.Volume:
                    break;
                case ButtonType.MaxBet:
                    break;
                case ButtonType.PlayTutorial:
                    break;
                case ButtonType.SkipTutorial:
                    break;
                case ButtonType.Help:
                    break;
                case ButtonType.HelpHome:
                    break;
                case ButtonType.HelpLeft:
                    break;
                case ButtonType.HelpRight:
                    break;
                case ButtonType.GenericHome:
                    break;
                case ButtonType.GenericLeft:
                    break;
                case ButtonType.GenericRight:
                    break;
                case ButtonType.Up:
                    break;
                case ButtonType.Down:
                    break;
                case ButtonType.Options:
                    break;
                case ButtonType.UseToken:
                    Manager.GameParamsLink.TokensUsed(e.tokenLevel, Manager.tokenLevelCost[e.tokenLevel - 1]);
                    break;
                case ButtonType.Confirm:
                    break;
                case ButtonType.Cancel:
                    if (Manager.StateManager.State != State.System || Manager.StateManager.currentSubstate == "Help" || Manager.StateManager.currentSubstate == "Paytable")
                    {
                        if (isUnderfunded)
                        {
                            underfundedCo = StartCoroutine(Underfunded());
                        }
                        else if (menuEnterState == State.InGame)
                        {
                            HitPlay();
                        }
                        else if (Manager.StateManager.currentSubstate == "Menu")
                        {
                            Manager.StateManager.ChangeState(menuEnterState, menuEnterSubstate);
                        }
                        else if (Manager.StateManager.currentSubstate != "Play")
                        {
                            if (Manager.Codespace.Link.totalValue > 0)
                            {
                                Manager.StateManager.ChangeState(State.InGame, "Play");
                                HitPlay();
                            } else
                            {
                                Manager.StateManager.ChangeState(State.InGame, "Attract");
                            }
                        }
                    }
                    break;
                case ButtonType.Forfeit:
                    break;
                case ButtonType.PayForBonusSpin:
                    break;
                case ButtonType.Utility2:
                case ButtonType.Utility1:
                    //nextButtonPressed = true;
                    break;
                case ButtonType.Utility3:
                    break;
                case ButtonType.Utility4:
                    if (Manager.IgnorePaytablePopulation)
                    {
                        Debug.Log("Paytable Population is ignored. Please toggle the option off on the Manager script if you wish the system to populate the Paytable.");
                    } else
                    {
                        Manager.StateManager.ChangeState(State.InGame, "Paytable");
                    }
                    break;
                case ButtonType.Utility5:
                    // Menu button
                    if (Manager.IgnoreMenuPopulation)
                    {
                        Debug.Log("Menu Population is ignored. Please toggle the option off on the Manager script if you wish the system to populate the Menu.");
                    } else
                    {
                        menuEnterState = Manager.StateManager.State;
                        menuEnterSubstate = Manager.StateManager.currentSubstate;
                        Manager.StateManager.ChangeState(State.InGame, "Menu");
                    }
                    break;
                case ButtonType.NULL:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// The bet amount has been changed.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        internal void BetChanged(object source, BetChangeArgs e)
        {
            //base.BetChanged(source, e);

            int[] newTokenCost = new int[4];

            switch (e.betLevel)
            {
                case 0:
                    newTokenCost = new int[] { 10, 15, 10, 10 };
                    break;
                case 1:
                    newTokenCost = new int[] { 20, 30, 10, 10 };
                    break;
                case 2:
                    newTokenCost = new int[] { 40, 60, 10, 10 };
                    break;
                case 3:
                    newTokenCost = new int[] { 60, 90, 10, 10 };
                    break;
                case 4:
                    newTokenCost = new int[] { 100, 150, 10, 10 };
                    break;
                default:
                    break;
            }

            Manager.TokenManager.ChangeTokenCost(newTokenCost);
        }

        /// <summary>
        /// The bet has been reduced because the player's balance dipped below the selected bet. Notify them!
        /// </summary>
        internal void BetReduced()
        {
            if (newGame) { return; }
        }

        /// <summary>
        /// Tokens have been spent
        /// </summary>
        internal void TokenSpend(object source, TokenSpendArgs e)
        {
            //base.TokenSpend(source, e);

            switch (e.tokenLevel)
            {
                case 1:
                case 2:
                    allWin = true;
                    break;
                default:
                    break;
            }

            instantWin = true;
        }

        /// <summary>
        /// Event handler for a controller joystick move. For the purposes of the GCUI we don't distinguish between joysticks and just send one event per move - not continuous. Fine for navigation.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        internal void Move(object source, MoveArgs e)
        {
            if (Manager.logControllerInputs)
                Dbg.Trace("Moved " + e.direction);
        }

        /// <summary>
        /// Handler for a change in the player's balance. e.newBalance is the new value, e.higher is a bool which is true if money has been added, false if money has been taken away.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        internal void BalanceChange(object source, BalanceArgs e)
        {
            Dbg.Trace("New balance: " + e.newBalance + ", " + (e.higher ? "money added" : "money lost"));

            if (e.higher && Manager.StateManager.currentSubstate == "CashOutConfirm")
            {
                Manager.StateManager.ChangeState(State.InGame);
            }

            if (isUnderfunded && Manager.Codespace.Link.totalValue >= Manager.MathModel.denoms[0])
            {
                isUnderfunded = false;

                Manager.StateManager.ChangeState(State.InGame);
            }
        }

        /// <summary>
        /// Handler for a state change. e.State is the new state.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        internal void StateChange(object source, StateArgs e)
        {
            switch (e.state)
            {
                case State.InGame:
                    if (Manager.StateManager.currentSubstate == "")
                    {
                        if (Manager.Codespace.Link.totalValue > 0)
                        {
                            Manager.StateManager.ChangeState(State.InGame, "Play");
                            HitPlay();
                        }
                        else
                        {
                            Manager.StateManager.ChangeState(State.InGame, "Attract");
                        } 
                        return;
                    }

                    Manager.ButtonManager.SetButtonTypeInteractable(ButtonType.Utility5, true);

                    if (Manager.Codespace.Link.totalValue < Manager.MathModel.denoms[0] && Manager.StateManager.currentSubstate == "Play")
                    {
                        if (underfundedCo != null)
                        {
                            StopCoroutine(underfundedCo);
                        }
                        underfundedCo = StartCoroutine(Underfunded());
                    }
                    else
                    {
                        if (Manager.StateManager.noCashTimeOut != null)
                        {
                            StopCoroutine(Manager.StateManager.noCashTimeOut);
                        }
                        if (underfundedCountdownInProgress || Manager.StateManager.timeoutCountdownActive || Manager.Codespace.Link.totalValue > 0)
                        {
                            break;
                        }
                        Debug.Log("NoCashTimeOut");
                        Manager.StateManager.noCashTimeOut = StartCoroutine(Manager.StateManager.NoCashTimeOut());
                    }
                    break;
                case State.System:
                    Manager.ButtonManager.SetButtonTypeInteractable(ButtonType.Utility5, false);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Triggered by updating MathParams in Codespace. This is very different for each math model, and makes sense it should be handled here.
        /// </summary>
        internal void ParseMathParams()
        {

        } 

        private void ParseMathResults()
        {
            TotalBonusWin = 0;
            foreach (string item in Manager.Codespace.Link.mathResults)
            {

                //Debug.Log("mathResults: " + item.ToString());
                string[] param = item.Split(':');

                if (param[0].Trim() == "Big Bonus")
                {
                    Dbg.Trace("Big Bonus Active");

                    bigBonusesActive = true;

                    string[] splitString = new string[1] { ",{" };

                    string[] bonuses = param[2].Split(splitString, System.StringSplitOptions.None);

                    int numBonuses = int.Parse(bonuses[0].Trim());

                    bigBonuses = new int[numBonuses][];
                    bigBonusPick = new int[numBonuses];

                    for (int i = 0; i < numBonuses; i++)
                    {
                        string[] bonusWins = bonuses[i + 1].Replace('}', char.MinValue).Split(',');

                        bigBonuses[i] = new int[3];
                        bigBonusPick[i] = int.Parse(bonusWins[3].Replace('"', char.MinValue)) - 1;

                        Dbg.Trace("Choice " + i);

                        for (int a = 0; a < 3; a++)
                        {
                            bigBonuses[i][a] = int.Parse(bonusWins[a].Replace('"', char.MinValue));
                            Dbg.Trace("Prize " + a + ": " + bigBonuses[i][a]);
                        }

                        Dbg.Trace("Pick " + bigBonusPick[i]);
                    }


                }
            }
        }

        internal void ResetButtonPress()
        {
            buttonPressed = false;

            //whichButtonPressed = 4;

            for (int i = 0; i < 3; i++)
            {
                buttonsPressed[i] = false;
            }
        }

        /*internal int WhichButtonPressed()
        {
            int num = 4;

            for (int i = 0; i < 3; i++)
            {
                if (buttonsPressed[i])
                    num = i;
            }

            return num;
        } */

        private void ControllerButtonPressed(object source, ControllerButtonArgs e)
        {
            //base.ControllerButtonPressed(source, e);

            switch (e.button)
            {
                case ControllerButtons.A:
                    if (e.buttonPressed)
                    {
                        LeftDown();
                    }
                    else
                    {
                        LeftUp();
                    }
                    break;
                case ControllerButtons.B:
                    if (e.buttonPressed)
                    {
                        MiddleDown();
                    }
                    else
                    {
                        MiddleUp();
                    }
                    break;
                case ControllerButtons.X:
                    if (e.buttonPressed)
                    {
                        RightDown();
                    }
                    else
                    {
                        RightUp();
                    }
                    break;
                default:
                    break;
            }
        }

        public void LeftDown()
        {
            buttonPressed = true;
            buttonsPressed[0] = true;
            if (boolPicked == false)
            {
                MadeAPick(0);
            }
        }

        public void LeftUp()
        {

        }

        public void MiddleDown()
        {
            buttonPressed = true;
            buttonsPressed[1] = true;
            if (boolPicked == false)
            {
                MadeAPick(1);
            }
        }

        public void MiddleUp()
        {

        }

        public void RightDown()
        {
            buttonPressed = true;
            buttonsPressed[2] = true;
            if (boolPicked == false)
            {
                MadeAPick(2);
            }
        }

        public void RightUp()
        {

        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                LeftDown();
            }

            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                LeftUp();
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                MiddleDown();
            }

            if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                MiddleUp();
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                RightDown();
            }

            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                RightUp();
            }
        }
    }
}
