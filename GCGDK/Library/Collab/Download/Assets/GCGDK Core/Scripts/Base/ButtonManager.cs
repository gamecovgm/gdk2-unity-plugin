﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using GCUI;

namespace GCUI
{
    public delegate void ButtonPressEventHandler(object source, ScreenButtonArgs e);

    public class ScreenButtonArgs : System.EventArgs
    {
        public ButtonType pressedButton;
        public int tokenLevel;
        public int denomLevel;
        public ScreenButtonArgs(ButtonType thisbutton, int token = 0, int denom = 0)
        {
            pressedButton = thisbutton;
            tokenLevel = token;
            denomLevel = denom;
        }
    }

    public delegate void BetChangeEventHandler(object source, BetChangeArgs e);

    public class BetChangeArgs : System.EventArgs
    {
        public int betLevel;
        public int betValue;
        public BetChangeArgs(int newBetLevel, int newBetValue)
        {
            betLevel = newBetLevel;
            betValue = newBetValue;
        }
    }

    public class ButtonManager : MonoBehaviour
    {

        [HideInInspector]
        public Manager Manager;
        [HideInInspector]
        public PlayLoop playLoop;

        public event ButtonPressEventHandler ButtonPressEvent;
        public event BetChangeEventHandler BetChangeEvent;

        internal List<GCGDKButton> AllButtonsList = new List<GCGDKButton>();

        internal int activeDenom;

        internal List<GCGDKButton> betButtons;
        private void Awake()
        {
            Manager = GetComponent<Manager>();
            playLoop = GetComponent<PlayLoop>();

            UpdateAllButtons();

            betButtons = GetButtonsOfType(ButtonType.BetDenom);
        }

        internal void UpdateAllButtons()
        {
            GCGDKButton[] AllButtons = Resources.FindObjectsOfTypeAll<GCGDKButton>();
            AllButtonsList.Clear();
            foreach (GCGDKButton button in AllButtons)
            {
                AllButtonsList.Add(button);
            }
            //Debug.Log("AllButtons count is " + NewButtonsList.Count);
        }

        /// <summary>
        /// Pressed button is interactable, perform appropriate task.
        /// </summary>
        /// <param name="buttonType">Pressed button</param>
        public virtual void DoButtonAction(GCGDKButton GCUIButton = null, ButtonType buttonType = ButtonType.NULL)
        {
            int tokenLevel = 0;
            int denomLevel = 0;
            int tokenCost = 0;

            if (GCUIButton != null)
            {
                buttonType = GCUIButton.thisButton;
                tokenLevel = GCUIButton.tokenLevel;
                denomLevel = GCUIButton.denomLevel;
                tokenCost = GCUIButton.tokenCost;
            }
            else if (buttonType == ButtonType.NULL)
            {
                return;
            }

            //Manager.AudioManager.PlaySFX(Manager.AudioManager.buttonActiveClick);

            switch (buttonType)
            {
                case ButtonType.Play:
                    Manager.PlayLoop.HitPlay();
                    break;
                case ButtonType.Cashout:
                    if (Manager.Codespace.Link.totalValue == 0)
                    {
                        break;
                    }

                    if (Manager.StateManager.currentSubstate == "CashOutConfirm")
                    {
                        StartCoroutine(Manager.Cashout.DoCashout());
                    }
                    else
                    {
                        Manager.Cashout.CashoutRequested();
                    }
                    break;
                case ButtonType.BetDenom:
                    activeDenom = GCUIButton.denomLevel;
                    CheckBetViable();
                    break;
                case ButtonType.BetUp:
                    activeDenom++;
                    CheckBetViable();
                    break;
                case ButtonType.BetDown:
                    activeDenom--;
                    CheckBetViable();
                    break;
                case ButtonType.Volume:
                    //Manager.AudioManager.IncrementVolume();
                    break;
                case ButtonType.MaxBet:
                    break;
                case ButtonType.PlayTutorial:
                    StartCoroutine(Manager.PlayLoop.DoTutorial(false));
                    break;
                case ButtonType.SkipTutorial:
                    Manager.GameParamsLink.SkipTutorial();
                    Manager.PlayLoop.tutorialSkipped = true;
                    break;
                case ButtonType.Help:
                    Manager.LaunchHelp();
                    break;
                case ButtonType.HelpHome:
                    Manager.Back();
                    break;
                case ButtonType.HelpLeft:
                    Manager.NavigateHelp(-1);
                    break;
                case ButtonType.HelpRight:
                    Manager.NavigateHelp(1);
                    break;
                case ButtonType.GenericHome:
                    break;
                case ButtonType.GenericLeft:
                    break;
                case ButtonType.GenericRight:
                    break;
                case ButtonType.Up:
                    break;
                case ButtonType.Down:
                    break;
                case ButtonType.Options:
                    break;
                case ButtonType.UseToken:
                    Manager.TokenManager.UseTokens(tokenLevel, tokenCost);
                    break; 
                case ButtonType.Confirm:
                    break;
                case ButtonType.Cancel:
                    if (Manager.StateManager.currentSubstate == "CashOutConfirm")
                    {
                        Manager.Cashout.CashoutCancel();
                    }
                    break;
                case ButtonType.Forfeit:
                    Manager.PlayLoop.forfeit = true;
                    break;
                case ButtonType.MultiGameExit:
                    Manager.MultiGame.BackToMain();
                    break;
                default:
                    break;
            }

            ButtonPressEvent?.Invoke(this, new ScreenButtonArgs(buttonType, tokenLevel, denomLevel));
        }

        /// <summary>
        /// Pressed button is not interactable, perform optional unavailable button task.
        /// </summary>
        /// <param name="buttonType">Pressed button</param>
        public virtual void DoButtonUnavailableAction(GCGDKButton GCUIButton)
        {
            switch (GCUIButton.thisButton)
            {
                case ButtonType.Play:
                    break;
                case ButtonType.Cashout:
                    break;
                case ButtonType.BetDenom:
                    break;
                case ButtonType.BetUp:
                    break;
                case ButtonType.BetDown:
                    break;
                case ButtonType.Volume:
                    break;
                case ButtonType.MaxBet:
                    break;
                case ButtonType.PlayTutorial:
                    break;
                case ButtonType.SkipTutorial:
                    break;
                case ButtonType.Help:
                    break;
                case ButtonType.HelpHome:
                    break;
                case ButtonType.HelpLeft:
                    break;
                case ButtonType.HelpRight:
                    break;
                case ButtonType.GenericHome:
                    break;
                case ButtonType.GenericLeft:
                    break;
                case ButtonType.GenericRight:
                    break;
                case ButtonType.Up:
                    break;
                case ButtonType.Down:
                    break;
                case ButtonType.Options:
                    break;
                case ButtonType.UseToken:
                    break;
                case ButtonType.Confirm:
                    break;
                case ButtonType.Cancel:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Enable or Disable all buttons of a given type. This sets their Interactable status, not whether they are active in the hierarchy.
        /// </summary>
        /// <param name="buttonType">Type of button</param>
        /// <param name="enable">Defaults to true for enable. False disables.</param>
        public virtual void SetButtonTypeInteractable(ButtonType buttonType, bool enable = true, int sub = 0)
        {
            int updateButtonsInt = 0;

            for (int i = 0; i < AllButtonsList.Count; i++)
            {
                if (AllButtonsList[i] != null)
                {
                    if (AllButtonsList[i].thisButton == buttonType)
                    {
                        if (buttonType == ButtonType.UseToken)
                        {
                            if (AllButtonsList[i].tokenLevel == sub || sub == 0)
                            {
                                AllButtonsList[i].GetComponent<Button>().interactable = enable;
                            }
                        }
                        else if (buttonType == ButtonType.BetDenom)
                        {
                            if (AllButtonsList[i].denomLevel == sub || sub == 0)
                            {
                                AllButtonsList[i].GetComponent<Button>().interactable = enable;
                            }
                        }
                        else
                        {
                            AllButtonsList[i].GetComponent<Button>().interactable = enable;
                        }
                    }
                } else
                {
                    updateButtonsInt += 1;
                }
            }

            if (updateButtonsInt > 0)
            {
                UpdateAllButtons();
            }
        }

        /// <summary>
        /// Turn on or off all buttons of a given type. This disables or enables them entirely.
        /// </summary>
        /// <param name="buttonType">Type of button</param>
        /// <param name="enable">Defaults to true for enable. False disables.</param>
        /// <param name="sub">If button is token or denom, specifies which level. 0 for all levels</param>
        public virtual void SetButtonTypeVisible(ButtonType buttonType, bool enable = true, int sub = 0, float duration = 99, bool gameObject = false)
        {
            int updateButtonsInt = 0;

            for (int i = 0; i < AllButtonsList.Count; i++)
            {
                if (AllButtonsList[i] != null)
                {
                    if (AllButtonsList[i].thisButton == buttonType)
                    {
                        if (buttonType == ButtonType.UseToken)
                        {
                            if (AllButtonsList[i].tokenLevel == sub || sub == 0)
                            {
                                if (gameObject)
                                {
                                    AllButtonsList[i].gameObject.SetActive(enable);
                                }
                                /*else
                                {
                                    AllButtons[i].GetComponent<GCUIObject>().SetActive(enable, duration);
                                } */

                                AllButtonsList[i].interactable = enable;
                            }
                        }
                        else if (buttonType == ButtonType.BetDenom)
                        {
                            if (AllButtonsList[i].denomLevel == sub || sub == 0)
                            {
                                if (gameObject)
                                {
                                    AllButtonsList[i].gameObject.SetActive(enable);
                                }
                                /*else
                                {
                                    AllButtons[i].GetComponent<GCUIObject>().SetActive(enable, duration);
                                } */

                                AllButtonsList[i].interactable = enable;
                            }
                        }
                        else
                        {
                            if (gameObject)
                            {
                                AllButtonsList[i].gameObject.SetActive(enable);
                            }
                            /*else
                            {
                                AllButtons[i].GetComponent<GCUIObject>().SetActive(enable, duration);
                            } */

                            AllButtonsList[i].interactable = enable;
                        }
                    }
                } else
                {
                    updateButtonsInt += 1;
                }
            }

            if (updateButtonsInt > 0)
            {
                UpdateAllButtons();
            }

            Dbg.Trace("Setting ButtonType " + buttonType.ToString() + " " + sub + " visible: " + enable);

        }


        /// <summary>
        /// Returns a List of all GCUIButtons of a given ButtonType. Yeah it's a List not an Array. What's your problem?
        /// </summary>
        /// <param name="buttonType"></param>
        /// <returns></returns>
        public virtual List<GCGDKButton> GetButtonsOfType(ButtonType buttonType)
        {
            List<GCGDKButton> toReturn = new List<GCGDKButton>();

            for (int i = 0; i < AllButtonsList.Count; i++)
            {
                if (AllButtonsList[i].thisButton == buttonType)
                    toReturn.Add(AllButtonsList[i].GetComponent<GCGDKButton>());
            }

            return toReturn;
        }

        /// <summary>
        /// Set the selected denom to its selected state.
        /// </summary>
        /// <param name="selected"></param>
        public virtual void SelectDenom(int selected)
        {
            foreach (GCGDKButton betButton in betButtons)
            {
                if (betButton.denomLevel != selected)
                    betButton.DeSelectDenom();
            }

            foreach (GCGDKButton betButton in betButtons)
            {
                if (betButton.denomLevel == selected)
                    betButton.SelectDenom();
            }
        }

        /// <summary>
        /// Set the interactibility of Bet Denom buttons based on current balance.
        /// </summary>
        public virtual void UpdateBetDenomButtons()
        {
            if (betButtons.Count == 0 || Manager.MathModel.denoms.Length == 0)
            {
                return;
            }

            for (int i = 0; i < betButtons.Count; i++)
            {
                betButtons[i].GetComponent<Button>().interactable = Manager.MathModel.denoms[betButtons[i].denomLevel] <= Manager.Codespace.Link.totalValue;
            }

            SetButtonTypeInteractable(ButtonType.Play, Manager.Codespace.Link.totalValue >= Manager.MathModel.denoms[0]);
            SetButtonTypeInteractable(ButtonType.Cashout, Manager.Codespace.Link.totalValue > 0);

            if (Manager.Codespace.Link.totalValue < Manager.MathModel.denoms[0])
            {
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.CurrentBetAmount, "");
                UpdateBetChangeButtons();
                return;
            }

            CheckBetViable();
        }

        /// <summary>
        /// Check to see if the selected bet is viable, and if not try and find one that is.
        /// </summary>
        public virtual void CheckBetViable()
        {
            bool reduce = false;

            if (!Manager.MathModel || Manager.MathModel.denoms.Length == 0)
            {
                return;
            }

            Dbg.Trace("Checking Bet");

            while (Manager.MathModel.denoms[activeDenom] > Manager.Codespace.Link.totalValue && activeDenom >= 0)
            {
                activeDenom--;

                if (activeDenom == -1)
                {
                    // Underfunded. BAIL!
                    activeDenom = 0;
                    return;
                }

                reduce = true;
            }

            for (int i = 0; i < betButtons.Count; i++)
            {
                if (betButtons[i].denomLevel == activeDenom)
                {
                    SelectDenom(activeDenom);
                }
            }

            Dbg.Trace("Active denom: " + activeDenom);

            Manager.Codespace.Link.PostSetBetCommand(Manager.MathModel.denoms[activeDenom]);

            UpdateBetChangeButtons();

            Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.CurrentBetAmount, Manager.Currencify(Manager.MathModel.denoms[activeDenom]));

            //Manager.Paytable.PaytableSelect(activeDenom + 1);

            if (reduce)
            {
                Manager.PlayLoop.BetReduced();
                Manager.DynamicText.UpdateDynamicText(DynamicTextCategory.AchievedPrize, "");
            }

            BetChangeEvent?.Invoke(this, new BetChangeArgs(activeDenom, Manager.MathModel.denoms[activeDenom]));
        }

        /// <summary>
        /// Check to see if the Bet Up and Bet Down buttons should be active or inactive based on current bet and available balance.
        /// </summary>
        public virtual void UpdateBetChangeButtons()
        {
            if (Manager.Codespace.Link.codespaceState != 4 || Manager.Codespace.Link.totalValue <= Manager.MathModel.denoms[0])
            {
                SetButtonTypeInteractable(ButtonType.BetUp, false);
                SetButtonTypeInteractable(ButtonType.BetDown, false);
                return;
            }

            SetButtonTypeInteractable(ButtonType.BetDown, activeDenom != 0);

            if (activeDenom < 4 && Manager.MathModel.denoms[activeDenom + 1] <= Manager.Codespace.Link.totalValue)
            {
                SetButtonTypeInteractable(ButtonType.BetUp, true);
            }
            else
            {
                SetButtonTypeInteractable(ButtonType.BetUp, false);
            }
        }
    }
}
