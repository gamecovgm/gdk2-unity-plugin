﻿namespace GCGDK
{
    public enum MathType // For testing purposes.
    {
        Gc8002,
        Gdk2
    }

    public enum Gdk2SkillModel
    {
        NON_SKILL, // Method A.  No “skill” action.
        POST_SKILL, // Method B.  Player performs an action to trigger a bet and then performs a second “skill” action to claim the reward.
        PRE_SKILL // Method C.  Player performs a “skill” action to trigger a bet.
    }

    public enum Gdk2TokenModel
    {
        TOKENS, // Game awards tokens.
        NO_TOKENS // Game does not award tokens.
    }

    public enum Gdk2GemPolicy
    {
        GEM_NONE, // Bets are not GEM-eligible.
        GEM_ANY, // Every bet is GEM-eligible.
        GEM_ZERO, // Bets with $0 reward are GEM-eligible.
        GEM_OPTIMAL // Bets with optimal play are GEM-eligible.
    }

    public enum State
    {
        InGame,
        System
    }

    public enum ButtonType
    {
        Play,
        Cashout,
        BetDenom,
        BetUp,
        BetDown,
        Volume,
        MaxBet,
        PlayTutorial,
        SkipTutorial,
        Help,
        HelpLeft,
        HelpRight,
        HelpHome,
        GenericLeft,
        GenericRight,
        GenericHome,
        Up,
        Down,
        Options,
        UseToken,
        Confirm,
        Cancel,
        Forfeit,
        PayForBonusSpin,
        Utility1,
        Utility2,
        Utility3,
        Utility4,
        Utility5,
        MultiGameExit,
        NULL
    }

    public enum GameDisplayMode
    {
        Transparency,
        Regions,
        Integrated
    }

    public enum ControllerButtons
    {
        Any,
        A,
        B,
        X,
        Y,
        LeftShoulder,
        RightShoulder,
        LeftStick,
        RightStick,
        Start,
        Guide,
        Back,
        LeftTrigger,
        RightTrigger
    }

    public enum Directions
    {
        Up,
        Down,
        Left,
        Right
    }

    public enum BangEnum
    {
        Disabled,
        General,
        Override
    }

    public enum DynamicTextCategory
    {
        None,
        ContextBased,
        MissPool,
        CashBalance,
        TokenBalance,
        MiniProgressiveAmount,
        MaxiProgressiveAmount,
        MegaProgressiveAmount,
        MiniProgressiveDateWon,
        MaxiProgressiveDateWon,
        MegaProgressiveDateWon,
        AchievedPrize,
        AvailablePrize,
        CashOdds,
        TokenOdds,
        CurrentBetAmount,
        ProgressiveBonusWonAmount,
        Timer,
        DiscourageAbandonment,
        NonCashableBalance,
        HandpayAmount,
        JackpotHandpayAmount,
        TiltHeader,
        TiltBody,
        CornerMessages,
        PointsAccumulated,
        PointsUntilBonus,
        CashoutAmount,
        TiltCashoutPrompt,
        // Added by Meric
        Pick3Result0,
        Pick3Result1,
        Pick3Result2,
        MinigameResult,
        BonusResult
    }

    public enum Culture
    {
        USA,
        UK,
        France,
        Netherlands,
        Chile,
        Spain
    }

    public enum CSStates
    {
        OutOfService = 0,
        DisabledByOnlineSystem, //1
        OnlineSystemDown,       //2
        Tilt,                   //3
        SetupBet,               //4
        StartPlay,              //5
        DisplayPending,         //6
        BonusActive,            //7
        BonusDisplayPending,    //8
        HelpScreen,             //9
        JackpotHandPay,         //10
        HandPay,                //11
        CashoutPending,         //12
        ReelStop,               //13
        AFTLock,                //14
        AttendantMenu,          //15
        DelayGame,              //16
        GambleOrTake,           //17
        Gamble,                 //18
        GambleDisplayPending,   //19
        StartFreeSpins,         //20
        GambleWin,              //21
        WaitGameReg,            //22
        AwardSASProgressive,    //23
        ProgressiveDisplayPending,//24
        OLSPlayLocked,          //25
        OLSTitleDisabled,       //26
        Menu,                   //27
        ProgressiveControllerDown,//28
        RecallReplay,           //29
        DisabledByTicketQueue,  //30
        TicketInEscrow,         //31
        PayCheckMode            //32
    }
}