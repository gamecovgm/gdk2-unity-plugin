﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GCUI;

[CustomEditor(typeof(StateManager))]
public class StateManagerEditor : Editor
{
    private StateManager StateManager;

    List<SerializedProperty> inGameSubstatesProp;
    List<SerializedProperty> systemSubstatesProp;

    private bool showInGame = false;
    private bool showSystem = false;

    private void OnEnable()
    {
        StateManager = (StateManager)target;

        PopulateSubstates();
    }

    void PopulateSubstates()
    {

        inGameSubstatesProp = new List<SerializedProperty>();
        for (int i = 0; i < StateManager.InGame.SubStates.Count; i++)
        {
            string propertyName = "InGame.SubStates";
            inGameSubstatesProp.Add(serializedObject.FindProperty(propertyName).GetArrayElementAtIndex(i).FindPropertyRelative("CanvasObjects"));
        }

        systemSubstatesProp = new List<SerializedProperty>();
        for (int i = 0; i < StateManager.System.SubStates.Count; i++)
        {
            string propertyName = "System.SubStates";
            systemSubstatesProp.Add(serializedObject.FindProperty(propertyName).GetArrayElementAtIndex(i).FindPropertyRelative("CanvasObjects"));
        }
    }

    void ShowState(StateObject thisState, List<SerializedProperty> subStatesProp)
    {
        EditorGUI.indentLevel += 1;

        for (int i = 0; i < thisState.SubStates.Count; i++)
        {
            /*if (i == 0 || (thisState == StateManager.System && i < 8))
            {
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(thisState.SubStates[i].Name, EditorStyles.boldLabel, GUILayout.Width(150));
                EditorGUILayout.LabelField("ID: " + thisState.SubStates[i].Id);
                GUILayout.EndHorizontal();
            } else if (i == 0 || (thisState == StateManager.InGame && i < 8))
            {
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(thisState.SubStates[i].Name, EditorStyles.boldLabel, GUILayout.Width(150));
                EditorGUILayout.LabelField("ID: " + thisState.SubStates[i].Id);
                GUILayout.EndHorizontal();
            }
            else
            {
                EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("ID: " + thisState.SubStates[i].Id, GUILayout.Width(55));
                thisState.SubStates[i].Name = EditorGUILayout.TextField(thisState.SubStates[i].Name);
                if (GUILayout.Button("Remove SubState"))
                {
                    RemoveSubState(thisState.SubStates, i);
                }
                GUILayout.EndHorizontal();
            } */
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("ID: " + thisState.SubStates[i].Id, GUILayout.Width(55));
            thisState.SubStates[i].Name = EditorGUILayout.TextField(thisState.SubStates[i].Name);
            if (GUILayout.Button("Remove SubState"))
            {
                RemoveSubState(thisState.SubStates, i);
            }
            GUILayout.EndHorizontal();

            EditorGUI.indentLevel += 2;
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(subStatesProp[i], new GUIContent("Drop CanvasGroup Objects Here"));
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Updated " + thisState.State);
                EditorUtility.SetDirty(target);
            }
            if (subStatesProp[i].isExpanded && thisState.SubStates.Count > i)
            {
                EditorGUI.indentLevel += 1;
                for (int a = 0; a < thisState.SubStates[i].CanvasObjects.Count; a++)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.ObjectField(thisState.SubStates[i].CanvasObjects[a], typeof(CanvasGroup), true);
                    if (GUILayout.Button("Remove"))
                    {
                        RemoveCanvasObject(thisState.SubStates[i].CanvasObjects, a);
                    }
                    GUILayout.EndHorizontal();
                }
                EditorGUI.indentLevel -= 1;
            }
            EditorGUI.indentLevel -= 2;
        }

        GUILayout.BeginHorizontal();
        GUILayout.Space(50);
        if (GUILayout.Button("Add SubState", GUILayout.Width(200)))
        {
            AddSubstate(thisState);
        }
        GUILayout.EndHorizontal();

        EditorGUI.indentLevel -= 1;
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        PopulateSubstates();

        serializedObject.Update();

        EditorGUILayout.LabelField("Current state: " + serializedObject.FindProperty("currentStateString").stringValue);

        //EditorGUILayout.HelpBox("NB: Canvas Objects in 'In-Game's Idle SubState will be visible in all 'In-Game' SubStates. Idle can be empty.", MessageType.Info);

        showInGame = EditorGUILayout.Foldout(showInGame, "IN-GAME");
        if (showInGame)
        {
            ShowState(StateManager.InGame, inGameSubstatesProp);
            EditorGUILayout.Space();
        }

        showSystem = EditorGUILayout.Foldout(showSystem, "SYSTEM");
        if (showSystem)
        {
            ShowState(StateManager.System, systemSubstatesProp);
        }

        serializedObject.ApplyModifiedProperties();
    }

    void RemoveCanvasObject(List<CanvasGroup> from, int index)
    {
        from.RemoveAt(index);
        PopulateSubstates();
        Repaint();
        Undo.RecordObject(target, "Updated " + from);
        EditorUtility.SetDirty(target);
    }

    void AddSubstate(StateObject toState)
    {
        int i = 0;

        foreach (SubStateObject item in toState.SubStates)
        {
            i = item.Id > i ? item.Id : i;
        }

        i++;

        toState.SubStates.Add(new SubStateObject("SubState " + i, i));
        Undo.RecordObject(target, "Updated " + toState.State);
        EditorUtility.SetDirty(target);
    }

    void RemoveSubState(List<SubStateObject> from, int index)
    {
        from.RemoveAt(index);
        PopulateSubstates();
        Repaint();
        Undo.RecordObject(target, "Updated " + from);
        EditorUtility.SetDirty(target);
    }
}
