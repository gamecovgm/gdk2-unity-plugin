﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GCUI;

namespace GCUI
{
    public delegate void StateChangeEventHandler(object source, StateArgs e);

    public class StateArgs : System.EventArgs
    {
        public State state;
        public StateArgs(State newState)
        {
            state = newState;
        }
    }

    [Serializable]
    public class SubStateObject
    {
        public string Name;
        public int Id;
        public List<GCUIObject> GCUIObjects;

        public SubStateObject(string name, int id)
        {
            Name = name;
            Id = id;
            GCUIObjects = new List<GCUIObject>();
        }

        public void SetActive(bool active, float fadeTime)
        {
            foreach (GCUIObject item in GCUIObjects)
            {
                if (item)
                {
                    if (!item.gameObject.activeInHierarchy)
                    {
                        item.gameObject.SetActive(true);
                    }
                    CanvasGroup itemCanvasGroup = null;
                    if (item.gameObject.GetComponent<CanvasGroup>())
                    {
                        itemCanvasGroup = item.gameObject.GetComponent<CanvasGroup>();
                    } else
                    {
                        itemCanvasGroup = item.gameObject.AddComponent<CanvasGroup>();
                    }
                    if (active)
                    {
                        itemCanvasGroup.alpha = 1;
                        itemCanvasGroup.interactable = true;
                        itemCanvasGroup.blocksRaycasts = true;
                        itemCanvasGroup.enabled = true;
                    } else
                    {
                        itemCanvasGroup.alpha = 0;
                        itemCanvasGroup.interactable = false;
                        itemCanvasGroup.blocksRaycasts = false;
                        //itemCanvasGroup.enabled = false;
                    }
                    //item.SetActive(active, fadeTime);
                }
                else
                {
                    Dbg.Trace("WARNING: Missing Substate GCUIObject " + Name);
                }
            }
        }
    }

    [Serializable]
    public class StateObject
    {
        public State State;
        public List<SubStateObject> SubStates;

        public StateObject(State state)
        {
            State = state;
            SubStates = new List<SubStateObject>();

            if (state == State.System)
            {
                SubStates.Add(new SubStateObject("Loading", 1));
                SubStates.Add(new SubStateObject("Tilt", 2));
                SubStates.Add(new SubStateObject("Handpay", 3));
                SubStates.Add(new SubStateObject("JackpotHandpay", 4));
                SubStates.Add(new SubStateObject("NonCashableCredit", 5));
                SubStates.Add(new SubStateObject("CashOutConfirm", 6));
                SubStates.Add(new SubStateObject("CashingOut", 7));
                SubStates.Add(new SubStateObject("Emulation", 8));
                SubStates.Add(new SubStateObject("Underfunded", 9));
                SubStates.Add(new SubStateObject("Help", 10));
            }
            else
            {
                SubStates.Add(new SubStateObject("Primary", 0));
            }
        }

        public void ClearAll(float fadeTime = 99)
        {
            foreach (SubStateObject item in SubStates)
            {
                item.SetActive(false, fadeTime);
            }
        }

        public void SetActive(bool active, int Id = 0, float fadeTime = 99)
        {
            ClearAll(fadeTime);

            if (!active) { return; }

            foreach (SubStateObject item in SubStates)
            {
                if ((item.Id == 0 && State != State.System) || item.Id == Id)
                {
                    item.SetActive(true, fadeTime);
                }
            }
        }

        public void SetActive(bool active, string subState, float fadeTime = 99)
        {
            ClearAll(fadeTime);

            if (!active) { return; }

            foreach (SubStateObject item in SubStates)
            {
                if ((item.Id == 0 && State != State.System) || item.Name == subState)
                {
                    item.SetActive(true, fadeTime);
                }
            }
        }
    }

    public class StateManager : MonoBehaviour
    {
        [HideInInspector]
        public Manager Manager;

        public StateObject Attract = new StateObject(State.Attract);
        public StateObject Idle = new StateObject(State.Idle);
        public StateObject Tutorial = new StateObject(State.Tutorial);
        public StateObject PreGame = new StateObject(State.PreGame);
        public StateObject InGame = new StateObject(State.InGame);
        public StateObject PostGame = new StateObject(State.PostGame);
        public StateObject Bonus = new StateObject(State.Bonus);
        public StateObject System = new StateObject(State.System);

        public State State;
        public State previousState;
        public string previousSubstate;
        public string currentStateString; // For the Inspector
        public string currentSubstate;

        public Coroutine noCashTimeOut;

        public event StateChangeEventHandler StateEvent;

        public float timeoutDuration = 20f;
        bool timeoutCountdownActive;

        StateObject[] stateObjects;

        private void Awake()
        {
            Manager = GetComponent<Manager>();
            stateObjects = new StateObject[] { Attract, Idle, Tutorial, PreGame, InGame, PostGame, Bonus, System };

        }

        /// <summary>
        /// Set all states to inactive, in preparation for switching.
        /// </summary>
        /// <param name="fadeTime"></param>
        void ClearAllStates(float fadeTime = 99)
        {
            foreach (StateObject state in stateObjects)
            {
                state.ClearAll(fadeTime);
            }
        }

        /// <summary>
        /// Change UI State. Changes UI.
        /// </summary>
        /// <param name="newState">State to change to</param>
        /// <param name="subStateId">Id of SubState. Defaults to 0, which is Primary.</param>
        /// <param name="fadeTime">Time for fade transition</param>
        public virtual void ChangeState(State newState, int subStateId = 0, float fadeTime = 99)
        {
            if (ChangeStateInternal(newState, "", fadeTime))
            {
                currentSubstate = "";

                stateObjects[(int)newState].SetActive(true, subStateId, fadeTime);

                Dbg.Trace("New State: " + newState + " SubState: " + subStateId);

                if (subStateId != 0)
                {
                    currentStateString += " - " + subStateId;
                }

                //Manager.ButtonManager.UpdateBetDenomButtons();

                StateEvent?.Invoke(this, new StateArgs(State));
            }
        }

        /// <summary>
        /// Change UI State. Changes UI.
        /// </summary>
        /// <param name="newState">State to change to</param>
        /// <param name="subStateName">Substate name</param>
        /// <param name="fadeTime">Time for fade transition</param>
        public virtual void ChangeState(State newState, string subStateName, float fadeTime = 99)
        {
            if (ChangeStateInternal(newState, subStateName, fadeTime))
            {
                stateObjects[(int)newState].SetActive(true, subStateName, fadeTime);

                Dbg.Trace("New State: " + newState + " SubState: " + subStateName);

                previousSubstate = currentSubstate;
                currentStateString += " - " + subStateName;
                currentSubstate = subStateName;

                try
                {
                    StopCoroutine(noCashTimeOut);
                    timeoutCountdownActive = false;
                }
                catch (Exception)
                {
                }

                if (subStateName == "Help")
                {
                    noCashTimeOut = StartCoroutine(NoCashTimeOut());
                }

                //Manager.ButtonManager.UpdateBetDenomButtons();

                StateEvent?.Invoke(this, new StateArgs(State));
            }
        }

        /// <summary>
        /// Sets SubStates on for testing. Not available while play mode is active.
        /// </summary>
        public virtual void VisualizeState(State newState, SubStateObject subStateObj)
        {
            Debug.Log ("Visualizing: " + newState + " SubState: " + subStateObj.Name);

            ClearVisualize(false);

            if (subStateObj.GCUIObjects.Count > 0)
            {
                foreach (GCUIObject item in subStateObj.GCUIObjects)
                {
                    item.gameObject.SetActive(true);
                }
            } else
            {
                Debug.Log("Substate " + subStateObj.Name + " has no objects attached!");
            }

            previousSubstate = "";
            currentStateString = "[Visualize] " + newState + " - " + subStateObj.Name;
            currentSubstate = subStateObj.Name;
        }

        /// <summary>
        /// Clears visualization of substates. Not required; starting play mode does the same thing.
        /// </summary>
        public virtual void ClearVisualize(bool clearBool)
        {
            StateObject[] stateObjectsForVisualize = new StateObject[] { Attract, Idle, Tutorial, PreGame, InGame, PostGame, Bonus, System };
            for (int i = 0; i < stateObjectsForVisualize.Length; i++)
            {
                if (stateObjectsForVisualize[i].SubStates.Count > 0)
                {
                    for (int a = 0; a < stateObjectsForVisualize[i].SubStates.Count; a++)
                    {
                        if (stateObjectsForVisualize[i].SubStates[a].Name != "Primary")
                        {
                            foreach (GCUIObject item in stateObjectsForVisualize[i].SubStates[a].GCUIObjects)
                            {
                                item.gameObject.SetActive(false);
                            }
                        }
                    }
                }
            }
            if (clearBool)
            {
                previousSubstate = "";
                currentStateString = "";
                currentSubstate = "";
                Debug.Log("State visualization cleared.");
            }
        }

        bool ChangeStateInternal(State newState, string subStateName = "", float fadeTime = 99)
        {
            if ((Manager.Codespace.tilted || (Manager.Codespace.Link.codespaceState > 8 && Manager.Codespace.Link.codespaceState != 14)) && newState != State.System)
            {
                Dbg.Trace("State must be System");
                return false;
            }

            if ((!Manager.Codespace.tilted && Manager.gameHandle != "") && Manager.Codespace.Link.codespaceState != 10 && Manager.Codespace.Link.codespaceState != 11 && subStateName != "Loading")
            {
                ClearAllStates(fadeTime);
                ChangeState(State.System, "Loading");
                return false;
            }

            if (newState == State.Attract && (Manager.Codespace.Link.totalValue > 0 || !Manager.PlayLoop.MachineClean()))
            {
                Dbg.Trace("Attract not appropriate");
                Manager.PlayLoop.StartSession();
                return false;
            }

            ClearAllStates(fadeTime);

            currentStateString = newState.ToString();

            previousState = State;
            State = newState;

            return true;
        }

        public virtual IEnumerator NoCashTimeOut()
        {
            if (Manager.PlayLoop.underfundedCountdownInProgress || timeoutCountdownActive || Manager.Codespace.Link.totalValue > 0) { yield break; }

            timeoutCountdownActive = true;

            timeoutDuration = 20;

            while (Manager.Codespace.Link.totalValue == 0 && timeoutDuration > 0)
            {
                yield return null;
                if (Manager.Codespace.Link.codespaceState == 4)
                {
                    timeoutDuration -= Time.deltaTime;
                }
            }

            timeoutCountdownActive = false;

            if (Manager.Codespace.Link.totalValue == 0 && Manager.Codespace.Link.codespaceState == 4)
            {
                ChangeState(State.Attract);
            }
        }
    }
}
