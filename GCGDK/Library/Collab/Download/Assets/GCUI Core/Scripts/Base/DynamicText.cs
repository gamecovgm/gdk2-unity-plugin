﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCUI;

namespace GCUI
{
    public class DynamicText : MonoBehaviour
    {

        [HideInInspector]
        public Manager Manager;

        GCGDKText[] allDynamicText;

        private void Awake()
        {
            Manager = GetComponent<Manager>();

            allDynamicText = GameObject.Find("GCUI").GetComponentsInChildren<GCGDKText>(true);
        }

        // Use this for initialization
        void Start()
        {

        }

        /// <summary>
        /// Updates all text objects in the target DynamicTextCategory to newText instantly.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="newText"></param>
        public void UpdateDynamicText(DynamicTextCategory target, string newText)
        {
            foreach (GCGDKText text in allDynamicText)
            {
                if (text.content == target)
                {
                    text.UpdateText(newText);
                }
            }
        }

        /// <summary>
        /// Bangs currency value up (or down) over duration seconds in all text objects in the target DynamicTextCategory.
        /// </summary>
        /// <param name="newValue"></param>
        /// <param name="duration">Bang duration (default 1 second)</param>
        public void UpdateDynamicText(DynamicTextCategory target, int newValue, float duration = 99, bool currencify = true, bool bangDown = false, string preText = "", string postText = "", AudioClip bangSFX = null)
        {
            foreach (GCGDKText text in allDynamicText)
            {
                if (text.content == target)
                {
                    text.UpdateText(newValue, duration, currencify, bangDown, preText, postText, bangSFX);
                }
            }
        }

        /// <summary>
        /// Interrupts text bang in all text objects of type DynamicTextCategory.target, jumps to end.
        /// </summary>
        /// <param name="target"></param>
        public void InterruptTextBang(DynamicTextCategory target)
        {
            foreach (GCGDKText text in allDynamicText)
            {
                if (text.content == target)
                {
                    text.interruptBang = true;
                }
            }
        }

        /// <summary>
        /// Returns true if any text object of type DynamicTextCategory.target are currently banging.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool IsBanging(DynamicTextCategory target)
        {
            bool isIt = false;

            for (int i = 0; i < allDynamicText.Length; i++)
            {
                if (allDynamicText[i].banging)
                {
                    isIt = true;
                    break;
                }
            }

            return isIt;
        }
    }
}
